Vicnisoft Project
=================

A Symfony project created on November 4, 2016, 10:54 am.

References 
==========
1. https://symfony.com/doc/master/bundles/NelmioApiDocBundle/the-apidoc-annotation.html
2. https://www.hostinger.com/tutorials/ssl/how-to-install-free-ssl-from-lets-encypt-on-shared-hosting

Todo:
=====

1. RestApiBundle

* Fix Error : config.yml -  exception_controller: 'RestApiBundle\Controller\ExceptionController::showAction' 

* Implement EntityRepository 

* 14.10.2016

    - Using ExceptionListener instead fos_rest:exception
    - Completed implement api-key
    - Completed implement EntityRepository
    
    * Todo :
        - Tạo thuật toán giải mã api-key
        
Note:
=====
* PATCH  update only the fields provided (The HTTP methods PATCH can be used to update partial resources) , PUT update all fields. So, PATCH
 
* In order use CustomEntityRepository , we have to remove file yml/xml... at Resources/config/doctrine
 
* Generate Single Entity

    - php app/console doctrine:mapping:import --force AppBundle yml --filter="UserProject"

    - php app/console doctrine:mapping:convert annotation ./src/AppBundle/Entity --from-database  --filter="UserProject"

Command :

* ssh -l u860238199 -p 65002 31.170.164.23

php bin/acme issue --domains ws.nlpapps.xyz:ws.nlpapps.xyz --path /home/u860238199/public_html:/home/u860238199/public_html --server letsencrypt