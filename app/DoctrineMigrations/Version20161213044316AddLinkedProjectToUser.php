<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161213044316AddLinkedProjectToUser extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("
        	ALTER TABLE user ADD  linked_project_id INT(11) UNSIGNED ;
        ");
        $this->addSql("ALTER TABLE user ADD INDEX index_user_project (linked_project_id)");
        $this->addSql("ALTER TABLE user ADD CONSTRAINT fk_user_project FOREIGN KEY (linked_project_id) REFERENCES project(id) ON DELETE SET NULL ON UPDATE CASCADE");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
