<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161212075747AddTableYoutubeVideo extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("CREATE TABLE youtube_video(id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, video_id VARCHAR (100)  NOT NULL, channel_id INT(11) UNSIGNED NOT NULL, published_at INT(11) UNSIGNED NOT NULL,
title VARCHAR (255) NOT NULL , description VARCHAR (255) ,
thumbnails_default VARCHAR (255), status INT(11) UNSIGNED NOT NULL,
PRIMARY KEY (id) ) CHARACTER SET utf8 COLLATE utf8_unicode_ci");
        $this->addSql("ALTER TABLE youtube_video ADD INDEX index_youtube_video_channel (channel_id)");
        $this->addSql("ALTER TABLE youtube_video ADD CONSTRAINT fk_youtube_video_channel FOREIGN KEY (channel_id) REFERENCES youtube_channel(id) ON DELETE CASCADE ON UPDATE CASCADE");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
