<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161207031747AddAccessTokenFieldToTableUserDevice extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("
        	ALTER TABLE user_device ADD  access_token VARCHAR(255) NOT NULL, ADD project_id INT(11) UNSIGNED NOT NULL ;
        ");
        $this->addSql("SET foreign_key_checks = 0");
        $this->addSql("ALTER TABLE user_device ADD INDEX index_user_device_project (project_id)");
        $this->addSql("ALTER TABLE user_device ADD CONSTRAINT fk_user_device_project FOREIGN KEY (project_id) REFERENCES project(id) ON DELETE CASCADE ON UPDATE CASCADE");
        $this->addSql("SET foreign_key_checks = 1");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
