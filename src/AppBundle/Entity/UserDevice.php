<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserDevice
 *
 * @ORM\Table(name="user_device", indexes={@ORM\Index(name="index_users_devices_user", columns={"user_id"}), @ORM\Index(name="index_users_devices_device", columns={"device_id"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserDeviceRepository")
 */
class UserDevice
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_active", type="integer", nullable=false)
     */
    private $lastActive;

    /**
     * @var string
     *
     * @ORM\Column(name="device_identifiter", type="string", length=255, nullable=true)
     */
    private $deviceIdentifiter;

    /**
     * @var string
     *
     * @ORM\Column(name="release_version", type="string", length=25, nullable=true)
     */
    private $releaseVersion;

    /**
     * @var string
     *
     * @ORM\Column(name="build_version", type="string", length=25, nullable=true)
     */
    private $buildVersion;

    /**
     * @var string
     *
     * @ORM\Column(name="access_token", type="string", length=255, nullable=false)
     */
    private $accessToken;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_created", type="integer", nullable=true)
     */
    private $dateCreated;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_updated", type="integer", nullable=true)
     */
    private $dateUpdated;

    /**
     * @var \AppBundle\Entity\Device
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Device")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="device_id", referencedColumnName="id")
     * })
     */
    private $device;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var \AppBundle\Entity\Project
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Project")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * })
     */
    private $project;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lastActive
     *
     * @param integer $lastActive
     *
     * @return UserDevice
     */
    public function setLastActive($lastActive)
    {
        $this->lastActive = $lastActive;

        return $this;
    }

    /**
     * Get lastActive
     *
     * @return integer
     */
    public function getLastActive()
    {
        return $this->lastActive;
    }

    /**
     * Set deviceIdentifiter
     *
     * @param string $deviceIdentifiter
     *
     * @return UserDevice
     */
    public function setDeviceIdentifiter($deviceIdentifiter)
    {
        $this->deviceIdentifiter = $deviceIdentifiter;

        return $this;
    }

    /**
     * Get deviceIdentifiter
     *
     * @return string
     */
    public function getDeviceIdentifiter()
    {
        return $this->deviceIdentifiter;
    }

    /**
     * Set releaseVersion
     *
     * @param string $releaseVersion
     *
     * @return UserDevice
     */
    public function setReleaseVersion($releaseVersion)
    {
        $this->releaseVersion = $releaseVersion;

        return $this;
    }

    /**
     * Get releaseVersion
     *
     * @return string
     */
    public function getReleaseVersion()
    {
        return $this->releaseVersion;
    }

    /**
     * Set buildVersion
     *
     * @param string $buildVersion
     *
     * @return UserDevice
     */
    public function setBuildVersion($buildVersion)
    {
        $this->buildVersion = $buildVersion;

        return $this;
    }

    /**
     * Get buildVersion
     *
     * @return string
     */
    public function getBuildVersion()
    {
        return $this->buildVersion;
    }

    /**
     * Set dateCreated
     *
     * @param integer $dateCreated
     *
     * @return UserDevice
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return integer
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set dateUpdated
     *
     * @param integer $dateUpdated
     *
     * @return UserDevice
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated
     *
     * @return integer
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * Set device
     *
     * @param \AppBundle\Entity\Device $device
     *
     * @return UserDevice
     */
    public function setDevice(\AppBundle\Entity\Device $device = null)
    {
        $this->device = $device;

        return $this;
    }

    /**
     * Get device
     *
     * @return \AppBundle\Entity\Device
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return UserDevice
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Project $project
     */
    public function setProject($project)
    {
        $this->project = $project;
    }


}
