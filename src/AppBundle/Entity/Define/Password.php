<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/5/16
 * Time: 12:00 AM
 */

namespace AppBundle\Entity\Define;

use Rollerworks\Bundle\PasswordStrengthBundle\Validator\Constraints as RollerworksPassword;


class Password
{

    private $salt;
    /**
     * @RollerworksPassword\PasswordRequirements(requireLetters=true, requireNumbers=true, minLength=8)
     */
    protected $plainpassword;

    protected $password;
    /**
     * @return String
     */
    public function getPlainpassword()
    {
        return $this->plainpassword;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }



    public function setPlainPassword($password) {
        if(isset($password) && !empty($password) ) {
            $this->plainpassword = $password;
            $db_password = $this->getDBPassword($password);
            $this->setPassword($db_password);
        }
    }

    public function getDBPassword($password) {
        $salt = $this->genRandomPassword(32);
        $encrypted = ($salt) ? md5($password . $salt) : md5($password);
        return "$encrypted:$salt";
    }

    private function genRandomPassword($length = 8)
    {
        $salt = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $base = strlen($salt);
        $makepass = '';

        /*
         * Start with a cryptographic strength random string, then convert it to
         * a string with the numeric base of the salt.
         * Shift the base conversion on each character so the character
         * distribution is even, and randomize the start shift so it's not
         * predictable.
         */
        $random = openssl_random_pseudo_bytes($length + 1);
        $shift = ord($random[0]);
        for ($i = 1; $i <= $length; ++$i)
        {
            $makepass .= $salt[($shift + ord($random[$i])) % $base];
            $shift += ord($random[$i]);
        }

        return $makepass;
    }

    private function generateSalt($length = 8)
    {
        $salt = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $base = strlen($salt);
        $makepass = '';

        /*
         * Start with a cryptographic strength random string, then convert it to
         * a string with the numeric base of the salt.
         * Shift the base conversion on each character so the character
         * distribution is even, and randomize the start shift so it's not
         * predictable.
         */
        $random = openssl_random_pseudo_bytes($length + 1);
        $shift = ord($random[0]);
        for ($i = 1; $i <= $length; ++$i)
        {
            $makepass .= $salt[($shift + ord($random[$i])) % $base];
            $shift += ord($random[$i]);
        }

        return $makepass;
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        $parts = explode(":", $this->password);
        $this->salt = (is_array($parts) && count($parts) > 1) ?  $parts[1] : null;
        return $this->salt;
    }

    public function generatePlainPassword() {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%";
        $numbers = "0123456789";
        $password = substr( str_shuffle( $chars ), 0, 7 );
        $number = substr( str_shuffle( $numbers ), 0, 1 );
        $password = str_shuffle( $password.$number );

        return $password;
    }



}