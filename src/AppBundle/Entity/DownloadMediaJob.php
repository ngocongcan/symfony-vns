<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DownloadMediaJob
 *
 * @ORM\Table(name="download_media_job")
 * @ORM\Entity
 */
class DownloadMediaJob
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="target_id", type="integer", nullable=false)
     */
    private $targetId;

    /**
     * @var string
     *
     * @ORM\Column(name="target_table", type="string", length=255, nullable=false)
     */
    private $targetTable;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="local_url", type="string", length=255, nullable=false)
     */
    private $localUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="remote_url", type="string", length=255, nullable=false)
     */
    private $remoteUrl;

    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="integer", nullable=true)
     */
    private $active = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="date_created", type="integer", nullable=true)
     */
    private $dateCreated;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_updated", type="integer", nullable=true)
     */
    private $dateUpdated;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set targetId
     *
     * @param integer $targetId
     *
     * @return DownloadMediaJob
     */
    public function setTargetId($targetId)
    {
        $this->targetId = $targetId;

        return $this;
    }

    /**
     * Get targetId
     *
     * @return integer
     */
    public function getTargetId()
    {
        return $this->targetId;
    }

    /**
     * Set targetTable
     *
     * @param string $targetTable
     *
     * @return DownloadMediaJob
     */
    public function setTargetTable($targetTable)
    {
        $this->targetTable = $targetTable;

        return $this;
    }

    /**
     * Get targetTable
     *
     * @return string
     */
    public function getTargetTable()
    {
        return $this->targetTable;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return DownloadMediaJob
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set localUrl
     *
     * @param string $localUrl
     *
     * @return DownloadMediaJob
     */
    public function setLocalUrl($localUrl)
    {
        $this->localUrl = $localUrl;

        return $this;
    }

    /**
     * Get localUrl
     *
     * @return string
     */
    public function getLocalUrl()
    {
        return $this->localUrl;
    }

    /**
     * Set remoteUrl
     *
     * @param string $remoteUrl
     *
     * @return DownloadMediaJob
     */
    public function setRemoteUrl($remoteUrl)
    {
        $this->remoteUrl = $remoteUrl;

        return $this;
    }

    /**
     * Get remoteUrl
     *
     * @return string
     */
    public function getRemoteUrl()
    {
        return $this->remoteUrl;
    }

    /**
     * Set active
     *
     * @param integer $active
     *
     * @return DownloadMediaJob
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return integer
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set dateCreated
     *
     * @param integer $dateCreated
     *
     * @return DownloadMediaJob
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return integer
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set dateUpdated
     *
     * @param integer $dateUpdated
     *
     * @return DownloadMediaJob
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated
     *
     * @return integer
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }
}
