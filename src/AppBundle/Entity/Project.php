<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Project
 *
 * @ORM\Table(name="project", indexes={@ORM\Index(name="index_created_by_id", columns={"created_by"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProjectRepository")
 */
class Project
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=false)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_created", type="integer", nullable=false)
     */
    private $dateCreated;

    /**
     * @var string
     *
     * @ORM\Column(name="icon_url", type="string", length=255, nullable=true)
     */
    private $iconUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="appstore_url", type="string", length=255, nullable=true)
     */
    private $appstoreUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="googleplay_url", type="string", length=255, nullable=true)
     */
    private $googleplayUrl;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="pem_url_aspn", type="string", length=255, nullable=true)
     */
    private $pemUrlAspn;

    /**
     * @var string
     *
     * @ORM\Column(name="pass_pharse_aspn", type="string", length=25, nullable=true)
     */
    private $passPharseAspn;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * })
     */
    private $createdBy;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Project
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Project
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set dateCreated
     *
     * @param integer $dateCreated
     *
     * @return Project
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return integer
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set iconUrl
     *
     * @param string $iconUrl
     *
     * @return Project
     */
    public function setIconUrl($iconUrl)
    {
        $this->iconUrl = $iconUrl;

        return $this;
    }

    /**
     * Get iconUrl
     *
     * @return string
     */
    public function getIconUrl()
    {
        return $this->iconUrl;
    }

    /**
     * Set appstoreUrl
     *
     * @param string $appstoreUrl
     *
     * @return Project
     */
    public function setAppstoreUrl($appstoreUrl)
    {
        $this->appstoreUrl = $appstoreUrl;

        return $this;
    }

    /**
     * Get appstoreUrl
     *
     * @return string
     */
    public function getAppstoreUrl()
    {
        return $this->appstoreUrl;
    }

    /**
     * Set googleplayUrl
     *
     * @param string $googleplayUrl
     *
     * @return Project
     */
    public function setGoogleplayUrl($googleplayUrl)
    {
        $this->googleplayUrl = $googleplayUrl;

        return $this;
    }

    /**
     * Get googleplayUrl
     *
     * @return string
     */
    public function getGoogleplayUrl()
    {
        return $this->googleplayUrl;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Project
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set pemUrlAspn
     *
     * @param string $pemUrlAspn
     *
     * @return Project
     */
    public function setPemUrlAspn($pemUrlAspn)
    {
        $this->pemUrlAspn = $pemUrlAspn;

        return $this;
    }

    /**
     * Get pemUrlAspn
     *
     * @return string
     */
    public function getPemUrlAspn()
    {
        return $this->pemUrlAspn;
    }

    /**
     * Set passPharseAspn
     *
     * @param string $passPharseAspn
     *
     * @return Project
     */
    public function setPassPharseAspn($passPharseAspn)
    {
        $this->passPharseAspn = $passPharseAspn;

        return $this;
    }

    /**
     * Get passPharseAspn
     *
     * @return string
     */
    public function getPassPharseAspn()
    {
        return $this->passPharseAspn;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     *
     * @return Project
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }
}
