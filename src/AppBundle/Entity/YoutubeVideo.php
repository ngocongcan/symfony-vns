<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * YoutubeVideo
 *
 * @ORM\Table(name="youtube_video", indexes={@ORM\Index(name="index_youtube_video_channel", columns={"channel_id"})})
 * @ORM\Entity
 */
class YoutubeVideo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="video_id", type="string", length=100, nullable=false)
     */
    private $videoId;

    /**
     * @var integer
     *
     * @ORM\Column(name="published_at", type="integer", nullable=false)
     */
    private $publishedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="thumbnails_default", type="string", length=255, nullable=true)
     */
    private $thumbnailsDefault;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var \YoutubeChannel
     *
     * @ORM\ManyToOne(targetEntity="YoutubeChannel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="channel_id", referencedColumnName="id")
     * })
     */
    private $channel;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getVideoId()
    {
        return $this->videoId;
    }

    /**
     * @param string $videoId
     */
    public function setVideoId($videoId)
    {
        $this->videoId = $videoId;
    }

    /**
     * @return int
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * @param int $publishedAt
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getThumbnailsDefault()
    {
        return $this->thumbnailsDefault;
    }

    /**
     * @param string $thumbnailsDefault
     */
    public function setThumbnailsDefault($thumbnailsDefault)
    {
        $this->thumbnailsDefault = $thumbnailsDefault;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return \YoutubeChannel
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * @param \YoutubeChannel $channel
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;
    }



}

