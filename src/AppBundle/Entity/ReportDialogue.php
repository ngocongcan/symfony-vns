<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ReportDialogue
 *
 * @ORM\Table(name="report_dialogue", indexes={@ORM\Index(name="index_dialogues_reports_users", columns={"report_by"}), @ORM\Index(name="index_dialogues_reports_dialogues", columns={"dialogue_id"})})
 * @ORM\Entity
 */
class ReportDialogue
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reason", type="string", length=512, nullable=true)
     */
    private $reason;

    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="integer", nullable=true)
     */
    private $active = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="date_created", type="integer", nullable=true)
     */
    private $dateCreated;

    /**
     * @var \AppBundle\Entity\Dialogue
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Dialogue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dialogue_id", referencedColumnName="id")
     * })
     */
    private $dialogue;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="report_by", referencedColumnName="id")
     * })
     */
    private $reportBy;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reason
     *
     * @param string $reason
     *
     * @return ReportDialogue
     */
    public function setReason($reason)
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * Get reason
     *
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * Set active
     *
     * @param integer $active
     *
     * @return ReportDialogue
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return integer
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set dateCreated
     *
     * @param integer $dateCreated
     *
     * @return ReportDialogue
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return integer
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set dialogue
     *
     * @param \AppBundle\Entity\Dialogue $dialogue
     *
     * @return ReportDialogue
     */
    public function setDialogue(\AppBundle\Entity\Dialogue $dialogue = null)
    {
        $this->dialogue = $dialogue;

        return $this;
    }

    /**
     * Get dialogue
     *
     * @return \AppBundle\Entity\Dialogue
     */
    public function getDialogue()
    {
        return $this->dialogue;
    }

    /**
     * Set reportBy
     *
     * @param \AppBundle\Entity\User $reportBy
     *
     * @return ReportDialogue
     */
    public function setReportBy(\AppBundle\Entity\User $reportBy = null)
    {
        $this->reportBy = $reportBy;

        return $this;
    }

    /**
     * Get reportBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getReportBy()
    {
        return $this->reportBy;
    }
}
