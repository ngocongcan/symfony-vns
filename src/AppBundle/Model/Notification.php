<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 12/26/16
 * Time: 4:14 PM
 */

namespace AppBundle\Model;

use AppBundle\Entity\Project;

class Notification
{
    /**
     * @var boolean
     *
     */

    private $deviceOnly = true;

    /**
     * @var Project
     *
     */
    private $project;

    /**
     * @var String
     */
    private $deviceId;
    /**
     * @var String
     *
     */
    private $message;

    /**
     * @param $deviceId
     *
     */

    /**
     * @param $deviceOnly
     *
     */

    public function setDeviceOnly($deviceOnly){
        $this->deviceOnly = $deviceOnly;
    }

    /**
     * @return bool
     *
     */
    public function getDeviceOnly(){
        return $this->deviceOnly;
    }


    public function setDeviceId($deviceId){
        $this->deviceId = $deviceId;
    }

    /**
     * @return String
     *
     */
    public function getDeviceId(){
        return $this->deviceId;
    }

    /**
     * @param $message
     *
     */
    public function setMessage($message){
        $this->message = $message;
    }

    /**
     * @return String
     *
     */
    public function getMessage(){
        return $this->message;

    }


    /**
     * @param $project
     *
     */

    public function setProject($project){

        $this->project = $project;

    }

    /**
     * @return Project
     *
     */

    public function getProject(){
        return $this->project;
    }

}