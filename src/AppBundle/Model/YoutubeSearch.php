<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 12/12/16
 * Time: 3:27 PM
 */

namespace AppBundle\Model;


class YoutubeSearch
{
    /** @var string  */
    private $searchText;

    /** @var string  */
    private $channelId ;

    /** @var string  */
    private $playlistId ;

    /**
     * @return string
     */
    public function getSearchText()
    {
        return $this->searchText;
    }

    /**
     * @param string $searchText
     */
    public function setSearchText($searchText)
    {
        $this->searchText = $searchText;
    }

    /**
     * @return string
     */
    public function getChannelId()
    {
        return $this->channelId;
    }

    /**
     * @param string $channelId
     */
    public function setChannelId($channelId)
    {
        $this->channelId = $channelId;
    }

    /**
     * @return string
     */
    public function getPlaylistId()
    {
        return $this->playlistId;
    }

    /**
     * @param string $playlistId
     */
    public function setPlaylistId($playlistId)
    {
        $this->playlistId = $playlistId;
    }




}