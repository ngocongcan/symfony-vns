<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 12/14/16
 * Time: 11:20 AM
 */

namespace AppBundle\Model;


class EslfastSearch
{
    private $url;

    /** @var  Categories */
    private $category;

    private $name;

    private $audioHost = "http://www.eslfast.com/robot/";
    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getAudioHost()
    {
        return $this->audioHost;
    }

    /**
     * @param string $audioHost
     */
    public function setAudioHost($audioHost)
    {
        $this->audioHost = $audioHost;
    }


}