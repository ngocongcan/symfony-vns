<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 7/6/16
 * Time: 11:38 AM
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;


class EditCategory extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options){


        $builder->add('name', TextType::class, array(
            'label' => "Name *",
            'constraints' => new NotBlank(),
            'trim' => true,
        ))->add('description', TextareaType::class, array(
            'label' => "Description",
            'trim' => true,
        ))->add('iconUrl', TextType::class, array(
            'label' => "Icon URL",
            'trim' => true,
        ))->add('approved', ChoiceType::class, array(
            'label' => "Approval status",
            'choices' => $options['approval_status'],
            'expanded' => true,
            'label_attr' => array('class' => 'radio_btn'),
            'attr' => array('class' => 'radio_btn')
        ));

        $builder->add('project', EntityType::class, array(
            'class' => 'AppBundle\Entity\Project',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('p')->andWhere('p.active = 1')
                    ->orderBy('p.name', 'ASC');
            },
            'choice_label' => 'name',
            'label' => "Project",
        ));

        $builder->add('cancel', SubmitType::class, array('label' => "Cancel", 'attr' => array('formnovalidate' => 'formnovalidate')))
            ->add('submit', SubmitType::class, array('label' => "Save",'attr' => array(
                'class' => 'btn-primary'
            )));


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'user_has_project' => false,
            'approval_status' => array(),
        ));
    }


}