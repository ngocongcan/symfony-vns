<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 12/12/16
 * Time: 3:22 PM
 */

namespace AppBundle\Form\Youtube;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class SearchVideo extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('searchText', TextType::class, array(
            'label' => "Search",
            'trim' => true,
        ))->add('channelId', TextType::class, array(
            'label' => "Channel ID",
            'trim' => true,
        ))->add('playlistId', TextType::class, array(
            'label' => "Playlist ID",
            'trim' => true,
        ));
        $builder->add('cancel', SubmitType::class, array('label' => "Cancel", 'attr' => array('formnovalidate' => 'formnovalidate')))
            ->add('submit', SubmitType::class, array('label' => "Save",'attr' => array(
                'class' => 'btn-primary'
            )));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'search_video';
    }


}