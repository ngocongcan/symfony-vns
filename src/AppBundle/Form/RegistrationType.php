<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/4/16
 * Time: 10:47 PM
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', TextType::class, array(
            'label' => 'Username',
        ))->add('plainpassword', RepeatedType::class, array(
            'first_options'=> array(
                'label' => 'Password',
            ),
            'second_options' => array(
                'label' => 'Repeat Password',
            ),
            'type' => PasswordType::class,

        ))->add('email', EmailType::class, array(
            'label' => 'Email',
        ));

        $builder->add('login', SubmitType::class,
            array('label' => "Login",
                'attr' =>
                    array('formnovalidate' => 'formnovalidate')
            )
        )->add('submit', SubmitType::class,
            array('label' => "Register",
                'attr' => array(
                    'class' => 'btn-primary'
                )
            )
        );

    }

    public function getName()
    {
        return "registration";
    }

    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Appbundle\Entity\User',
        ));
    }


}