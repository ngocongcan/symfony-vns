<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/4/16
 * Time: 8:20 PM
 */

namespace AppBundle\Form;


use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class EditProject extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder , array $options){

        $builder->add('name', TextType::class, array(
            'label' => "Name"
        ))->add('description', TextareaType::class,array(
                'label' => 'Description',
                'constraints' => new NotBlank(),
            ))
            ->add('iconUrl', FileType::class,array(
                'label' => 'Icon',
            ))
            ->add('appstoreUrl', TextType::class,array(
                'label' => 'AppStore Link',
            ))
             ->add('googleplayUrl', TextType::class,array(
                'label' => 'Google Play Link',
            ))

            ->add('createdBy', EntityType::class, array(
                'class' => 'AppBundle\Entity\User',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.username', 'ASC');
                },
                'choice_label' => 'username',
            ));

        $builder->add('cancel', SubmitType::class, array('label' => "Cancel", 'attr' => array('formnovalidate' => 'formnovalidate')))
            ->add('submit', SubmitType::class, array('label' => "Save",'attr' => array(
                'class' => 'btn-primary'
            )));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Project',
        ));
    }


}