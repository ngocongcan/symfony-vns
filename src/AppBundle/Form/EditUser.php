<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 5/31/16
 * Time: 5:55 PM
 */

namespace AppBundle\Form;


use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditUser extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options){


        $builder->add('company', TextType::class, array(
            'label' => "Company"
        ))
            ->add('username', TextType::class, array(
                'label' => "Username *",
                'constraints' => array(new NotBlank()),
                'help_label_tooltip' => array(
                    'title' => 'Your username can only contain lower case letters, numbers, - and _ character',
                ),
            ))
            ->add('plainpassword', RepeatedType::class, array(
                'first_options'  => array(
                    'label' => "Password *",
                    'help_label_tooltip' => array(
                        'title' => 'Password must contain a minimum of 8 characters and at least one number',
                    )
                ),
                'second_options' => array('label' => "Password Verification *"),
                'type' => PasswordType::class,
                'data' => '',
            ))
            ->add('firstname', TextType::class, array(
                'label' => "First name *",
                'constraints' => new NotBlank(),
                'required' => true
            ))
            ->add('lastname', TextType::class, array(
                'label' => "Last name *",
                'constraints' => new NotBlank(),
                'required' => true
            ))
            ->add('email', RepeatedType::class, array(
                'first_options'  => array(
                    'label' => "Email *",
                    'widget_addon_prepend' => array(
                        'text' => '@',
                    )
                ),
                'second_options' => array(
                    'label' => "Email Verification *",
                    'widget_addon_prepend' => array(
                        'text' => '@',
                    )
                ),
                'type' => EmailType::class,
                'constraints' => array(new NotBlank(), new Email()),
                'required' => true,

            ))
            ->add('phone', TextType::class, array(
                'label' => "Phone"
            ));

        $builder->add('apiKey', TextType::class, array(
                'label' => "Api Key",
                'data' => $options['api_key'],
                'disabled' => true,
                "widget_btn_append" => array(
                    array(
                        "icon" => "remove",
                        'class' => 'btn-danger btn-add-remove-api-key'
                    )
                ),
            )
        );

        $builder->add('linkedProject', EntityType::class, array(
            'class' => 'AppBundle\Entity\Project',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('p')->andWhere('p.active = 1')
                    ->orderBy('p.name', 'ASC');
            },
            'choice_label' => 'name',
            'label' => 'Linked Project *'

        ));

        $builder->add('cancel', SubmitType::class, array('label' => "Cancel", 'attr' => array('formnovalidate' => 'formnovalidate')))
            ->add('submit', SubmitType::class, array('label' => "Save",'attr' => array(
                'class' => 'btn-primary'
            )));


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User',
            'user' => null,
            'api_key' => null,
        ));
    }

}