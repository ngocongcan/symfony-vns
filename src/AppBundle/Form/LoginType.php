<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/4/16
 * Time: 10:57 PM
 */

namespace AppBundle\Form;


use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LoginType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', TextType::class, array(
            'label' => 'Username',
        ))->add('email', PasswordType::class, array(
            'label' => 'Password',
        ));

        $builder->add('cancel', SubmitType::class,
            array('label' => "Register",
                'attr' =>
                    array('formnovalidate' => 'formnovalidate')
            )
        )->add('submit', SubmitType::class,
            array('label' => "Login",
                'attr' => array(
                    'class' => 'btn-primary'
                )
            )
        );
    }

    public function getName()
    {
        return 'login';
    }



}