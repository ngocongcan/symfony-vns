<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 8/21/16
 * Time: 11:49 AM
 */

namespace AppBundle\Form\ParserHTML;

use AppBundle\Entity\Project;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class EditEslFast extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('url', TextType::class, array(
            'label' => "URL *",
            'constraints' => new NotBlank(),

        ))->add('audioHost', TextType::class, array(
            'label' => "Audio Host",

        ));

        $project =  $options['project'] ;

        if($project instanceof Project){
            $projectId = $project->getId();
            $builder->add('category', EntityType::class, array(
                'class' => 'AppBundle\Entity\Category',
                'query_builder' => function (EntityRepository $er) use ($projectId) {
                    return $er->createQueryBuilder('c')
                        ->leftJoin('c.project','p')
                        ->andWhere('c.active = 1')
                        ->andWhere("p.id = $projectId")
                        ->orderBy('c.name', 'ASC');
                },
                'choice_label' => 'name',
                'label' => 'Category *'
            ));

        } else {
            $builder->add('category', EntityType::class, array(
                'class' => 'AppBundle\Entity\Category',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')->andWhere('c.active = 1')
                        ->orderBy('c.name', 'ASC');
                },
                'choice_label' => 'name',
                'label' => 'Category *'

            ));
        }

        $builder->add('cancel', SubmitType::class, array('label' => "Cancel", 'attr' => array('formnovalidate' => 'formnovalidate')))
            ->add('submit', SubmitType::class, array('label' => "Save",'attr' => array(
                'class' => 'btn-primary'
            )));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'project' => null,
        ));

    }


}