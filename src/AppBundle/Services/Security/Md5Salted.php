<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/5/16
 * Time: 12:07 PM
 */

namespace AppBundle\Services\Security;


use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;

class Md5Salted implements PasswordEncoderInterface
{
    public function encodePassword($raw, $salt)
    {
        $encrypted  = ($salt) ? md5($raw . $salt) : md5($raw);
        return $encrypted;
    }

    public function isPasswordValid($encoded, $raw, $salt)
    {
        $parts = explode(":", $encoded);
        $pass = $parts[0];
        return $pass === ($this->encodePassword($raw,$salt));
    }


}