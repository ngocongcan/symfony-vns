<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/14/16
 * Time: 11:37 PM
 */

namespace AppBundle\Services;


class Common
{

    private $characterSet = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    private $apiKeyLength = 64;
    private $accessTokenLength = 32;

    protected $doctrine;

    public function __construct($doctrine){

        $this->doctrine = $doctrine;
    }

    public function generateApiKey()
    {
        return self::generate($this->characterSet, $this->apiKeyLength);
    }

    public function generateAccessToken()
    {
        return self::generate($this->characterSet, $this->accessTokenLength);
    }

    public static function generate($characterSet = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', $apiKeyLength = 64)
    {
        $characterSetLength = strlen($characterSet);
        $apikey = '';
        for ($i = 0; $i < $apiKeyLength; ++$i) {
            $apikey .= $characterSet[rand(0, $characterSetLength - 1)];
        }
        return rtrim(base64_encode(sha1(uniqid('ue' . rand(rand(), rand())) . $apikey)), '=');
    }


    public function generateFileName($key, $prefix, $extension){
        $englishString = $this->convertVietnameseCharactersToEnglish($key);
        $noSpaceString = str_replace(' ','_',$englishString);
        $fileName = $prefix. '_' .$noSpaceString.'_'. time(). '_' . rand(1, 99999). '.'.$extension;

        return $fileName;
    }

    function convertVietnameseCharactersToEnglish ($str){

        $unicode = array(

            'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',

            'd'=>'đ',

            'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',

            'i'=>'í|ì|ỉ|ĩ|ị',

            'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',

            'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',

            'y'=>'ý|ỳ|ỷ|ỹ|ỵ',

            'A'=>'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',

            'D'=>'Đ',

            'E'=>'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',

            'I'=>'Í|Ì|Ỉ|Ĩ|Ị',

            'O'=>'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',

            'U'=>'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',

            'Y'=>'Ý|Ỳ|Ỷ|Ỹ|Ỵ',

        );

        foreach($unicode as $nonUnicode=>$uni){

            $str = preg_replace("/($uni)/i", $nonUnicode, $str);

        }

        return $str;

    }
}