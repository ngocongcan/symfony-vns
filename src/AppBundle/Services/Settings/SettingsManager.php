<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/4/16
 * Time: 2:45 PM
 */

namespace AppBundle\Services\Settings;


class SettingsManager
{

    const YOUTUBE_VIDEO_STATUS_DEFAULT = 0;
    const YOUTUBE_VIDEO_STATUS_APPROVED = 1;

    const APPROVAL_STATUS_NOT_COMPLETED = 0;
    const APPROVAL_STATUS_PENDING_APPROVAL = 1;
    const APPROVAL_STATUS_APPROVED = 2;

    const VIDEO_TYPE_MP4 = 'mp4';
    const VIDEO_TYPE_YOUTUTE = 'youtube';
    const VIDEO_TYPE_VIMEO = 'vimeo';

    const MEDIA_TYPE_AUDIO = 'audio';
    const MEDIA_TYPE_YOUTUTE = 'youtube';
    const MEDIA_TYPE_VIDEO = 'video';
    const MEDIA_TYPE_TEXT = 'text';

    public static function getAvailablesRoles(){

        return array(
            'ROLE_CAN_ACCESS_PROJECTS',
            'ROLE_CAN_ACCESS_USERS',
            'ROLE_CAN_ACCESS_ANALYTICS',
        );
    }
    public static function getVideoTypes(){

        return [
            'Mp4' => SELF::VIDEO_TYPE_MP4,
            'Youtube' => SELF::VIDEO_TYPE_YOUTUTE,
            'Vimeo' => SELF::VIDEO_TYPE_VIMEO,
            '#NULL' => '',
        ];

    }

    public static function getDialogueTypes(){

        return array(
            SELF::MEDIA_TYPE_AUDIO => "Audio",
            SELF::MEDIA_TYPE_VIDEO => "Video",
            SELF::MEDIA_TYPE_YOUTUTE => "Youtube",
            SELF::MEDIA_TYPE_TEXT => "Text",
            '' => "#NULL",
        );
    }

    public static function getApprovalStatus(){
        return array(
            SELF::APPROVAL_STATUS_NOT_COMPLETED => 'Not completed',
            SELF::APPROVAL_STATUS_PENDING_APPROVAL => 'Pending approval',
            SELF::APPROVAL_STATUS_APPROVED => 'Approved',
        );
    }

    public static function getYoutubeVideoStatus(){
        return array(
            SELF::YOUTUBE_VIDEO_STATUS_DEFAULT => 'Not completed',
            SELF::YOUTUBE_VIDEO_STATUS_APPROVED => 'Approved',
        );
    }

    public function getUsersDefaultColumns(){

        return $columns = array(
            'id' => array(
                'display' => true,
                'name' => 'ID #',
                'order' => 'ASC',
            ),
            'ip_address' => array(
                'display' => true,
                'name' => 'IP Address',
                'order' => 'ASC',
            ),
            'username' => array(
                'display' => true,
                'name' => 'Username',
                'order' => 'ASC',
            ),
            'email' => array(
                'display' => true,
                'name' => 'Email',
                'order' => 'ASC',
            ),
            'linked_project' => array(
                'display' => true,
                'name' => 'Linked Project',
                'order' => 'ASC',

            ),
            'project_name' => array(
                'display' => true,
                'name' => 'Project Name',
                'order' => 'ASC',

            ),

        );

    }

    public function getProjectsDefaultColumns(){

        return $columns = array(
            'id' => array(
                'display' => true,
                'name' => 'ID #',
                'order' => 'ASC',
            ),
            'icon_url' => array(
                'display' => true,
                'name' => 'Icon',
                'order' => 'ASC',
            ),
            'name' => array(
                'display' => true,
                'name' => 'Name',
                'order' => 'ASC',
            ),
            'description' => array(
                'display' => true,
                'name' => 'Desc',
                'order' => 'ASC',
            ),
            'appstore_url' => array(
                'display' => true,
                'name' => 'AppStore Link',
                'order' => 'ASC',
            ),
            'googleplay_url' => array(
                'display' => true,
                'name' => 'PlayStore Link',
                'order' => 'ASC',
            )
        );

    }

    public function getDevicesDefaultColumns(){

        return $columns = array(
            'id' => array(
                'display' => true,
                'name' => 'ID #',
                'line' => 0,
                'order' => 'ASC',
            ),
            'type' => array(
                'display' => true,
                'name' => 'Type',
                'line' => 0,
                'order' => 'ASC',

            ),
            'os_version' => array(
                'display' => true,
                'name' => 'OS Version',
                'line' => 0,
                'order' => 'ASC',
            ),
            'localize' => array(
                'display' => true,
                'name' => 'Localize',
                'line' => 0,
                'order' => 'ASC',
            ),
            'date_created' => array(
                'display' => true,
                'name' => 'Date Created',
                'line' => 0,
                'order' => 'ASC',
            ),
            'project_name' => array(
                'display' => true,
                'name' => 'Project Name',
                'line' => 1,
                'order' => 'ASC',
            ),
            'username' => array(
                'display' => true,
                'name' => 'Username',
                'line' => 1,
                'order' => 'ASC',
            ),
            'device_identifier' => array(
                'display' => true,
                'name' => 'Device Identifier',
                'line' => 1,
                'order' => 'ASC',
            ),
            'last_active' => array(
                'display' => true,
                'name' => 'Last Active',
                'line' => 1,
                'order' => 'ASC',
            ),
            'release_version' => array(
                'display' => true,
                'name' => 'Release Version',
                'line' => 1,
                'order' => 'ASC',
            ),
            'build_version' => array(
                'display' => true,
                'name' => 'Build Version',
                'line' => 1,
                'order' => 'ASC',
            ),

        );

    }

    public function getLogsDefaultColumns(){

        return $columns = array(
            'user_id' => array(
                'display' => true,
                'name' => 'User ID #',
                'line' => 0,
                'order' => 'ASC',
            ),
            'client_ip' => array(
                'display' => true,
                'name' => 'Client IP',
                'line' => 0,
                'order' => 'ASC',
            ),
            'project_name' => array(
                'display' => true,
                'name' => 'Project',
                'line' => 0,
                'order' => 'ASC',
            ),
            'country_name' => array(
                'display' => true,
                'name' => 'Country',
                'line' => 0,
                'order' => 'ASC',
            ),
            'count' => array(
                'display' => true,
                'name' => 'Count',
                'line' => 0,
                'order' => 'ASC',
            ),
            'created_on' => array(
                'display' => true,
                'name' => 'Created On',
                'line' => 0,
                'order' => 'ASC',
            ),
            'request_id' => array(
                'display' => true,
                'name' => 'ID #',
                'line' => 1,
                'order' => 'ASC',
            ),
            'username' => array(
                'display' => true,
                'name' => 'username',
                'line' => 1,
                'order' => 'ASC',
            ),
            'time_zone' => array(
                'display' => true,
                'name' => 'Time Zone',
                'line' => 1,
                'order' => 'ASC',
            ),
            'city' => array(
                'display' => true,
                'name' => 'City',
                'line' => 1,
                'order' => 'ASC',
            ),

        );

    }


    /**
     * @return array
     *
     */
    public function getCategoriesDefaultColumns(){

        return $columns = array(
            'id' => array(
                'display' => true,
                'name' => 'ID #',
                'order' => 'ASC',
            ),
            'name' => array(
                'display' => true,
                'name' => 'Name',
                'order' => 'ASC',
            ),
            'description' => array(
                'display' => true,
                'name' => 'Description',
                'order' => 'ASC',
            ),
            'icon_url' => array(
                'display' => true,
                'name' => 'Icon URL',
                'order' => 'ASC',
            ),
            'project_name' => array(
                'display' => true,
                'name' => 'Project Name',
                'order' => 'ASC',
            ),
            'total_dialogues' => array(
                'display' => true,
                'name' => 'Total Dialogues',
                'order' => 'ASC',
            ),
            'approval_status' => array(
                'display' => true,
                'name' => 'Approval Status',
                'text' => $this->getApprovalStatus(),
                'order' => 'ASC',
            ),

        );

    }

    public function getDialoguesDefaultColumns(){

        return $columns = array(
            'id' => array(
                'display' => true,
                'name' => 'ID #',
                'order' => 'ASC',
            ),
            'name' => array(
                'display' => true,
                'name' => 'Name',
                'order' => 'ASC',
            ),
            'content' => array(
                'display' => true,
                'name' => 'Content',
                'order' => 'ASC',
//                'raw' => true,
            ),
            'icon_url' => array(
                'display' => false,
                'name' => 'Icon URL',
                'order' => 'ASC',
            ),
            'media_url' => array(
                'display' => true,
                'name' => 'Media URL',
                'order' => 'ASC',
            ),
            'approval_status' => array(
                'display' => false,
                'name' => 'Approval status',
                'text' => $this->getApprovalStatus(),
                'order' => 'ASC',
            ),
            'category_name' => array(
                'display' => false,
                'name' => 'Category',
                'order' => 'ASC',
            ),
            'project_name' => array(
                'display' => false,
                'name' => 'Project',
                'order' => 'ASC',
            ),
            'number_report' => array(
                'display' => true,
                'name' => 'Number of report',
                'order' => 'ASC',
            ),
            'type' => array(
                'display' => false,
                'name' => 'Media Type',
                'text' => $this->getDialogueTypes(),
                'order' => 'ASC',
            ),

        );
    }

    public function getYoutubeVideoDefaultColumns(){

        return $columns = array(
            'id' => array(
                'display' => true,
                'name' => 'ID ',
                'order' => 'ASC',
            ),
            'title' => array(
                'display' => true,
                'name' => 'Title',
                'order' => 'ASC',
            ),
            'channel_title' => array(
                'display' => true,
                'name' => 'Channel title',
                'order' => 'ASC',
            ),
            'video_id' => array(
                'display' => false,
                'name' => 'Video ID',
                'order' => 'ASC',
            ),
            'thumbnails_default' => array(
                'display' => true,
                'name' => 'Thumbnail',
                'order' => 'ASC',
            ),
            'status' => array(
                'display' => false,
                'name' => 'Status',
                'text' => $this->getApprovalStatus(),
                'order' => 'ASC',
            ),
            'description' => array(
                'display' => false,
                'name' => 'Description',
                'order' => 'ASC',
            ),
            'channel_id' => array(
                'display' => false,
                'name' => 'Channel ID',
                'order' => 'ASC',
            ),
        );
    }

}