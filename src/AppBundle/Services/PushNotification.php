<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 12/26/16
 * Time: 4:12 PM
 */

namespace AppBundle\Services;


use AppBundle\Model\Notification;
use Doctrine\Bundle\DoctrineBundle\Registry;
use RMS\PushNotificationsBundle\Message\iOSMessage;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Exception;

class PushNotification
{
    private $container ;
    private $doctrine;

    public function __construct(ContainerInterface $container, Registry $doctrine){
        $this->container = $container;
        $this->doctrine = $doctrine;
    }

    public function sendIosMessage(Notification $notification){

        $pemFileName = $notification->getProject()->getPemUrlAspn();
        $passPharse = $notification->getProject()->getPassPharseAspn();
        $dir = $this->container->getParameter('pem_dir');
        $pemPath = $dir . $pemFileName;
//        $pemContent = file_get_contents($pemPath);

//        $this->container->get('rms_push_notifications')->setAPNSPemAsString($pemContent, $passPharse);
//        $pemPath2 = $this->container->getParameter('pem_path');

        $result = array();
        if($notification->getDeviceOnly()){
            if($this->sendSingleMessage($notification->getDeviceId(), $notification->getMessage(),$pemPath, $passPharse ) ){
                $result[] = $notification->getDeviceId();
            } else {
                $result[] = "fail ". $notification->getDeviceId();
            }
        } else {

            $devices = $notification->getProject()->getUserDevices();
            foreach($devices as $device){
                if($this->sendSingleMessage($device->getDeviceIdentifiter(), $notification->getMessage(), $pemPath, $passPharse ) ) {
                    $result[] = $device->getDeviceIdentifiter();
                }
            }

        }

        return $result;

    }

    private function sendSingleMessage($identifier, $msg, $pemPath, $passPharse){
        try{
            $message = new iOSMessage();
            $message->setMessage($msg);
            $message->setDeviceIdentifier($identifier);
            $pemContent = file_get_contents($pemPath);
            $this->container->get('rms_push_notifications')->setAPNSPemAsString($pemContent, $passPharse);
            $this->container->get('rms_push_notifications')->send($message);

        } catch(Exception $e){
            return false;
        }

        return true;
    }

}