<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 8/23/16
 * Time: 10:02 AM
 */

namespace AppBundle\Services\Jobs;


use AppBundle\Entity\Dialogue;
use AppBundle\Services\Settings\SettingsManager;
use Doctrine\Bundle\DoctrineBundle\Registry;
use AppBundle\Entity\DownloadMediaJob;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use Exception;

class DownloadMedia
{

    const DIALOGUE_TABLE = 'Dialogue';

    /** @var   */
    private $doctrine;

    /** @var string */

    private $rootPath;

    /**
     * DownloadImage constructor.
     */
    public function __construct(Registry $doctrine, $rootPath)
    {
        $this->doctrine = $doctrine;
        $this->rootPath = $rootPath;

    }

    public function download($targetTable){

        $returnMessage = "Download ". $targetTable;

        $repository = $this->doctrine
            ->getRepository('AppBundle:DownloadMediaJob');
        $query = $repository->createQueryBuilder('dmj')
            ->where('dmj.targetTable = :targetTable')
            ->setParameter('targetTable', $targetTable)
            ->orderBy('dmj.id', 'DESC')
            ->getQuery();

        $mediaJobs = $query->getResult();

        if($targetTable == SELF::DIALOGUE_TABLE) {
            $maxId = -1;
            if(is_array($mediaJobs) && count($mediaJobs) > 0) {
                $maxId = $mediaJobs[0]->getTargetId();
            }

            $repository = $this->doctrine
                ->getRepository('AppBundle:Dialogue');

            /** @var QueryBuilder $query */

            $qb = $repository->createQueryBuilder('d')
                ->setParameter('maxId', $maxId)
                ->orderBy('d.id', 'ASC');


            $qb->add('where',
                $qb->expr()->andX(
                    $qb->expr()->gt('d.id', ':maxId'),
                    $qb->expr()->isNotNull('d.mediaUrl')
                )
            );

            $query = $qb->getQuery();

            $dialogues = $query->setMaxResults(1)->getOneOrNullResult();

            if($dialogues) {
                return $this->downloadDialogueMedia($dialogues);
            }

        }

        return $returnMessage;

    }


    private function downloadDialogueMedia(Dialogue $dialogues){

        $path = "";
        $mediaURL = $dialogues->getMediaUrl();
        $splits = explode('/', trim($mediaURL));
        $fileName = $dialogues->getMediaUrl();
        if($dialogues->getType() == SettingsManager::MEDIA_TYPE_YOUTUTE){

        }else {
            if(count($splits) > 0) {
                $fileName = $splits[count($splits) - 1];
                $download = new DownloadTask($this->rootPath);
                $path = $download->download($fileName, $mediaURL);
                if($path == null) {
                    $path = '';
                }
            }

        }
        try {
            $mediaJob = new DownloadMediaJob();
            $mediaJob->setLocalUrl($path);
            $mediaJob->setName($fileName);
            $mediaJob->setTargetId($dialogues->getId());
            $mediaJob->setTargetTable(SELF::DIALOGUE_TABLE);
            $mediaJob->setRemoteUrl($mediaURL);

            $this->doctrine->getManager()->persist($mediaJob);
            $this->doctrine->getManager()->flush();
        } catch (Exception $e){
            return $e->getMessage();
        }

    }
}