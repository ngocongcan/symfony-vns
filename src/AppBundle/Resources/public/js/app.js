/**
 * Created by canngo on 11/4/16.
 */
function setOrder(element) {
    var field = $(element).data("field");
    var order = $(element).data("order");
    $("#sortfield").val(field);
    $("#sortorder").val(order);
    $("#pageform").submit();
}

function setActive(element) {
    var active = ($(element).val() == 1) ? 0 : 1;
    $("#active").val(active);
    $("#pageform").submit();
}

function removeFilter(element) {
    $("#activefilter").val($(element).data("name"));
    if($(element).attr('data-value')){
        $("#activefiltervalue").val($(element).data("value"));
    }
    $("#pageform").submit();
}