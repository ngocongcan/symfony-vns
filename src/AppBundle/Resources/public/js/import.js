/**
 * Created by canngo on 12/12/16.
 */
function openPreviewModal(element) {
    var tr = $(element).closest('tr');
    $('#open_preview').find("#videoTitle").text($(element).data("title"));
    $('#open_preview').find("#channelTitle").text($(element).data("channelname"));
    $('#open_preview').find("iframe").attr('src','http://www.youtube.com/embed/' + $(element).data("videoid"));
    openPreview(element);
}

function openPreview(element) {
    $('#open_preview').modal('show');
}


$(document).ready(function ($) {
    initAjaxForm("[name='create_dialogue']");
});

function initAjaxForm(selector) {
    $('body').on('submit', selector, function (e) {
        e.preventDefault();
        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: $(this).serialize()
        })
            .done(function (data) {
                window.location.reload(true);
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                if (typeof jqXHR.responseJSON !== 'undefined') {
                    if (jqXHR.responseJSON.hasOwnProperty('form')) {
                        $('#book_dialogue').html(jqXHR.responseJSON.form);
                    }
                    if (jqXHR.responseJSON.hasOwnProperty('errorMessage')) {
                        $('#show_error').find("#error_message").text(jqXHR.responseJSON.errorMessage);
                        $('#book_dialogue').find("#show_error").removeClass('hide').removeClass('show').addClass('show');
                    }
                } else {
                    window.location.reload(true);
                }

            });
    });
}

function openBookDialogueModal(element) {
    var tr = $(element).closest('tr');
    $('#book_dialogue').find("#create_dialogue_name").val($(element).data("name"));
    $('#book_dialogue').find("#create_dialogue_mediaUrl").val("https://www.youtube.com/watch?v=" + $(element).data("videoid"));
    $('#book_dialogue').find("#create_dialogue_iconUrl").val($(element).data("iconurl"));
    $('#book_dialogue').find("#youtube_video_id").val($(element).data("videoyoutubeid"));
    openBookDialogue(element);
}

function openBookDialogue(element) {
    $('#book_dialogue').find("#show_error").hide();
    $('#book_dialogue').find(".modal-title").html($(element).data("title"));
    //$('#book_dialogue').find("[name='create_group']").attr("action", $(element).data("action"));
    $('#book_dialogue').modal('show');
}
