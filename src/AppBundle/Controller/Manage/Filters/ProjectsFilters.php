<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 10/3/16
 * Time: 10:47 PM
 */

namespace AppBundle\Controller\Manage\Filters;



use AppBundle\Controller\Manage\Tools\UserInfos;
use AppBundle\Tools\Filters\ManageFilters;
use AppBundle\Tools\Filters\Filter;
use AppBundle\Tools\Conditions\Condition;
use AppBundle\Tools\QueryHelpers\QueryBuilderTool;

class ProjectsFilters extends ManageFilters
{
    public function setFilters(UserInfos $userInfos)
    {
        // TODO: Implement setFilters() method.
        $filters = array();
        /** @Desc("Defaults") */
        $default = 'Defaults';
        $this->addSection($default);

        // Project Name

        /*

        if(empty($userInfos->getProjectIds())) {
            $queryBuilder = new QueryBuilderTool();
            $queryBuilder->addToSelect("DISTINCT p.name as text, p.id as value");
            $queryBuilder->setFrom('project', 'p');
            $queryBuilder->addOrderBy('name');

            $condition = new Condition(Condition::IN, 'id', 'p');
            $text = 'Project Name';
            $filter = new Filter(Filter::TYPE_SELECT, 'project_name', $text, array(0), $condition, $queryBuilder);
            $filter->setDefaultText($text);
            $filter->setSection($default);
            $filters["project_name"] = $filter;
        }
        */


        // Get filters values
        $this->getFiltersValues($filters);
        return $filters;
    }


}