<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 8/12/16
 * Time: 4:28 PM
 */

namespace AppBundle\Controller\Manage\Filters;


use AppBundle\Controller\Manage\Tools\UserInfos;
use AppBundle\Tools\Filters\ManageFilters;
use AppBundle\Tools\Filters\Filter;
use AppBundle\Tools\Conditions\Condition;
use AppBundle\Tools\QueryHelpers\QueryBuilderTool;

class DeviceFilters extends ManageFilters
{

    public function setFilters(UserInfos $userInfos)
    {
        // TODO: Implement setFilters() method.
        $filters = array();
        /** @Desc("User") */
        $userSection = 'User';
        $this->addSection($userSection);
        /** @Desc("Device") */
        $deviceSection = 'Device';
        $this->addSection($deviceSection);
        /** @Desc("Project") */
        $projectSection = 'Project';
        $this->addSection($projectSection);

//		Username
        $condition = new Condition(Condition::INTEXT, 'username', 'u');
        $text = 'Username';
        $filter = new Filter(Filter::TYPE_TEXT, 'username', $text, '', $condition);
        $filter->setSection($userSection);
        $filters["username"] = $filter;

        // os_version
        $condition = new Condition(Condition::EQUAL, 'os_version', 'd');
        $text = 'OS Version';
        $filter = new Filter(Filter::TYPE_NUMBER, 'os_version', $text, '', $condition);
        $filter->setSection($deviceSection);
        $filters["os_version"] = $filter;
        // build_version
        $condition = new Condition(Condition::EQUAL, 'build_version', 'ud');
        $text = 'Build Version';
        $filter = new Filter(Filter::TYPE_TEXT, 'build_version', $text, '', $condition);
        $filter->setSection($deviceSection);
        $filters["build_version"] = $filter;
        // release_version
        $condition = new Condition(Condition::EQUAL, 'release_version', 'ud');
        $text = 'Release Version';
        $filter = new Filter(Filter::TYPE_TEXT, 'release_version', $text, '', $condition);
        $filter->setSection($deviceSection);
        $filters["release_version"] = $filter;

        // Project Name
        $queryBuilder = new QueryBuilderTool();
        $queryBuilder->addToSelect("DISTINCT p.name as text, p.id as value");
        $queryBuilder->setFrom('project', 'p');
        $queryBuilder->addOrderBy('name');

        $condition = new Condition(Condition::IN, 'id', 'p');
        $text = 'Project Name';
        $filter = new Filter(Filter::TYPE_SELECT, 'project_name', $text, array(0), $condition, $queryBuilder);
        $filter->setDefaultText($text);
        $filter->setSection($projectSection);
        $filters["project_name"] = $filter;

        // Get filters values
        $this->getFiltersValues($filters);
        return $filters;
    }
}