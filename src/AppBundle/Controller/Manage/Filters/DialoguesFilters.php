<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 8/12/16
 * Time: 5:10 PM
 */

namespace AppBundle\Controller\Manage\Filters;


use AppBundle\Controller\Manage\Tools\UserInfos;
use AppBundle\Tools\Filters\ManageFilters;
use AppBundle\Tools\Conditions\Condition;
use AppBundle\Tools\QueryHelpers\QueryBuilderTool;
use AppBundle\Tools\Filters\Filter;

class DialoguesFilters extends ManageFilters
{
    public function setFilters(UserInfos $userInfos)
    {
        // TODO: Implement setFilters() method.
        $filters = array();
        /** @Desc("Defaults") */
        $default = 'Defaults';
        $this->addSection($default);

        //		Name
        $condition = new Condition(Condition::INTEXT, 'name', 'd');
        $text = 'Name';
        $filter = new Filter(Filter::TYPE_TEXT, 'name', $text, '', $condition);
        $filter->setSection($default);
        $filters["name"] = $filter;

        //		Content
        $condition = new Condition(Condition::INTEXT, 'content', 'd');
        $text = 'Content';
        $filter = new Filter(Filter::TYPE_TEXT, 'content', $text, '', $condition);
        $filter->setSection($default);
        $filters["content"] = $filter;

        // Project Name


        if(empty($userInfos->getProjectIds())) {
            $queryBuilder = new QueryBuilderTool();
            $queryBuilder->addToSelect("DISTINCT p.name as text, p.id as value");
            $queryBuilder->setFrom('project', 'p');
            $queryBuilder->addOrderBy('name');

            $condition = new Condition(Condition::IN, 'id', 'p');
            $text = 'Project Name';
            $filter = new Filter(Filter::TYPE_SELECT, 'project_name', $text, array(0), $condition, $queryBuilder);
            $filter->setDefaultText($text);
            $filter->setSection($default);
            $filters["project_name"] = $filter;
        }

        $queryBuilder = new QueryBuilderTool();
        $queryBuilder->addToSelect("DISTINCT c.name as text, c.id as value");
        $queryBuilder->setFrom('category', 'c');
        $queryBuilder->addOrderBy('c.name');



        if(!is_null($userInfos->getLinkedProject())) {
            $queryBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'project', 'p', 'p.id = c.project_id');
            $condition = new Condition(Condition::EQUAL,'id', 'p', '', $userInfos->getLinkedProject()->getId());
            $queryBuilder->addCondition($condition);
        }

        $condition = new Condition(Condition::IN, 'id', 'c');
        $text = 'Category Name';
        $filter = new Filter(Filter::TYPE_SELECT, 'category_name', $text, array(0), $condition, $queryBuilder);
        $filter->setDefaultText($text);
        $filter->setSection($default);
        $filters["category_name"] = $filter;

        // Approval Status

        $condition = new Condition(Condition::EQUAL, 'approved', 'd');
        $text = 'Approval Status';
        $filter = new Filter(Filter::TYPE_CHOICE, 'approval_status', $text, -1 , $condition);
        $filter->setDefaultText($text);
        $statues = $this->settings->getApprovalStatus();
        $options = array();
        foreach ($statues as $key => $value){
            $options[] = array(
                'value' => $key,
                'text' => $value,
            );
        }

        $filter->setValue('-1');
        $filter->setSelectValues($options);
        $filters['approval_status'] = $filter;

        // Media Type

        $condition = new Condition(Condition::EQUAL, 'type', 'd');
        $text = 'Media Type';
        $filter = new Filter(Filter::TYPE_CHOICE, 'type', $text, -1 , $condition);
        $filter->setDefaultText("Select type");
        $types = $this->settings->getDialogueTypes();
        $options = array();
        foreach ($types as $key => $value){
            $options[] = array(
                'value' => $key,
                'text' => $value,
            );
        }

        $filter->setSelectValues($options);
        $filters['type'] = $filter;

        // Get filters values
        $this->getFiltersValues($filters);
        return $filters;

    }


}