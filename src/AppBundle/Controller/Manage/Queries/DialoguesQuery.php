<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/5/16
 * Time: 5:24 PM
 */

namespace AppBundle\Controller\Manage\Queries;


use AppBundle\Tools\Conditions\Condition;
use AppBundle\Tools\QueryHelpers\QueryBuilderTool;
use AppBundle\Tools\QueryHelpers\QueryManager;
use AppBundle\Controller\Manage\Tools\UserInfos;

class DialoguesQuery extends QueryManager
{


    public function buildQuery(UserInfos $userInfos = null, $nolimit = false){

        $this->sqlBuilder = new QueryBuilderTool();
        $this->sqlBuilder->addToSelect('d.id, d.name, d.content , d.icon_url, d.media_url, d.type as type,
        d.approved as approval_status, c.name as category_name, p.name as project_name,
        COUNT(rd.id) number_report
        ');
        $this->sqlBuilder->setFrom('dialogue', 'd');
        $this->sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'category', 'c', 'c.id = d.category_id');
        $this->sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'project', 'p', 'p.id = c.project_id');
        $this->sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'report_dialogue', 'rd', 'rd.dialogue_id = d.id');
        $this->sqlBuilder->addOrderBy('id', QueryBuilderTool::ORDERDESC);
        $this->sqlBuilder->addGroupBy('id', 'd');
        $condition = new Condition(Condition::EQUAL, 'active', 'd', '', $this->getActive());
        $this->sqlBuilder->addCondition($condition);

        $linkedProject = $userInfos->getLinkedProject();
        if(!is_null($linkedProject)){
            $condition = new Condition(Condition::EQUAL,'id', 'p', '', $linkedProject->getId());
            $this->sqlBuilder->addCondition($condition);
        }

        foreach($this->conditions as $condition){
            $this->sqlBuilder->addCondition($condition);
        }

        if(!$nolimit) {
            $this->sqlBuilder->setLimit(($this->page * $this->rows_per_page) . "," . $this->rows_per_page);
            $this->executeCountQuery();
            if ($this->total_rows == 0) {
                return array();
            }
        }
        $this->sqlBuilder->addOrderBy($this->order->getField(), $this->order->getOrder());

        return $this->executeQuery($this->sqlBuilder);
    }

}