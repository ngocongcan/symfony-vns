<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/4/16
 * Time: 7:53 PM
 */

namespace AppBundle\Controller\Manage\Queries;


use AppBundle\Tools\Conditions\Condition;
use AppBundle\Tools\QueryHelpers\QueryBuilderTool;
use AppBundle\Tools\QueryHelpers\QueryManager;
use AppBundle\Controller\Manage\Tools\UserInfos;

class ProjectsQuery extends QueryManager
{
    public function buildQuery(UserInfos $userInfos = null, $nolimit = false){
        $this->sqlBuilder = new QueryBuilderTool();
        $select = '*';
        $this->sqlBuilder->addToSelect($select);
        $this->sqlBuilder->setFrom('project','p');

        foreach($this->conditions as $condition){
            $this->sqlBuilder->addCondition($condition);
        }
        $condition = new Condition(Condition::EQUAL, 'active', 'p', '', $this->getActive());
        $this->sqlBuilder->addCondition($condition);

        if(!$nolimit) {
            $this->sqlBuilder->setLimit(($this->page * $this->rows_per_page) . "," . $this->rows_per_page);
            $this->executeCountQuery();
            if ($this->total_rows == 0) {
                return array();
            }
        }
        $this->sqlBuilder->addOrderBy($this->order->getField(), $this->order->getOrder());

        return $this->executeQuery($this->sqlBuilder);
    }


}