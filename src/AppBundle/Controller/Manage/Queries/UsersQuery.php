<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/4/16
 * Time: 2:35 PM
 */

namespace AppBundle\Controller\Manage\Queries;


use AppBundle\Tools\Conditions\Condition;
use AppBundle\Tools\QueryHelpers\QueryBuilderTool;
use AppBundle\Tools\QueryHelpers\QueryManager;
use AppBundle\Controller\Manage\Tools\UserInfos;

class UsersQuery extends QueryManager
{


    public function buildQuery(UserInfos $userInfos = null, $nolimit = false){

        $this->sqlBuilder = new QueryBuilderTool();
        $select = "
            u.id as id, ip_address, username, email,
            GROUP_CONCAT(DISTINCT p.name SEPARATOR ', ')  as project_name,
            p2.name as linked_project
        ";
        $this->sqlBuilder->addToSelect($select);
        $this->sqlBuilder->setFrom('user', 'u');
//        $this->sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'user','u','ug.user_id = u.id');
//        $this->sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'group','g','ug.group_id = g.id');
        $this->sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'project','p2','p2.id = u.linked_project_id');
        $this->sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'user_project','up','up.user_id = u.id');
        $this->sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'project','p','up.project_id = p.id');

        $condition = new Condition(Condition::EQUAL, 'active', 'u', '', $this->getActive());
        $this->sqlBuilder->addCondition($condition);

        /*
        if(!is_null($userInfos) && !empty($userInfos->getProjectIds())){
            $condition = new Condition(Condition::IN,'id', 'p', '', $userInfos->getProjectIds());
            $this->sqlBuilder->addCondition($condition);
        }
        */

        foreach($this->conditions as $condition){
            $this->sqlBuilder->addCondition($condition);
        }

        $this->sqlBuilder->addGroupBy('id','u');

        if(!$nolimit) {
            $this->sqlBuilder->setLimit(($this->page * $this->rows_per_page) . "," . $this->rows_per_page);
            $this->executeCountQuery();
            if ($this->total_rows == 0) {
                return array();
            }
        }

        $this->sqlBuilder->addOrderBy($this->order->getField(), $this->order->getOrder());

        return $this->executeQuery($this->sqlBuilder);

    }


}