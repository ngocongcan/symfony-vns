<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 7/1/16
 * Time: 10:01 AM
 */

namespace AppBundle\Controller\Manage\Queries;


use AppBundle\Tools\QueryHelpers\QueryManager;
use AppBundle\Tools\QueryHelpers\QueryBuilderTool;
use AppBundle\Tools\Conditions\Condition;
use AppBundle\Controller\Manage\Tools\UserInfos;

class DevicesQuery extends QueryManager
{
    public function buildQuery(UserInfos $userInfos = null, $nolimit = false){
        $this->sqlBuilder = new QueryBuilderTool();
        $select = "
            ud.id as user_device_id, from_unixtime(ud.last_active) as last_active,
            ud.device_identifiter as device_identifier, ud.release_version, ud.build_version,
            d.id as id, d.type as type, d.os_version as os_version, d.localize as localize, from_unixtime(d.date_created) as date_created,
            p.name as project_name, u.username as username
        ";
        $this->sqlBuilder->addToSelect($select);
        $this->sqlBuilder->setFrom('user_device', 'ud');
        $this->sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'device','d','d.id = ud.device_id');
        $this->sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'project','p','p.id = ud.project_id');
        $this->sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'user','u','u.id = ud.user_id');
        $this->sqlBuilder->addGroupBy('id','ud');

        foreach($this->conditions as $condition){
            $this->sqlBuilder->addCondition($condition);
        }

        if(!$nolimit) {
            $this->sqlBuilder->setLimit(($this->page * $this->rows_per_page) . "," . $this->rows_per_page);
            $this->executeCountQuery();
            if ($this->total_rows == 0) {
                return array();
            }
        }
        $this->sqlBuilder->addOrderBy($this->order->getField(), $this->order->getOrder());

        return $this->executeQuery( $this->sqlBuilder);

    }

}