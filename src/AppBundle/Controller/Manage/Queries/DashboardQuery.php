<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 8/22/16
 * Time: 5:47 PM
 */

namespace AppBundle\Controller\Manage\Queries;



use AppBundle\Tools\QueryHelpers\QueryBuilderTool;
use AppBundle\Tools\QueryHelpers\QueryManager;
use AppBundle\Tools\Conditions\Condition;
use AppBundle\Controller\Manage\Tools\UserInfos;

class DashboardQuery extends QueryManager
{
    public function buildQuery(UserInfos $userInfos = null)
    {
        // TODO: Implement buildQuery() method.

        if($userInfos == null) {
            return null;
        }

        $newUsers = $this->getNewUsers($userInfos);
        $newDevices = $this->getNewDevices($userInfos);
//        $newRequests = $this->getNewRequests($userInfos);
        $mediaDownloads = $this->getMediaDownloads($userInfos);
        $imageDownloads = $this->getImageDownloads($userInfos);

        return array(
            'Users' => $newUsers,
            'Devices' => $newDevices,
//            'Requests' => $newRequests,
            'MediaDownloads' => $mediaDownloads,
            'ImageDownloads' => $imageDownloads,
        );

    }


    private function getNewUsers(UserInfos $userInfos){

        $sqlBuilder = new QueryBuilderTool();
        $sqlBuilder->addToSelect('*');
        $sqlBuilder->setFrom('user', 'u');
//        if($lastTime = $userInfos->getUser()->getDateCreated()) {
//            $condition = new Condition(Condition::GOREQUAL,'date_created', 'u', '', $lastTime);
//            $sqlBuilder->addCondition($condition);
//        }

        return $this->executeQuery($sqlBuilder);

    }

    private function getNewDevices(UserInfos $userInfos){

        $sqlBuilder = new QueryBuilderTool();
        $sqlBuilder->addToSelect('*');
        $sqlBuilder->setFrom('device', 'd');

//        if($lastTime = $userInfos->getUser()->getLastLogin()) {
//            $condition = new Condition(Condition::GOREQUAL,'date_created', 'd', '', $lastTime);
//            $sqlBuilder->addCondition($condition);
//        }

        return $this->executeQuery($sqlBuilder);

    }

    private function getNewRequests(UserInfos $userInfos){

        $sqlBuilder = new QueryBuilderTool();
        $sqlBuilder->addToSelect('*');
        $sqlBuilder->setFrom('log_user', 'lu');

//        if($lastTime = $userInfos->getUser()->getLastLogin()) {
//            $condition = new Condition(Condition::GOREQUAL,'date_created', 'lu', '', $lastTime);
//            $sqlBuilder->addCondition($condition);
//        }

        return $this->executeQuery($sqlBuilder);

    }


    private function getMediaDownloads(UserInfos $userInfos){

        $sqlBuilder = new QueryBuilderTool();
        $sqlBuilder->addToSelect('*');
        $sqlBuilder->setFrom('download_media_job', 'dmj');
        $sqlBuilder->addOrderBy('date_created', QueryBuilderTool::ORDERDESC);

        return $this->executeQuery($sqlBuilder);

    }

    private function getImageDownloads(UserInfos $userInfos){

        $sqlBuilder = new QueryBuilderTool();
        $sqlBuilder->addToSelect('*');
        $sqlBuilder->setFrom('download_image_job', 'dmj');
        $sqlBuilder->addOrderBy('date_created', QueryBuilderTool::ORDERDESC);

        return $this->executeQuery($sqlBuilder);

    }

}