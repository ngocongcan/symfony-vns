<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/4/16
 * Time: 7:58 PM
 */

namespace AppBundle\Controller\Manage\Queries;


use AppBundle\Tools\QueryHelpers\QueryBuilderTool;
use AppBundle\Tools\QueryHelpers\QueryManager;
use AppBundle\Tools\Conditions\Condition;
use AppBundle\Controller\Manage\Tools\UserInfos;
class CategoriesQuery extends QueryManager
{

    public function buildQuery(UserInfos $userInfos = null, $nolimit = false){

        $this->sqlBuilder = new QueryBuilderTool();
        $select = "
            c.id as id, c.name as name, c.description as description, p.name as project_name,
            c.icon_url as icon_url,  c.approved as approval_status, count(d.id) as total_dialogues

        ";
        $this->sqlBuilder->addToSelect($select);
        $this->sqlBuilder->setFrom('category', 'c');
        $this->sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN, 'project', 'p','p.id = c.project_id');
        $this->sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN, 'dialogue', 'd','d.category_id = c.id');
        $condition = new Condition(Condition::EQUAL, 'active', 'c', '', $this->getActive());
        $this->sqlBuilder->addCondition($condition);
        $this->sqlBuilder->addGroupBy('id','c');

        if(!$nolimit) {
            $this->sqlBuilder->setLimit(($this->page * $this->rows_per_page) . "," . $this->rows_per_page);
            $this->executeCountQuery();
            if ($this->total_rows == 0) {
                return array();
            }
        }
        $linkedProject = $userInfos->getLinkedProject();
        if(!is_null($linkedProject)){
            $condition = new Condition(Condition::EQUAL,'id', 'p', '', $linkedProject->getId());
            $this->sqlBuilder->addCondition($condition);
        }
        foreach($this->conditions as $condition){
            $this->sqlBuilder->addCondition($condition);
        }

        $this->sqlBuilder->addOrderBy($this->order->getField(), $this->order->getOrder());
        return $this->executeQuery($this->sqlBuilder);

    }


}