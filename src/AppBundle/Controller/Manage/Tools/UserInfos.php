<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 8/12/16
 * Time: 2:43 PM
 */

namespace AppBundle\Controller\Manage\Tools;

use AppBundle\Entity\User;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class UserInfos
{

    private $doctrine;
    private $tokenStorage;
    /** @var  User */
    private $user ;

    /**
     * UserInfos constructor.
     * @param User $user
     */
    public function __construct(RegistryInterface $doctrine,TokenStorage $tokenStorage)
    {
        $this->doctrine = $doctrine;
        $this->tokenStorage = $tokenStorage;
        $this->user = $tokenStorage->getToken()->getUser();

    }


    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return null|\Project
     */

    public function getLinkedProject(){

        return $this->user->getLinkedProject();
    }

    public function getProjectIds(){
        $usersProjects = $this->doctrine->getRepository('AppBundle:UserProject')->findBy(
            array('user'=> $this->user)
        );
        $projectIds = array_map(function($o) { return $o->getProject()->getId(); }, $usersProjects);
        return $projectIds;
    }





}