<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 12/28/16
 * Time: 4:21 PM
 */

namespace AppBundle\Controller\Manage;


use AppBundle\Controller\Manage\Tools\UserInfos;
use AppBundle\Controller\Manage\Queries\DevicesQuery;
use AppBundle\Controller\Manage\Filters\DeviceFilters;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

class DevicesController extends ManageController
{

    protected $page_path = 'manageDevicesPage';
    protected $route = 'manageDevices';
    protected $dbname = 'device';
    protected $default_order = "id";


    public function showAction(Request $request){

        $this->init();
        $this->initPageValues();
        $this->setFilters($request);
        $this->columns = $this->getColumns($request);
        return $this->renderPage($request);
    }

    protected function getDefaultColumns()
    {
        // TODO: Implement getDefaultColumns() method.
        return $this->get('settings')->getDevicesDefaultColumns();
    }


    private function getData(){

        $data['columns'] = $this->columns;
        $data['rows'] = $this->buildQuery();
        $data["pagination"] = $this->buildPaginationData();
        $data["page"] = $this->page;
        $data['page_path'] = $this->page_path;
        $data['active'] = $this->queryManager->getActive();
        /** Filters */
        $data["filters"] = $this->filterManager->getFiltersBySections($this->filters);
        $data["useFilterSections"] = true;
        $data["activefilters"] = $this->active_filters;

        return $data;

    }

    protected function init()
    {
        // TODO: Implement init() method.
        $rights = array();
        $this->userInfos = $this->get('app.userinfo');
        $this->queryManager = new DevicesQuery($this->getDoctrine());
        $this->filterManager = new DeviceFilters($this->getDoctrine(), $rights, $this->get('settings'));

    }

    protected function renderPage(Request $request)
    {
        // TODO: Implement renderPage() method.
        return $this->render('AppBundle:manage:devices.html.twig',
            array('data' => $this->getData()
            )
        );
    }

    protected function initFilters()
    {
        // TODO: Implement initFilters() method.
        return $this->filterManager->setFilters($this->userInfos);

    }


    private function buildQuery(){

        return $this->queryManager->buildQuery($this->userInfos);
    }

    protected function recycle($id, $active)
    {
        // TODO: Implement recycle() method.
    }


}