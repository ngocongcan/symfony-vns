<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 12/7/16
 * Time: 4:52 PM
 */

namespace AppBundle\Controller\Manage;


use AppBundle\Controller\Manage\Tools\UserInfos;
use AppBundle\Controller\Manage\Queries\DialoguesQuery;
use AppBundle\Controller\Manage\Filters\DialoguesFilters;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

class DialoguesController extends ManageController
{

    protected $recycle_path = 'manageDialoguesRecycle';
    protected $page_path = 'manageDialoguesPage';
    protected $route = 'manageDialogues';
    protected $create_path = 'createDialogue';
    protected $dbname = 'dialogue';
    protected $default_order = "id";


    public function showAction(Request $request){

        $this->init();
        $this->initPageValues();
        $this->setFilters($request);
        $this->columns = $this->getColumns($request);
        return $this->renderPage($request);
    }

    protected function getDefaultColumns()
    {
        // TODO: Implement getDefaultColumns() method.
        return $this->get('settings')->getDialoguesDefaultColumns();
    }


    protected function recycle($id, $active) {

        $recycled = ($active) ? 'recycle' : 'restore';
        $result = $this->get('app.dialogue.handler')->recycle($id, $active);
        if($result == true){
            $this->container->get('session')->getFlashBag()->add(
                'success',
                "Dialogue with ".$id. " ".$recycled." successfully"
            );
        } else {
            $this->container->get('session')->getFlashBag()->add(
                'error',
                "Not able to ".$recycled." dialogue with id ".$id
            );
        }
        return $result;
    }



    private function getData(){

        $data['columns'] = $this->columns;
        $data['rows'] = $this->buildQuery();
        $data["pagination"] = $this->buildPaginationData();
        $data["page"] = $this->page;
        $data['create_path'] = $this->create_path;
        $data['page_path'] = $this->page_path;
        $data['active'] = $this->queryManager->getActive();
        /** Filters */
        $data["filters"] = $this->filterManager->getFiltersBySections($this->filters);
        $data["useFilterSections"] = true;
        $data["activefilters"] = $this->active_filters;
        // Recycle info
        $data["recyclepath"] = $this->recycle_path;
        $data["recyclename"] = 'name';
        $data["recycleinfo"] = 'category_name';
        $data["recycletype"] = 'dialogue';


        return $data;

    }

    protected function init()
    {
        // TODO: Implement init() method.
        $rights = array();
        $this->userInfos = $this->get('app.userinfo');
        $this->queryManager = new DialoguesQuery($this->getDoctrine());
        $this->filterManager = new DialoguesFilters($this->getDoctrine(), $rights, $this->get('settings'));

    }

    protected function renderPage(Request $request)
    {
        // TODO: Implement renderPage() method.
        return $this->render('AppBundle:manage:dialogues.html.twig',
            array('data' => $this->getData()
            )
        );
    }

    protected function initFilters()
    {
        // TODO: Implement initFilters() method.
        return $this->filterManager->setFilters($this->userInfos);

    }


    private function buildQuery(){

        return $this->queryManager->buildQuery($this->userInfos);
    }

    
}