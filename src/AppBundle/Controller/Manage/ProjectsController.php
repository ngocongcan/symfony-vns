<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 12/28/16
 * Time: 3:44 PM
 */

namespace AppBundle\Controller\Manage;


use AppBundle\Controller\Manage\Filters\ProjectsFilters;
use AppBundle\Controller\Manage\Tools\UserInfos;
use AppBundle\Controller\Manage\Queries\ProjectsQuery;
use Symfony\Component\HttpFoundation\Request;

class ProjectsController extends ManageController
{
    protected $recycle_path = 'manageProjectsRecycle';
    protected $page_path = 'manageProjectsPage';
    protected $route = 'manageProjects';
    protected $create_path = 'createProject';
    protected $dbname = 'project';
    protected $default_order = "id";


    public function showAction(Request $request){

        $this->init();
        $this->initPageValues();
        $this->setFilters($request);
        $this->columns = $this->getColumns($request);
        return $this->renderPage($request);
    }

    protected function getDefaultColumns()
    {
        // TODO: Implement getDefaultColumns() method.
        return $this->get('settings')->getProjectsDefaultColumns();
    }


    protected function recycle($id, $active) {
        $recycled = ($active) ? 'recycle' : 'restore';
        $result = $this->get('app.project.handler')->recycle($id, $active);
        if($result == true){
            $this->container->get('session')->getFlashBag()->add(
                'success',
                "Project with ".$id. " ".$recycled." successfully"
            );
        } else {
            $this->container->get('session')->getFlashBag()->add(
                'error',
                "Not able to ".$recycled." project with id ".$id
            );
        }
        return $result;
    }



    private function getData(){

        $data['columns'] = $this->columns;
        $data['rows'] = $this->buildQuery();
        $data["pagination"] = $this->buildPaginationData();
        $data["page"] = $this->page;
        $data['create_path'] = $this->create_path;
        $data['page_path'] = $this->page_path;
        $data['active'] = $this->queryManager->getActive();
        /** Filters */
        $data["filters"] = $this->filterManager->getFiltersBySections($this->filters);
        $data["useFilterSections"] = true;
        $data["activefilters"] = $this->active_filters;
        // Recycle info
        $data["recyclepath"] = $this->recycle_path;
        $data["recyclename"] = 'name';
        $data["recycleinfo"] = 'description';
        $data["recycletype"] = 'Project';


        return $data;

    }

    protected function init()
    {
        // TODO: Implement init() method.
        $rights = array();
        $this->userInfos = $this->get('app.userinfo');
        $this->queryManager = new ProjectsQuery($this->getDoctrine());
        $this->filterManager = new ProjectsFilters($this->getDoctrine(), $rights, $this->get('settings'));

    }

    protected function renderPage(Request $request)
    {
        // TODO: Implement renderPage() method.
        return $this->render('AppBundle:manage:projects.html.twig',
            array('data' => $this->getData()
            )
        );
    }

    protected function initFilters()
    {
        // TODO: Implement initFilters() method.
        return $this->filterManager->setFilters($this->userInfos);

    }


    private function buildQuery(){

        return $this->queryManager->buildQuery($this->userInfos);
    }



}