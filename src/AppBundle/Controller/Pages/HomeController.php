<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/4/16
 * Time: 11:46 PM
 */

namespace AppBundle\Controller\Pages;



use AppBundle\Controller\Manage\Queries\DashboardQuery;
use AppBundle\Controller\Manage\Tools\UserInfos;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends Controller
{

    /** @var  DashboardQuery */

    private $queryManager;
    private $userInfo;

    public function indexAction(Request $request){

        $this->init();
        return $this->render('AppBundle:pages:home.html.twig',
            array(
                'data' => $this->getData(),
            )
        );
    }

    private function init(){

        $this->userInfo = $this->get('app.userinfo');
        $this->queryManager = new DashboardQuery($this->getDoctrine());

    }

    /** Privates */

    private function getData(){

        $data['page_title'] = "Home";
        $data['items'] = array();

        $result = $this->queryManager->buildQuery($this->userInfo);
        if ($result){
            $data['notifications'] = $result;
        }

        return $data;
    }

}