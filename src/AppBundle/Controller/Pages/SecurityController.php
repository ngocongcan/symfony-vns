<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/4/16
 * Time: 11:07 PM
 */

namespace AppBundle\Controller\Pages;


use AppBundle\Entity\User;
use AppBundle\Form\RegistrationType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends Controller
{
    public function loginAction(Request $request){

        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('home');
        }
        $helper = $this->get('security.authentication_utils');

        return $this->render(
            'AppBundle:login:login.html.twig',
            [
                'last_username' => $helper->getLastUsername(),
                'error' => $helper->getLastAuthenticationError(),
            ]
        );
    }


    public function registerAction(Request $request){

        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('home');
        }

        $registration = new User();
        $form = $this->createForm(new RegistrationType(), $registration);
        $form->handleRequest($request);

        if($form->get('login')->isClicked()){
            // redirect
            return $this->redirectToRoute('login');
        }

        if($form->isValid()){
            $this->createUser($request, $registration);
        }

        $data = array();

        if($form instanceof RedirectResponse){
            return $form;
        } else {

            return $this->render('AppBundle:login:register.html.twig', array(
                'data' => $data,
                'form' => $form->createView(),
            ));
        }


    }

    private function createUser(Request $request, User $user){

    }





}