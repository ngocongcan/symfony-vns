<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 12/7/16
 * Time: 5:30 PM
 */

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\EditUser;
use AppBundle\Services\Settings\SettingsManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Exception;

class UserController extends AbstractEntityController
{
    protected $route = "manageUsers";
    /** @var  $user  The User that is editing */
    private $user ;

    public function createAction(Request $request){

        $this->mode = SELF::MODE_CREATE;
        $this->user = new User();
        return $this->setAction($request);

    }

    public function editAction(Request $request, $id = null){

        if($id === null){
            throw new NotFoundHttpException('User id is missing');
        }

        $this->user = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);

        if($this->user === null){
            throw new NotFoundHttpException('This user id was not found');
        }
        return $this->setAction($request);
    }


    /** Privates */

    private function setAction(Request $request){

        $data = $this->getData();
        $form = $this->renderPage($request, $data);

        if($form instanceof RedirectResponse){
            return $form;
        }

        return $this->render('AppBundle:form:editUser.html.twig',
            array(
                'form' => $form->createView(),
                'data' => $data,
            )
        );
    }


    private function renderPage(Request $request, $data){

        $form = $this->createForm(EditUser::class, $this->user, $this->getOptions());
        $form->handleRequest($request);
        if($form->get('cancel')->isClicked()){
            return $this->redirectToRoute($this->route);
        }
        if($form->isValid()){
            /** @var User $user */
            $user = $form->getData();
            if($this->mode == SELF::MODE_CREATE && ($this->get('app.user.handler')->userExists($user))) {
                $this->container->get('session')->getFlashBag()
                    ->add(
                        'error',
                        "User with name " . $this->user->getName() . " exits in the this category "
                    );
            } else {
                $result = $this->get('app.user.handler')->save($user, $this->mode == SELF::MODE_EDIT );
                if($result == true){
                    if ($this->mode == SELF::MODE_CREATE) {
                        $this->container->get('session')->getFlashBag()->add(
                            'success',
                            "User ". $this->user->getName()." created successfully."
                        );
                    } else {
                        if ($this->mode == SELF::MODE_EDIT) {
                            $this->container->get('session')->getFlashBag()->add(
                                'success',
                                "User ". $this->user->getName() ." updated successfully."
                            );
                        }
                    }
                    return $this->redirectToRoute($this->route);
                } else {

                    $errorMessage = "Can not save User";
                    $this->container->get('session')->getFlashBag()
                        ->add(
                            'error',
                            $errorMessage
                        );
                    $data['error_messages'] = $errorMessage;
                }
            }
        }
        return $form;
    }

    private function getData(){

        $data['page_title'] = $this->mode == SELF::MODE_CREATE ? "Create User" : "Edit User";
        $data['options'] = $this->getOptions();
        return $data;
    }

    private function getOptions() {
        $options = array();
        $options['api_key'] = $this->get('app.common')->generateApiKey();
        return $options;
    }

}