<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Form\EditCategory;
use AppBundle\Services\Settings\SettingsManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Exception;


class CategoryController extends AbstractEntityController
{
    protected $route = "manageCategories";
    /** @var  Category  The category that is editing */
    private $category ;

    public function createAction(Request $request){

        $this->mode = SELF::MODE_CREATE;
        $this->category = new Category();
        $project = $this->get('app.entity.handler')->getLinkedProject();
        $this->category->setProject($project);
        return $this->setAction($request);

    }

    public function editAction(Request $request, $id = null){

        if($id === null){
            throw new NotFoundHttpException('Category id is missing');
        }

        $this->category = $this->getDoctrine()->getRepository('AppBundle:Category')->find($id);

        if($this->category === null){
            throw new NotFoundHttpException('This category id was not found');
        }
        return $this->setAction($request);
    }

    public function approveAction(Request $request, $id = null){

        if($id === null){
            throw new NotFoundHttpException('Category id is missing');
        }
        $category = $this->getDoctrine()->getRepository('AppBundle:Category')->find($id);
        if($category === null){
            throw new NotFoundHttpException('This category id was not found');
        }

        if($this->get('app.category.handler')->approveCategory($category)){
            $this->container->get('session')->getFlashBag()->add(
                'success',
                "Category ". $category->getName()." approved successfully."
            );
        } else {
            $this->container->get('session')->getFlashBag()->add(
                'error',
                "Category ". $category->getName()." cannot approve."
            );
        }
        return $this->redirectToRoute($this->route);
    }

    /** Privates */

    private function setAction(Request $request){

        $data = $this->getData();
        $form = $this->renderPage($request, $data);

        if($form instanceof RedirectResponse){
            return $form;
        }

        return $this->render('AppBundle:form:editCategory.html.twig',
            array(
                'form' => $form->createView(),
                'data' => $data,
            )
        );
    }


    private function renderPage(Request $request, $data){

        $form = $this->createForm(EditCategory::class, $this->category, $this->getOptions());
        $form->handleRequest($request);
        if($form->get('cancel')->isClicked()){
            return $this->redirectToRoute($this->route);
        }
        if($form->isValid()){
            /** @var Category $category */
            $category = $form->getData();
            if($this->mode == SELF::MODE_CREATE && ($this->get('app.category.handler')->categoryExists($category))) {
                $this->container->get('session')->getFlashBag()
                    ->add(
                        'error',
                        "Category with name " . $this->category->getName() . " exits in the project "
                    );
            } else {
                $result = $this->get('app.category.handler')->save($category, $this->mode == SELF::MODE_EDIT );
                if($result == true){
                    if ($this->mode == SELF::MODE_CREATE) {
                        $this->container->get('session')->getFlashBag()->add(
                            'success',
                            "Category ". $this->category->getName()." created successfully."
                        );
                    } else {
                        if ($this->mode == SELF::MODE_EDIT) {
                            $this->container->get('session')->getFlashBag()->add(
                                'success',
                                "Category ". $this->category->getName() ." updated successfully."
                            );
                        }
                    }
                    return $this->redirectToRoute($this->route);
                } else {

                    $errorMessage = "Can not save category";
                    $this->container->get('session')->getFlashBag()
                        ->add(
                            'error',
                            $errorMessage
                        );
                    $data['error_messages'] = $errorMessage;
                }
            }
        }
        return $form;
    }

    private function getData(){

        $data['page_title'] = $this->mode == SELF::MODE_CREATE ? "Create Category" : "Edit Category";
        $data['options'] = $this->getOptions();
        return $data;
    }

    private function getOptions() {

        $options = array();
        $options['approval_status'] = $this->get('settings')->getApprovalStatus();
        return $options;
    }


}
