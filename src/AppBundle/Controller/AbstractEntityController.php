<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 12/6/16
 * Time: 9:30 PM
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

abstract class AbstractEntityController extends Controller
{

    const MODE_CREATE  =   'create';
    const MODE_EDIT    =   'edit';

    protected $mode;
    protected $route = '';

}