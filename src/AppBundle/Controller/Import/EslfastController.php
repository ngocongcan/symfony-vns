<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 12/14/16
 * Time: 11:17 AM
 */

namespace AppBundle\Controller\Import;


use AppBundle\Model\EslfastSearch;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Form\ParserHTML\EditEslFast;
use AppBundle\Entity\Dialogue;

class EslfastController extends Controller
{

    /**
     * @param Request $request
     * @return \Symfony\Component\Form\Form|Response
     */
    public function parserAction(Request $request){

        return $this->renderEslFastPage($request);

    }

    private function renderEslFastPage(Request $request){

        $options = array();
        $eslModel = new EslfastSearch();
        $options['project'] = $this->get('app.entity.handler')->getLinkedProject();
        $form = $this->createForm(EditEslFast::class, $eslModel, $options);

        $form->handleRequest($request);
        if($form->get('cancel')->isClicked()){

        }

        if($form->isValid()){
            try {

                $eslModel = $form->getData();
                $result = $this->get('app.eslfast.handler')->parser($eslModel);

                $message = "";

                if(array_key_exists('NewDialogues', $result)){
                    $NewDialogues = $result['NewDialogues'];
                    /** @var Dialogue $dialogue */
                    foreach($NewDialogues as $dialogue){
                        $message = $message . "\n" . $dialogue->getId() . " - " . $dialogue->getName() ;
                    }



                    $message = $this->displayDialogueSuccessMessage($message)  . " has been parsed successfully";

                }
                if(array_key_exists('ExceptionMessage', $result)){
                    $message = $result['ExceptionMessage'];
                }

                $this->container->get('session')->getFlashBag()->add(
                    'success',
                    $message
                );

                $data['result'] = $result;

            } catch (Exception $e) {
                $this->container->get('session')->getFlashBag()
                    ->add(
                        'error',
                        $e->getMessage()
                    );
                $data['error_messages'] = $e->getMessage();
            }
        }

        if($form instanceof RedirectResponse){
            return $form;
        }

        $data['page_title'] = 'Parser Data From EslFast';
        return $this->render('AppBundle:import/inc:form-eslfast-search.html.twig',
            array(
                'form' => $form->createView(),
                'data' => $data,
            )
        );
    }

    private function displayDialogueSuccessMessage($message){

        $path = $this->generateUrl("manageDialogues");
        return "<a href=\"$path\" > $message </a>";
    }


}