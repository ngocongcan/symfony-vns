<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 12/12/16
 * Time: 3:55 PM
 */

namespace AppBundle\Controller\Import\Filters;


use AppBundle\Controller\Manage\Tools\UserInfos;
use AppBundle\Tools\Filters\ManageFilters;
use AppBundle\Tools\Conditions\Condition;
use AppBundle\Tools\QueryHelpers\QueryBuilderTool;
use AppBundle\Tools\Filters\Filter;


class YoutubeVideoFilters extends ManageFilters
{
    public function setFilters(UserInfos $userInfos)
    {

        // TODO: Implement setFilters() method.
        $filters = array();
        /** @Desc("Defaults") */
        $default = 'Defaults';
        $this->addSection($default);
        //		Title
        $condition = new Condition(Condition::INTEXT, 'title', 'yv');
        $text = 'Title';
        $filter = new Filter(Filter::TYPE_TEXT, 'title', $text, '', $condition);
        $filter->setSection($default);
        $filters["title"] = $filter;


        $queryBuilder = new QueryBuilderTool();
        $queryBuilder->addToSelect("DISTINCT c.channel_title as text, c.id as value");
        $queryBuilder->setFrom('youtube_channel', 'c');
        $queryBuilder->addOrderBy('c.channel_title');

        $condition = new Condition(Condition::IN, 'id', 'yc');
        $text = 'Channel';
        $filter = new Filter(Filter::TYPE_SELECT, 'channel_title', $text, array(0), $condition, $queryBuilder);
        $filter->setDefaultText($text);
        $filter->setSection($default);
        $filters["channel_title"] = $filter;


        // Approval Status

        $condition = new Condition(Condition::EQUAL, 'status', 'yv');
        $text = 'Status';
        $filter = new Filter(Filter::TYPE_CHOICE, 'approval_status', $text, -1 , $condition);
        $filter->setDefaultText($text);
        $statues = $this->settings->getYoutubeVideoStatus();
        $options = array();
        foreach ($statues as $key => $value){
            $options[] = array(
                'value' => $key,
                'text' => $value,
            );
        }

        $filter->setValue('-1');
        $filter->setSelectValues($options);
        $filters['approval_status'] = $filter;

        // Get filters values
        $this->getFiltersValues($filters);
        return $filters;

    }


}