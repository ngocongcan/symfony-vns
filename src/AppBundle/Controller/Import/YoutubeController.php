<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 12/12/16
 * Time: 3:31 PM
 */

namespace AppBundle\Controller\Import;

use AppBundle\Controller\Import\Filters\YoutubeVideoFilters;
use AppBundle\Controller\Import\Queries\YoutubeVideoQuery;
use AppBundle\Controller\Manage\ManageController;
use AppBundle\Entity\Dialogue;
use AppBundle\Form\EditDialogue;
use AppBundle\Form\Youtube\SearchVideo;
use AppBundle\Model\YoutubeSearch;
use AppBundle\Services\Settings\SettingsManager;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Exception;

class YoutubeController extends ManageController
{
    protected $recycle_path = 'manageYoutubeVideoRecycle';
    protected $page_path = 'manageYoutubeVideoPage';
    protected $route = 'manageYoutubeVideo';
    protected $dbname = 'youtube_video';
    protected $default_order = "id";


    public function showAction(Request $request){

        $this->init();
        $this->initPageValues();
        $this->setFilters($request);
        $this->columns = $this->getColumns($request);
        return $this->renderPage($request);
    }

    public function linkDialogueAction(Request $request, $id){

    }

    public function deleteAllAction(Request $request){

        $this->get('app.youtube.handler')->deleteAllVideo();
        return $this->redirectToRoute($this->route);
    }

    protected function getDefaultColumns()
    {
        // TODO: Implement getDefaultColumns() method.
        return $this->get('settings')->getYoutubeVideoDefaultColumns();
    }


    private function getData(){

        $data['columns'] = $this->columns;
        $data['rows'] = $this->buildQuery();
        $data["pagination"] = $this->buildPaginationData();
        $data["page"] = $this->page;
        $data['page_path'] = $this->page_path;
        $data['active'] = $this->queryManager->getActive();
        /** Filters */
        $data["filters"] = $this->filterManager->getFiltersBySections($this->filters);
        $data["useFilterSections"] = true;
        $data["activefilters"] = $this->active_filters;
        // Recycle info
        $data["recyclepath"] = $this->recycle_path;
        $data["recyclename"] = 'title';
        $data["recycleinfo"] = 'channel_title';
        $data["recycletype"] = 'Video';

        return $data;

    }

    protected function init()
    {
        // TODO: Implement init() method.
        $rights = array();
        $this->userInfos = $this->get('app.userinfo');
        $this->queryManager = new YoutubeVideoQuery($this->getDoctrine());
        $this->queryManager->setActive(true);
        $this->filterManager = new YoutubeVideoFilters($this->getDoctrine(), $rights, $this->get('settings'));

    }

    protected function renderPage(Request $request)
    {
        $form = $this->createForm(SearchVideo::class, new YoutubeSearch(),array());
        $form->handleRequest($request);
        if($form->isValid()){
            $searchData = $form->getData();
            $this->get('app.youtube.handler')->searchData($searchData);
        }
        $dialogueForm = $this->renderDialogueForm($request);
        if(!$dialogueForm instanceof Form)
        {
            return $dialogueForm;
        }
        // TODO: Implement renderPage() method.
        return $this->render('AppBundle:import:youtube.html.twig',
            array(
                'data' => $this->getData(),
                'form' => $form->createView(),
                'dialogueForm' => $dialogueForm->createView(),
            )
        );
    }

    protected function initFilters()
    {
        // TODO: Implement initFilters() method.
        return $this->filterManager->setFilters($this->userInfos);

    }


    private function buildQuery(){

        return $this->queryManager->buildQuery($this->userInfos);
    }


    /** Dialogue Form */

    public function bookDialogueAction(Request $request){
        $form = $this->renderDialogueForm($request);
        return $form;
    }

    private function renderDialogueForm(Request $request) {

        $options = array(
            'action' => $this->generateUrl('manageYoutubeVideoBookDialogue'),
            'project' => $this->get('app.entity.handler')->getLinkedProject(),
        );
        $dialogue = new Dialogue();
        $form = $this->createForm(EditDialogue::class, $dialogue, $options );
        $form->handleRequest($request);
        /** @Desc("Error!") */
        $errormsg = "Error!";
        /** @Desc("Success!") */
        $successmsg = "Success!";
        if ( $form->isValid()) {
            try {
                /** @var Dialogue $dialogue */
                $dialogue = $form->getData();
                $dialogue->setType(SettingsManager::MEDIA_TYPE_YOUTUTE);
                $dialogue->setApproved(SettingsManager::APPROVAL_STATUS_APPROVED);

                if($this->get('app.dialogue.handler')->dialogueExists($dialogue) )
                {
                    $this->container->get('session')->getFlashBag()->add(
                        'error',
                        $this->get('translator')->trans($dialogue->getName(). " already exists.")
                    );

                } else {
                    $result =  $this->get('app.dialogue.handler')->save($dialogue);
                    if($result){
                        $this->container->get('session')->getFlashBag()->add(
                            'success',
                            $this->get('translator')->trans($dialogue->getName(). " has been saved successfully.")
                        );
                    } else {

                        $this->container->get('session')->getFlashBag()->add(
                            'error',
                            $this->get('translator')->trans($dialogue->getName(). " has not been saved.")
                        );
                    }
                }

                $videoyoutubeid = $request->get('youtube_video_id');
                if($videoyoutubeid){
                    $this->get('app.youtube.handler')->deleteVideo($videoyoutubeid);
                }

                return new JsonResponse(array('message' => $successmsg), 200);

            } catch (Exception $e) {

                return new JsonResponse(array('errorMessage' => $e->getMessage()), 400);
            }
        } elseif($form->isSubmitted() ) {

            return new JsonResponse(
                array(
                    'message' => $errormsg,
                    'form' => $this->renderView('AppBundle:import/inc:form-link-create-dialogue.html.twig',
                        array('form' => $form->createView()
                        ))
                ), 400);
        }
        return $form;
    }




    /**
     * @param $id
     * @param $active
     * @return bool
     */
    protected function recycle($id, $active) {

        $result = $this->get('app.youtube.handler')->deleteVideo($id, $active);
        if($result == true){
            $this->container->get('session')->getFlashBag()->add(
                'success',
                "Video with ".$id. " has been deleted successfully"
            );
        } else {
            $this->container->get('session')->getFlashBag()->add(
                'error',
                "Not able to delete video with id ".$id
            );
        }
        return $result;
    }


}