<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 12/12/16
 * Time: 3:55 PM
 */

namespace AppBundle\Controller\Import\Queries;


use AppBundle\Tools\Conditions\Condition;
use AppBundle\Tools\QueryHelpers\QueryBuilderTool;
use AppBundle\Tools\QueryHelpers\QueryManager;
use AppBundle\Controller\Manage\Tools\UserInfos;

class YoutubeVideoQuery extends QueryManager
{
    public function buildQuery(UserInfos $userInfos = null, $nolimit = false){

        $this->sqlBuilder = new QueryBuilderTool();
        $this->sqlBuilder->addToSelect('yv.id, yv.title, yv.video_id , yv.thumbnails_default,
        yv.description, yv.status,
        yc.channel_id as channel_id, yc.channel_title
        ');
        $this->sqlBuilder->setFrom('youtube_video', 'yv');
        $this->sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'youtube_channel', 'yc', 'yc.id = yv.channel_id');

        foreach($this->conditions as $condition){
            $this->sqlBuilder->addCondition($condition);
        }
        $this->active = true;

        if(!$nolimit) {
            $this->sqlBuilder->setLimit(($this->page * $this->rows_per_page) . "," . $this->rows_per_page);
            $this->executeCountQuery();
            if ($this->total_rows == 0) {
                return array();
            }
        }
        $this->sqlBuilder->addOrderBy($this->order->getField(), $this->order->getOrder());

        return $this->executeQuery($this->sqlBuilder);
    }

}