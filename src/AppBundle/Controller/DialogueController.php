<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 12/7/16
 * Time: 4:40 PM
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Dialogue;
use AppBundle\Form\EditDialogue;
use AppBundle\Services\Settings\SettingsManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Exception;

class DialogueController extends AbstractEntityController
{
    protected $route = "manageDialogues";
    /** @var  Dialogue  The dialogue that is editing */
    private $dialogue ;

    public function createAction(Request $request){

        $this->mode = SELF::MODE_CREATE;
        $this->dialogue = new Dialogue();
        return $this->setAction($request);

    }

    public function editAction(Request $request, $id = null){

        if($id === null){
            throw new NotFoundHttpException('Dialogue id is missing');
        }

        $this->dialogue = $this->getDoctrine()->getRepository('AppBundle:Dialogue')->find($id);

        if($this->dialogue === null){
            throw new NotFoundHttpException('This dialogue id was not found');
        }
        return $this->setAction($request);
    }

    public function approveAction(Request $request, $id = null){

        if($id === null){
            throw new NotFoundHttpException('Dialogue id is missing');
        }
        $entity = $this->getDoctrine()->getRepository('AppBundle:Dialogue')->find($id);
        if($entity === null){
            throw new NotFoundHttpException('This dialogue id was not found');
        }

        if($this->get('app.dialogue.handler')->approve($entity)){
            $this->container->get('session')->getFlashBag()->add(
                'success',
                "Dialogue ". $entity->getName()." approved successfully."
            );
        } else {
            $this->container->get('session')->getFlashBag()->add(
                'error',
                "Dialogue ". $entity->getName()." cannot approve."
            );
        }
        return $this->redirectToRoute($this->route);
    }

    /** Privates */

    private function setAction(Request $request){

        $data = $this->getData();
        $form = $this->renderPage($request, $data);

        if($form instanceof RedirectResponse){
            return $form;
        }

        return $this->render('AppBundle:form:editDialogue.html.twig',
            array(
                'form' => $form->createView(),
                'data' => $data,
            )
        );
    }


    private function renderPage(Request $request, $data){

        $form = $this->createForm(EditDialogue::class, $this->dialogue, $this->getOptions());
        $form->handleRequest($request);
        if($form->get('cancel')->isClicked()){
            return $this->redirectToRoute($this->route);
        }
        if($form->isValid()){
            /** @var Dialogue $dialogue */
            $dialogue = $form->getData();
            if($this->mode == SELF::MODE_CREATE && ($this->get('app.dialogue.handler')->dialogueExists($dialogue))) {
                $this->container->get('session')->getFlashBag()
                    ->add(
                        'error',
                        "Dialogue with name " . $this->dialogue->getName() . " exits in the this category "
                    );
            } else {
                $result = $this->get('app.dialogue.handler')->save($dialogue, $this->mode == SELF::MODE_EDIT );
                if($result == true){
                    if ($this->mode == SELF::MODE_CREATE) {
                        $this->container->get('session')->getFlashBag()->add(
                            'success',
                            "Dialogue ". $this->dialogue->getName()." created successfully."
                        );
                    } else {
                        if ($this->mode == SELF::MODE_EDIT) {
                            $this->container->get('session')->getFlashBag()->add(
                                'success',
                                "Dialogue ". $this->dialogue->getName() ." updated successfully."
                            );
                        }
                    }
                    return $this->redirectToRoute($this->route);
                } else {

                    $errorMessage = "Can not save dialogue";
                    $this->container->get('session')->getFlashBag()
                        ->add(
                            'error',
                            $errorMessage
                        );
                    $data['error_messages'] = $errorMessage;
                }
            }
        }
        return $form;
    }

    private function getData(){

        $data['page_title'] = $this->mode == SELF::MODE_CREATE ? "Create Dialogue" : "Edit Dialogue";
        $data['options'] = $this->getOptions();
        return $data;
    }

    private function getOptions() {

        $options = array();
        $options['approval_status'] = $this->get('settings')->getApprovalStatus();
        $options['type'] = $this->get('settings')->getDialogueTypes();
        $project = $this->get('app.entity.handler')->getLinkedProject();
        $options['project'] = $project ? $project : null;
        return $options;
    }


}