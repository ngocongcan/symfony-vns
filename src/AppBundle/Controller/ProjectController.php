<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 12/28/16
 * Time: 4:01 PM
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Project;
use AppBundle\Form\EditProject;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Exception;


class ProjectController extends AbstractEntityController
{
    protected $route = "manageProjects";
    /** @var  Project  The project that is editing */
    private $project ;

    public function createAction(Request $request){

        $this->mode = SELF::MODE_CREATE;
        $this->project = new Project();
        $this->project->setCreatedBy($this->getUser());
        return $this->setAction($request);

    }

    public function editAction(Request $request, $id = null){

        if($id === null){
            throw new NotFoundHttpException('Project id is missing');
        }

        $this->project = $this->getDoctrine()->getRepository(Project::class)->find($id);

        if($this->project === null){
            throw new NotFoundHttpException('This project id was not found');
        }
        return $this->setAction($request);
    }


    /** Privates */

    private function setAction(Request $request){

        $data = $this->getData();
        $form = $this->renderPage($request, $data);

        if($form instanceof RedirectResponse){
            return $form;
        }

        return $this->render('AppBundle:form:editProject.html.twig',
            array(
                'form' => $form->createView(),
                'data' => $data,
            )
        );
    }


    private function renderPage(Request $request, $data){

        $form = $this->createForm(EditProject::class, $this->project, $this->getOptions());
        $form->handleRequest($request);
        if($form->get('cancel')->isClicked()){
            return $this->redirectToRoute($this->route);
        }
        if($form->isValid()){
            /** @var Project $category */
            $project = $form->getData();
            if($this->mode == SELF::MODE_CREATE && ($this->get('app.project.handler')->projectExists($project))) {
                $this->container->get('session')->getFlashBag()
                    ->add(
                        'error',
                        "Project with name " . $this->project->getName() . " already exists. "
                    );
            } else {
                $result = $this->get('app.project.handler')->save($project, $this->mode == SELF::MODE_EDIT );
                if($result == true){
                    if ($this->mode == SELF::MODE_CREATE) {
                        $this->container->get('session')->getFlashBag()->add(
                            'success',
                            "Project ". $this->project->getName()." created successfully."
                        );
                    } else {
                        if ($this->mode == SELF::MODE_EDIT) {
                            $this->container->get('session')->getFlashBag()->add(
                                'success',
                                "Project ". $this->project->getName() ." updated successfully."
                            );
                        }
                    }
                    return $this->redirectToRoute($this->route);
                } else {

                    $errorMessage = "Can not save Project";
                    $this->container->get('session')->getFlashBag()
                        ->add(
                            'error',
                            $errorMessage
                        );
                    $data['error_messages'] = $errorMessage;
                }
            }
        }
        return $form;
    }

    private function getData(){

        $data['page_title'] = $this->mode == SELF::MODE_CREATE ? "Create Project" : "Edit Project";
        $data['options'] = $this->getOptions();
        return $data;
    }

    private function getOptions() {
        $options = array();
        return $options;
    }


}
