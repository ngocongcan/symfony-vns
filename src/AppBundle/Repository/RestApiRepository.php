<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/27/16
 * Time: 1:21 PM
 */

namespace AppBundle\Repository;


interface RestApiRepository
{
    /**
     * @param $id
     * @return mixed
     */
    public function get($id);

    /**
     * @param bool|true $active
     * @param int $lastUpdated
     * @param int $lastCreated
     * @return mixed
     */
    public function getAll($active = true, $lastUpdated = null, $lastCreated = null);

    /**
     * @param $projectId
     * @param bool|true $active
     * @param int $lastUpdated
     * @param int $lastCreated
     * @return mixed
     */
    public function getAllByProject($projectId = null, $active = null, $approvedStatus, $lastUpdated = null, $lastCreated = null);


}