<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/13/16
 * Time: 11:29 PM
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class ProjectRepository extends EntityRepository
{
    /**
     * @param int $limit
     * @param int $offset
     * @return array
     * [
     * {
     *  "id": 2,
     *  "name": "Francais facile",
     *  "description": "Học tiếng pháp qua video, hội thoại, đoạn văn",
     *  "active": true
     *  }
     * ]
     */
    public function findAllOrderedByName($limit = 20, $offset = 0)
    {
        return $this->getEntityManager()
            ->createQuery(
                "SELECT p.id, p.name, p.description, p.active FROM AppBundle:Project p  ORDER BY p.name ASC"
            )->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getResult();
    }

    /**
     * @param $id
     * @return mixed
     * {
     *  "id": 2,
     *  "name": "Francais facile",
     *  "description": "Học tiếng pháp qua video, hội thoại, đoạn văn",
     *  "active": true
     *  }
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     */
    public function findById($id)
    {
        return $this->getEntityManager()
            ->createQuery(
                "SELECT p.id, p.name, p.description, p.active FROM AppBundle:Project p  WHERE p.id = $id"
            )
            ->getOneOrNullResult();
    }


}