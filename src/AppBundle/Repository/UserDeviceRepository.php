<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/21/16
 * Time: 4:08 PM
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class UserDeviceRepository extends EntityRepository implements RestApiRepository
{
    /**
     * @param $id
     * @return
     * {
     *       "id": 2,
     *       "buildVersion": null,
     *       "releaseVersion": null,
     *       "lastActive": 1466257067,
     *       "deviceIdentifiter": null,
     *       "userId": 3,
     *       "deviceId": 2
     * }
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findById($id){

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('ud.id, ud.accessToken, ud.buildVersion, ud.releaseVersion, ud.lastActive, ud.deviceIdentifiter, u.id as userId, u.apiKey as apiKey, d.id as deviceId')
            ->from('AppBundle:UserDevice', 'ud')
            ->innerJoin('ud.user', 'u')
            ->innerJoin('ud.device', 'd')
            ->where('ud.id = :id')
            ->setParameters(array(
                'id' => $id,
            ))
        ;
        $query = $qb->getQuery();
        return $query->getOneOrNullResult();

    }

    /**
     * @param $limit
     * @param $offset
     * @return array
     * [
     *  {
     *       "id": 2,
     *       "buildVersion": null,
     *       "releaseVersion": null,
     *       "lastActive": 1466257067,
     *       "deviceIdentifiter": null,
     *       "userId": 3,
     *       "deviceId": 2
     *  }
     * ]
     */
    public function findAllOrderById($limit, $offset){

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('ud.id, ud.buildVersion, ud.releaseVersion, ud.lastActive, ud.deviceIdentifiter, u.id as userId, d.id as deviceId')
            ->from('AppBundle:UserDevice', 'ud')
            ->innerJoin('ud.user', 'u')
            ->innerJoin('ud.device', 'd')
            ->orderBy('ud.id', 'ASC')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
        ;
        $query = $qb->getQuery();
        return $query->getResult();

    }

    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('ud.id, ud.accessToken, ud.buildVersion, ud.releaseVersion, ud.lastActive, ud.deviceIdentifiter, u.id as userId, u.apiKey as apiKey, d.id as deviceId')
            ->from('AppBundle:UserDevice', 'ud')
            ->innerJoin('ud.user', 'u')
            ->innerJoin('ud.device', 'd')
            ->where('ud.id = :id')
            ->setParameters(array(
                'id' => $id,
            ))
        ;
        $query = $qb->getQuery();
        return $query->getOneOrNullResult();
    }

    /**
     * @param bool|true $active
     * @param int $lastUpdated
     * @param int $lastCreated
     * @return mixed
     */
    public function getAll($active = true, $lastUpdated = null, $lastCreated = null)
    {
        // TODO: Implement getAll() method.
    }

    /**
     * @param $projectId
     * @param bool|true $active
     * @param int $lastUpdated
     * @param int $lastCreated
     * @return mixed
     */
    public function getAllByProject($projectId = null, $active = null, $approvedStatus, $lastUpdated = null, $lastCreated = null)
    {
        // TODO: Implement getAllByProject() method.
    }


}