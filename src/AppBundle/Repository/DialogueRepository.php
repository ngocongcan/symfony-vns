<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/21/16
 * Time: 7:13 PM
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class DialogueRepository extends EntityRepository implements RestApiRepository
{
    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('d.id, d.name, d.active, d.approved, d.content, d.iconUrl, d.dateCreated, d.dateUpdated, c.id as categoryId')
            ->from('AppBundle:Dialogue', 'd')
            ->leftJoin('d.category', 'c')
            ->where('d.id = :id')
            ->setParameter('id', $id);
        ;

        $query = $qb->getQuery();
        return $query->getOneOrNullResult();
    }

    /**
     * @param bool|true $active
     * @param int $lastUpdated
     * @param int $lastCreated
     * @return mixed
     */
    public function getAll($active = true, $lastUpdated = null, $lastCreated = null)
    {
        // TODO: Implement getAll() method.
    }

    /**
     * @param $projectId
     * @param bool|true $active
     * @param int $lastUpdated
     * @param int $lastCreated
     * @return mixed
     */
    public function getAllByProject($projectId = null, $active = null, $approvedStatus, $lastUpdated = null, $lastCreated = null)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('d.id, d.name, d.active, d.type, d.approved, d.content, d.iconUrl, d.mediaUrl, d.dateCreated, d.dateUpdated,
        c.id as categoryId')
            ->from('AppBundle:Dialogue', 'd')
            ->leftJoin('d.category', 'c')
            ->leftJoin('c.project', 'p')
            ->orderBy('d.id');
        ;

        if($projectId !== null){
            $qb->andWhere('p.id = :projectId');
            $qb->setParameter('projectId', $projectId);
        }
        if($active !== null){
            $qb->andWhere('d.active = :active');
            $qb->setParameter('active', $active);
        }
        if($approvedStatus !== null){
            $qb->andWhere('d.approved = :approvedStatus');
            $qb->setParameter('approvedStatus', $approvedStatus);

        }
        if($lastUpdated !== null){
            $qb->andWhere('d.dateUpdated >= :lastUpdated');
            $qb->setParameter('lastUpdated', $lastUpdated);

        }
        if($lastCreated !== null){
            $qb->andWhere('d.dateCreated >= :lastCreated');
            $qb->setParameter('lastCreated', $lastCreated);
        }
        $query = $qb->getQuery();
        return $query->getResult();
    }



}