<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/21/16
 * Time: 7:12 PM
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class CategoryRepository extends EntityRepository implements RestApiRepository
{

    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('c.id, c.name, c.approved, c.description, c.iconUrl, c.dateCreated, c.dateUpdated')
            ->from('AppBundle:Category', 'c')
            ->where('c.id = :id')
            ->setParameter('id', $id);
        ;

        $query = $qb->getQuery();
        return $query->getOneOrNullResult();
    }

    /**
     * @param bool|true $active
     * @param int $lastUpdated
     * @param int $lastCreated
     * @return mixed
     */
    public function getAll($active = true, $lastUpdated = null, $lastCreated = null)
    {
        // TODO: Implement getAll() method.
    }

    /**
     * @param $projectId
     * @param bool|true $active
     * @param int $lastUpdated
     * @param int $lastCreated
     * @return mixed
     */
    public function getAllByProject($projectId = null, $active = null, $approvedStatus, $lastUpdated = null, $lastCreated = null)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('c.id, c.name, c.active, c.approved, c.description, c.iconUrl, c.dateCreated, c.dateUpdated')
            ->from('AppBundle:Category', 'c')
            ->leftJoin('c.project', 'p')
            ->where('p.active = :pActive')
            ->setParameter('pActive', true);
        ;

        if($projectId !== null){
            $qb->andWhere('p.id = :projectId');
            $qb->setParameter('projectId', $projectId);
        }
        if($active !== null){
            $qb->andWhere('c.active = :active');
            $qb->setParameter('active', $active);
        }
        if($approvedStatus ){
            $qb->andWhere('c.approved = :approvedStatus');
            $qb->setParameter('approvedStatus', $approvedStatus);

        }
        if($lastUpdated !== null){
            $qb->andWhere('c.dateUpdated >= :lastUpdated');
            $qb->setParameter('lastUpdated', $lastUpdated);

        }
        if($lastCreated !== null){
            $qb->andWhere('c.dateCreated >= :lastCreated');
            $qb->setParameter('lastCreated', $lastCreated);
        }
        $query = $qb->getQuery();
        return $query->getResult();
    }

}