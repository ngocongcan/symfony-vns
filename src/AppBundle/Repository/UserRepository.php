<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/14/16
 * Time: 6:00 PM
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use AppBundle\Entity\User;

class UserRepository extends EntityRepository implements RestApiRepository, UserProviderInterface
{
    public function findOneByApiKey($apiKey){

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('u')
            ->from('AppBundle:User', 'u')
            ->where('u.apiKey = :apiKey')
            ->setParameters(array(
                'apiKey' => $apiKey,
            ))
        ;
        $query = $qb->getQuery();
        return $query->getOneOrNullResult();

    }

    public function findAllOrderedByName($limit = 20, $offset = 0)
    {
        return $this->getEntityManager()
            ->createQuery(
                "SELECT u.id, u.username, u.email, u.active FROM AppBundle:User u  ORDER BY u.username ASC"
            )
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getResult();
    }

    public function findById($id)
    {
        return $this->getEntityManager()
            ->createQuery(
                "SELECT u.id, u.username, u.email, u.active FROM AppBundle:User u  WHERE u.id = $id"
            )
            ->getOneOrNullResult();
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return array
     * [
     * {
     *  "id": 2,
     *  "name": "Francais facile",
     *  "description": "Học tiếng pháp qua video, hội thoại, đoạn văn",
     *  "active": true
     *  }
     * ]
     */
    public function findAllOrderedByUserName($limit = 20, $offset = 0)
    {
        return $this->getEntityManager()
            ->createQuery(
                "SELECT u.id, u.username, u.email, u.active FROM AppBundle:User u ORDER BY u.username ASC"
            )->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getResult();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('u.id, u.username, u.email, u.active')
            ->from('AppBundle:User', 'u')
            ->where('u.id = :id')
            ->setParameter('id', $id);
        ;

        $query = $qb->getQuery();
        return $query->getOneOrNullResult();
    }

    /**
     * @param bool|true $active
     * @param int $lastUpdated
     * @param int $lastCreated
     * @return mixed
     */
    public function getAll($active = true, $lastUpdated = null, $lastCreated = null)
    {
        // TODO: Implement getAll() method.
    }

    /**
     * @param $projectId
     * @param bool|true $active
     * @param int $lastUpdated
     * @param int $lastCreated
     * @return mixed
     */
    public function getAllByProject($projectId = null, $active = null, $approvedStatus, $lastUpdated = null, $lastCreated = null)
    {
        // TODO: Implement getAllByProject() method.
    }

    /**
     * Loads the user for the given username.
     *
     * This method must throw UsernameNotFoundException if the user is not
     * found.
     *
     * @param string $username The username
     *
     * @return UserInterface
     *
     * @throws UsernameNotFoundException if the user is not found
     */
    public function loadUserByUsername($username)
    {
        try {
            /* @var $user User */
            $user = $this->findOneBy(array('username' => $username));
        } catch (NoResultException $e) {
            $errMsg = sprintf('Unable to find an active User object identified by "%s".', $username);
            throw new UsernameNotFoundException($errMsg, 0, $e);
        }
        return $user;
    }

    /**
     * Refreshes the user for the account interface.
     *
     * It is up to the implementation to decide if the user data should be
     * totally reloaded (e.g. from the database), or if the UserInterface
     * object can just be merged into some internal array of users / identity
     * map.
     *
     * @param UserInterface $user
     *
     * @return UserInterface
     *
     * @throws UnsupportedUserException if the account is not supported
     */
    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(
                sprintf(
                    'Instances of "%s" are not supported.',
                    $class
                )
            );
        }
        $user = $this->find($user->getId());

        return $user;
    }

    /**
     * Whether this provider supports the given user class.
     *
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return $this->getEntityName() === $class || is_subclass_of($class, $this->getEntityName());

    }


}