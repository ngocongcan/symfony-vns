<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 12/6/16
 * Time: 11:38 PM
 */

namespace AppBundle\Handler;

use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use AppBundle\Entity\User;
use Exception;


class EntityHandler
{
    private $doctrine;
    private $formFactory;
    private $tokenStorage;
    /** @var User $user */
    private $user;

    public function __construct(RegistryInterface $doctrine, FormFactoryInterface $formFactory,TokenStorage $tokenStorage)
    {
        $this->doctrine = $doctrine;
        $this->formFactory = $formFactory;
        $this->tokenStorage = $tokenStorage;
        $this->user = $tokenStorage->getToken() ? $tokenStorage->getToken()->getUser() : null;

    }

    /**
     * @return RegistryInterface
     */
    public function getDoctrine()
    {
        return $this->doctrine;
    }

    /**
     * @return FormFactoryInterface
     */
    public function getFormFactory()
    {
        return $this->formFactory;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return \AppBundle\Entity\Project|null
     */
    public function getLinkedProject(){

        return $this->user ? $this->user->getLinkedProject() : null;

    }

    /**
     * @return array
     */

    public function getProjects(){

        $usersProjects = $this->doctrine->getRepository('AppBundle:UserProject')->findBy(
            array('user'=> $this->user)
        );

        if(is_array($usersProjects) == false){
            return array();
        }

        $projects = array_map(function($o) { return $o->getProject(); }, $usersProjects);

        return $projects;
    }

    /**
     * @return array
     */

    public function getProjectIds(){
        $usersProjects = $this->doctrine->getRepository('AppBundle:UserProject')->findBy(
            array('user'=> $this->user)
        );

        $projectIds = array_map(function($o) { return $o->getProject()->getId(); }, $usersProjects);

        return $projectIds;
    }
    /**
     * @param $entityClass
     * @return mixed
     */
    public function createEntity($entityClass)
    {
        return new $entityClass();
    }


}