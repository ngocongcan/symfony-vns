<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 12/7/16
 * Time: 4:38 PM
 */

namespace AppBundle\Handler;

use AppBundle\Entity\User;
use AppBundle\Repository\UserRepository;
use Exception;

class UserHandler
{
    private $entityHandler;
    private $entityClass;
    /** @var UserRepository  */
    private $userRepository;

    public function __construct(EntityHandler $entityHandler, $entityClass)
    {
        $this->entityHandler = $entityHandler;
        $this->entityClass = $entityClass;
        $this->userRepository = $this->entityHandler->getDoctrine()->getRepository($entityClass);
    }

    public function userExists(User $entity){

        return $this->userRepository->findOneBy(array(
            'username' => $entity->getUsername(),
        ));
    }

    public function save(User $entity, $isUpdated = false){
        $em = $this->entityHandler->getDoctrine()->getManager();
        try {
            $em->getConnection()->beginTransaction();
            if($isUpdated){

            } else {
                $entity->setDateCreated(time());
            }
            $em->persist($entity);
            $em->flush();
            $em->getConnection()->commit();
            return true;

        } catch (Exception $e) {
            $em->getConnection()->rollback();
            return false;
        }
    }



    public function recycle($id, $active) {
        try{
            $em = $this->entityHandler->getDoctrine()->getManager();
            $entity = $this->userRepository->find($id);
            $entity->setActive(!$active);
            $em->persist($entity);
            $em->flush();

        } catch(Exception $e){
            return false;
        }
        return true;
    }

}