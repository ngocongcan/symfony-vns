<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 12/14/16
 * Time: 11:37 AM
 */

namespace AppBundle\Handler;

use AppBundle\Entity\Dialogue;
use AppBundle\Model\EslfastSearch;
use AppBundle\Services\Settings\SettingsManager;
use AppBundle\Tools\HtmlHelper;
use Symfony\Bridge\Doctrine\RegistryInterface;
use PHPHtmlParser\Dom;
use Exception;

class EslfastHandler
{

    /** @var RegistryInterface  */
    private $doctrine;
    private $dialogueRepository;
    /** @var  DialogueHandler */
    private $dialogueHandler;


    public function __construct(RegistryInterface $doctrine, DialogueHandler $dialogueHandler)
    {
        $this->doctrine = $doctrine;
        $this->dialogueRepository = $this->doctrine->getRepository('AppBundle:Dialogue');
        $this->dialogueHandler = $dialogueHandler;

    }

    public function parser(EslfastSearch &$model){
        $result = array();
        try {
            $dom = new Dom();
            $dom->load($model->getUrl());
            $html = $dom->outerHtml;
            $fontsNodes = $dom->find('font');
            $name = null;

            foreach($fontsNodes as $fontNode){

                $fontSize = $fontNode->getAttribute('size');
                if($fontSize == "7") {
                    $name = $fontNode->text;
                    continue;

                } elseif($fontSize != 5) {
                    continue;
                }

                $childDom = new Dom();

                $htmlContent = $fontNode->innerHtml;
                $childDom->load($htmlContent);

                $audioList = array();
                $audioNodes = $childDom->find('audio');
                foreach($audioNodes as $node){
                    $audioURL = $node->getAttribute('src');
                    $audioList[] = $model->getAudioHost() . str_replace('../', '', $audioURL);
                }

                $contentHtml = $childDom->outerHtml;
                $html2Text = new HtmlHelper($contentHtml);
                $allContent = $html2Text->getText();
                $model->setName($name);
                $newDialogues = array();

                $allContent = str_replace("1\n","",$allContent);
                $allContent = str_replace("2\n","",$allContent);
                $allContent = str_replace("3\n","",$allContent);

                $contents = explode("\n Repeat",$allContent);

                if(count($contents) == (count($audioList) + 1))
                    foreach($audioList as $index => $url) {
                        $content = $contents[$index +1];
                        $result = $this->saveNewDialogue($model, $url, $content, $index);
                        if($result instanceof Dialogue){
                            $newDialogues[] = $result;
                        }
                    }

                $result = array(
                    'AudioURL' => $audioList ,
                    'Content' => $contentHtml,
                    'Name' => $name,
                    'URL' => $model->getUrl(),
                    'NewDialogues' => $newDialogues,
                );

            }

        } catch(Exception $e){
            $result['ExceptionMessage'] = $e->getMessage();
        }

        return $result;
    }


    function saveNewDialogue(EslfastSearch $model, $audioURL = '', $content = '' , $index = 0){

        $result = false;
        if($category = $model->getCategory()) {
            $index = $index + 1;
            $dialogue = new Dialogue();
            $name = substr($model->getName(), 3);
            $dialogue->setName($name . ' ' .$index);
            $dialogue->setContent($content);
            $dialogue->setMediaUrl($audioURL);
            $dialogue->setCategory($category);
            $dialogue->setType(SettingsManager::MEDIA_TYPE_AUDIO);
            $dialogue->setApproved(SettingsManager::APPROVAL_STATUS_PENDING_APPROVAL);
            $dialogue->setDateCreated(time());

            if($this->dialogueHandler->dialogueExists($dialogue) == false){
                $result = $this->dialogueHandler->save($dialogue);
            }

            if($result) return $dialogue;
        }
        return $result;
    }

}