<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 12/6/16
 * Time: 11:38 PM
 */

namespace AppBundle\Handler;


use AppBundle\Entity\Category;
use AppBundle\Repository\CategoryRepository;
use AppBundle\Services\Settings\SettingsManager;
use Exception;

class CategoryHandler
{
    private $entityHandler;
    private $entityClass;
    /** @var CategoryRepository  */
    private $categoryRepository;

    public function __construct(EntityHandler $entityHandler, $entityClass)
    {
        $this->entityHandler = $entityHandler;
        $this->entityClass = $entityClass;
        $this->categoryRepository = $this->entityHandler->getDoctrine()->getRepository($entityClass);
    }

    public function categoryExists(Category $category){

        return $this->categoryRepository->findOneBy(array(
            'name' => $category->getName(),
            'project' => $category->getProject(),
        ));
    }

     public function save(Category $entity, $isUpdated = false){
         $em = $this->entityHandler->getDoctrine()->getManager();
         try {
             $em->getConnection()->beginTransaction();
             if($isUpdated){
                 $entity->setDateUpdated(time());
             } else {
                 $entity->setDateUpdated(time());
                 $entity->setDateCreated(time());
             }
             $em->persist($entity);
             $em->flush();
             $em->getConnection()->commit();
             return true;

         } catch (Exception $e) {
             $em->getConnection()->rollback();
             return false;
         }
     }

    public function approveCategory(Category $entity){
        $em = $this->entityHandler->getDoctrine()->getManager();
        try {
            $em->getConnection()->beginTransaction();
            $entity->setApproved(SettingsManager::APPROVAL_STATUS_APPROVED);
            $entity->setDateUpdated(time());
            $em->persist($entity);
            $em->flush();
            $em->getConnection()->commit();
            return true;

        } catch (Exception $e) {
            $em->getConnection()->rollback();
           return false;
        }
    }


    public function recycle($id, $active) {
        try{
            $em = $this->entityHandler->getDoctrine()->getManager();
            $category = $this->categoryRepository->find($id);
            $category->setActive(!$active);
            $category->setDateUpdated(time());
            $em->persist($category);
            $em->flush();

        } catch(Exception $e){
            return false;
        }
        return true;
    }

}