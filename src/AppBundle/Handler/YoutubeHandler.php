<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 12/12/16
 * Time: 2:27 PM
 */

namespace AppBundle\Handler;

use AppBundle\Form\Youtube\SearchVideo;
use AppBundle\Model\YoutubeSearch;
use AppBundle\Services\Settings\SettingsManager;
use Symfony\Bridge\Doctrine\RegistryInterface;
use AppBundle\Entity\YoutubeVideo;
use AppBundle\Entity\YoutubeChannel;
use Google_Client;
use Google_Service_YouTube;
use Google_Service_Exception;
use Google_Exception;
use Google_Service_YouTube_SearchListResponse;
use Google_Service_YouTube_SearchResult;

use Exception;

/**
 *   https://github.com/youtube/api-samples/tree/master/php
 ***/
class YoutubeHandler
{
    /** @var  RegistryInterface  */
    private $doctrine;

    private $videoRepository;

    private $channelRepository;

    const MAX_RESULT = 50;
    const DEFAULT_STATUS = 0;
    const YOUTUBE_API_DEVELOPER_KEY = "AIzaSyDPS2oiB9YQbFXQxt87G5eTW4wy8c6zexs";

    /**
     * YoutubeHandler constructor.
     * @param RegistryInterface $doctrine
     */
    public function __construct(RegistryInterface $doctrine)
    {
        $this->doctrine = $doctrine;
        $this->videoRepository = $this->doctrine->getRepository('AppBundle:YoutubeVideo');
        $this->channelRepository = $this->doctrine->getRepository('AppBundle:YoutubeChannel');
    }


    public function deleteAllVideo(){
        $em = $this->doctrine->getManager();
        try {
            $channels = $this->channelRepository->findAll();
            foreach($channels as $channel){
                $em->remove($channel);
            }
            $em->flush();
            return true;
        } catch (Exception $e){
            return false;
        }
    }

    public function deleteVideo($id) {
        $em = $this->doctrine->getManager();
        try {
            $video = $this->videoRepository->find($id);
            $em->remove($video);
            $em->flush();
            return true;
        } catch (Exception $e){
            return false;
        }
    }

    public function deleteChannel($id) {
        $em = $this->doctrine->getManager();
        try {
            $channel = $this->channelRepository->find($id);
            $em->remove($channel);
            $em->flush();
            return true;
        } catch (Exception $e){
            return false;
        }
    }



    public function searchData(YoutubeSearch $search){
        try{
            /** @var Google_Service_YouTube_SearchListResponse $result */
            if($search->getPlaylistId()){
                $result = $this->getVideoFromPlayList($search->getPlaylistId());
            } else{
                $result = $this->search($search->getSearchText(), $search->getChannelId());
            }

            foreach ($result->getItems() as $item) {
                $this->saveVideoItem($item);
            }

        } catch (Exception $e){

        }
    }

    private function saveVideoItem($item){

        try{
            $em = $this->doctrine->getManager();
            if($item instanceof Google_Service_YouTube_SearchResult){

                $videoId = $item['id']['videoId'];
                if($videoId) {
                    $video = $this->videoRepository->findOneBy(array('videoId' => $videoId));
                    if(is_null($video)){
                        $video = new YoutubeVideo();
                        $video->setVideoId($videoId);
                        $snipped = $item['snippet'];
                        $publishedAt = strtotime($snipped['publishedAt']);
                        $video->setPublishedAt($publishedAt);
                        $video->setTitle($snipped['title']);
                        $video->setDescription($snipped['description']);
                        $video->setStatus(SELF::DEFAULT_STATUS);

                        $thumbnail = $snipped['thumbnails']['default']['url'];
                        $video->setThumbnailsDefault($thumbnail);
                        $channelId = $item['snippet']['channelId'];
                        if($channelId) {
                            $channel = $this->channelRepository->findOneBy(array('channelId' => $channelId));
                            if(is_null($channel)){
                                $channel = new YoutubeChannel();
                                $channel->setChannelId($channelId);
                                $channel->setChannelTitle($snipped['channelTitle']);
                                $em->persist($channel);
                            }
                            $video->setChannel($channel);

                        }

                        $em->persist($video);
                        $em->flush();
                    }
                }


            } else if( $item instanceof \Google_Service_YouTube_PlaylistItem){

                $snipped = $item['snippet'];
                $videoId = $snipped['resourceId']['videoId'];
                if($videoId) {
                    $video = $this->videoRepository->findOneBy(array('videoId' => $videoId));
                    if(is_null($video)){
                        $video = new YoutubeVideo();
                        $video->setVideoId($videoId);
                        $publishedAt = strtotime($snipped['publishedAt']);
                        $video->setPublishedAt($publishedAt);
                        $video->setTitle($snipped['title']);
                        $video->setDescription($snipped['description']);
                        $video->setStatus(SELF::DEFAULT_STATUS);

                        $thumbnail = $snipped['thumbnails']['default']['url'];
                        $video->setThumbnailsDefault($thumbnail);
                        $channelId = $snipped['channelId'];
                        if($channelId) {
                            $channel = $this->channelRepository->findOneBy(array('channelId' => $channelId));
                            if(is_null($channel)){
                                $channel = new YoutubeChannel();
                                $channel->setChannelId($channelId);
                                $channel->setChannelTitle($snipped['channelTitle']);
                                $em->persist($channel);
                            }
                            $video->setChannel($channel);

                        }

                        $em->persist($video);
                        $em->flush();
                    }
                }
            }


        } catch (Exception $e){

        }
    }


    private function search($q = null, $channelId = null){
        $client = new Google_Client();
        $client->setApplicationName("EC Practice");
        $client->setDeveloperKey(SELF::YOUTUBE_API_DEVELOPER_KEY);

        $youtube = new Google_Service_YouTube($client);
        try {
            // Call the search.list method to retrieve results matching the specified
            // query term.
            $parametters = array();
            $parametters['maxResults'] = SELF::MAX_RESULT;
            $parametters['order'] = 'date';
            $parametters['type'] = 'video';
            if(!is_null($q)){
                $parametters['q'] = $q;
            }
            if(!is_null($channelId)){
                $parametters['channelId'] = $channelId;
            }
            $searchResponse = $youtube->search->listSearch('id,snippet', $parametters);

            return $searchResponse;

        } catch (Google_Service_Exception $e) {
            throw $e;
        } catch (Google_Exception $e) {
            throw $e;
        }
    }

    private function getVideoFromPlayList($playlistId){
        $client = new Google_Client();
        $client->setApplicationName("EC Practice");
        $client->setDeveloperKey(SELF::YOUTUBE_API_DEVELOPER_KEY);

        $youtube = new Google_Service_YouTube($client);
        try {
            // Call the search.list method to retrieve results matching the specified
            // query term.
            $parametters = array();
            $parametters['maxResults'] = SELF::MAX_RESULT;
            $parametters['playlistId'] = $playlistId;

            $searchResponse = $youtube->playlistItems->listPlaylistItems('id, snippet', $parametters);

            return $searchResponse;

        } catch (Google_Service_Exception $e) {
            throw $e;
        } catch (Google_Exception $e) {
            throw $e;
        }
    }


}

/*
 *
 *
 *
 *  public function search($q = null, $channelId = null){
        $client = new Google_Client();
        $client->setApplicationName("EC Practice");
        $client->setDeveloperKey("AIzaSyDPS2oiB9YQbFXQxt87G5eTW4wy8c6zexs");
        $results = array();

        $youtube = new Google_Service_YouTube($client);
        try {
            // Call the search.list method to retrieve results matching the specified
            // query term.
            $parametters = array();
            $parametters['maxResults'] = SELF::MAX_RESULT;
            $parametters['order'] = 'date';
            $parametters['type'] = 'video';
            if(!is_null($q)){
                $parametters['q'] = $q;
            }
            if(!is_null($channelId)){
                $parametters['channelId'] = $channelId;
            }
            $searchResponse = $youtube->search->listSearch('id,snippet', $parametters);

            return $searchResponse;
            // Add each result to the appropriate list, and then display the lists of
            // matching videos, channels, and playlists.
            foreach ($searchResponse['items'] as $searchResult) {
                switch ($searchResult['id']['kind']) {
                    case 'youtube#video':
                        $videos .= sprintf('<li>%s (%s)</li>',
                            $searchResult['snippet']['title'], $searchResult['id']['videoId']);
                        break;
                    case 'youtube#channel':
                        $channels .= sprintf('<li>%s (%s)</li>',
                            $searchResult['snippet']['title'], $searchResult['id']['channelId']);
                        break;
                    case 'youtube#playlist':
                        $playlists .= sprintf('<li>%s (%s)</li>',
                            $searchResult['snippet']['title'], $searchResult['id']['playlistId']);
                        break;
                }
            }

            $results['videos'] = $videos;
            $results['channels'] = $channels;
            $results['playlists'] = $playlists;


        } catch (Google_Service_Exception $e) {
            return   htmlspecialchars($e->getMessage());
        } catch (Google_Exception $e) {
            return   htmlspecialchars($e->getMessage());
        }


        return $results;
    }
 *
 */