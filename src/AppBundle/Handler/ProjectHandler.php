<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 12/28/16
 * Time: 3:52 PM
 */

namespace AppBundle\Handler;

use AppBundle\Entity\project;
use AppBundle\Repository\ProjectRepository;
use Exception;

class ProjectHandler
{
    private $entityHandler;
    private $entityClass;
    /** @var ProjectRepository  */
    private $projectRepository;

    public function __construct(EntityHandler $entityHandler, $entityClass)
    {
        $this->entityHandler = $entityHandler;
        $this->entityClass = $entityClass;
        $this->projectRepository = $this->entityHandler->getDoctrine()->getRepository($entityClass);
    }

    public function projectExists(Project $project){

        return $this->projectRepository->findOneBy(array(
            'name' => $project->getName(),
        ));
    }

    public function save(Project $entity, $isUpdated = false){
        $em = $this->entityHandler->getDoctrine()->getManager();
        try {
            $em->getConnection()->beginTransaction();
            if($isUpdated){
            } else {
                $entity->setDateCreated(time());
            }
            $em->persist($entity);
            $em->flush();
            $em->getConnection()->commit();
            return true;

        } catch (Exception $e) {
            $em->getConnection()->rollback();
            return false;
        }
    }


    public function recycle($id, $active) {
        try{
            $em = $this->entityHandler->getDoctrine()->getManager();
            /** @var Project $project */
            $project = $this->projectRepository->find($id);
            $project->setActive(!$active);
            $em->persist($project);
            $em->flush();

        } catch(Exception $e){
            return false;
        }
        return true;
    }

}