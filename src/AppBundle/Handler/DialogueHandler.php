<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 12/7/16
 * Time: 4:35 PM
 */

namespace AppBundle\Handler;

use AppBundle\Entity\Dialogue;
use AppBundle\Repository\DialogueRepository;
use AppBundle\Services\Settings\SettingsManager;
use Exception;
class DialogueHandler
{

    private $entityHandler;
    private $entityClass;
    /** @var DialogueRepository  */
    private $dialogueRepository;

    public function __construct(EntityHandler $entityHandler, $entityClass)
    {
        $this->entityHandler = $entityHandler;
        $this->entityClass = $entityClass;
        $this->dialogueRepository = $this->entityHandler->getDoctrine()->getRepository($entityClass);
    }

    public function dialogueExists(Dialogue $entity){

        $result1 =  $this->dialogueRepository->findOneBy(array(
            'name' => $entity->getName(),
            'category' => $entity->getCategory(),
        ));

        $result2 =  $this->dialogueRepository->findOneBy(array(
            'mediaUrl' => $entity->getMediaUrl(),
            'category' => $entity->getCategory(),
        ));

        $result = $result1 || $result2;

        return $result;
    }

    public function save(Dialogue $entity, $isUpdated = false){
        $em = $this->entityHandler->getDoctrine()->getManager();
        try {
            $em->getConnection()->beginTransaction();
            if($isUpdated){
                $entity->setDateUpdated(time());
            } else {
                $entity->setDateUpdated(time());
                $entity->setDateCreated(time());
            }
            $em->persist($entity);
            $em->flush();
            $em->getConnection()->commit();
            return true;

        } catch (Exception $e) {
            $em->getConnection()->rollback();
            return false;
        }
    }

    public function approve(Dialogue $entity){
        $em = $this->entityHandler->getDoctrine()->getManager();
        try {
            $em->getConnection()->beginTransaction();
            $entity->setApproved(SettingsManager::APPROVAL_STATUS_APPROVED);
            $entity->setDateUpdated(time());
            $em->persist($entity);
            $em->flush();
            $em->getConnection()->commit();
            return true;

        } catch (Exception $e) {
            $em->getConnection()->rollback();
            return false;
        }
    }


    public function recycle($id, $active) {
        try{
            $em = $this->entityHandler->getDoctrine()->getManager();
            $entity = $this->dialogueRepository->find($id);
            $entity->setActive(!$active);
            $entity->setDateUpdated(time());
            $em->persist($entity);
            $em->flush();

        } catch(Exception $e){
            return false;
        }
        return true;
    }

}