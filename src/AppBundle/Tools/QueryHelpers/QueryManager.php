<?php

/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 5/26/16
 * Time: 5:55 PM
 */

namespace AppBundle\Tools\QueryHelpers;

use AppBundle\Controller\Manage\Tools\UserInfos;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\DBAL\Statement;
use AppBundle\Tools\OrderTool;
use PDO;

abstract class QueryManager
{

    /** @var Registry */
    protected $doctrine;

    /** @var PDO */
    private $pdo;

    /** @var QueryBuilderTool */
    protected $sqlBuilder;

    protected $active = 1;

    protected $page = 0;

    protected $total_rows = 0;

    protected $rows_per_page = 50;

    protected $max_pages = 0;

    /**
     * @var OrderTool $order
     */
    protected $order;
    /**
     * @var array
     */
    protected $conditions = array();


    public function __construct($doctrine) {
        $this->doctrine = $doctrine;
        $this->pdo = $doctrine->getConnection();
    }

    abstract public function buildQuery(UserInfos $userInfos = null);


    public function getPage() {
        return $this->page;
    }

    public function setPage($page) {
        $this->page = $page;
    }

    public function getTotalRows() {
        return $this->total_rows;
    }

    public function setTotalRows($total_rows) {
        $this->total_rows = $total_rows;
    }

    public function getRowsPerPage() {
        return $this->rows_per_page;
    }

    public function setRowsPerPage($rows_per_page) {
        $this->rows_per_page = $rows_per_page;
    }

    public function getMaxPages() {
        return $this->max_pages;
    }

    public function setMaxPages($max_pages) {
        $this->max_pages = $max_pages;
    }

    public function getConditions() {
        return $this->conditions;
    }

    public function setConditions($conditions) {
        $this->conditions = $conditions;
    }

    public function addCondition($condition) {
        $this->conditions[] = $condition;
    }

    public function getOrder() {
        return $this->order;
    }

    public function setOrder(OrderTool $order) {
        $this->order = $order;
    }

    /**
     * @return int
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param int $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }


    /**
     * Execute a count query
     */
    protected function executeCountQuery() {
        $countSql = $this->sqlBuilder->getCountQuery();
        if (!empty($countSql)) {
            $this->executeCountQueryStatement($countSql);
        }
    }

    protected function executeLightweightCountQuery() {
        $countSql = $this->sqlBuilder->getLightweightCountQuery();
        if (!empty($countSql)) {
            $this->executeCountQueryStatement($countSql);
        }
    }

    protected function executeCountQueryStatement($countSql) {
        if (!empty($countSql)) {
            /* @var $sth Statement */
            $sth = $this->pdo->prepare($countSql);
            $this->sqlBuilder->bindParameters($sth);
            $sth->execute();

            $result = $sth->fetch(PDO::FETCH_NUM);
            $this->total_rows = $result[0];

            //Max number of pages
            $this->max_pages = ceil($this->total_rows / $this->rows_per_page);

        }
    }

    /**
     * Execute sql queries
     * @param QueryBuilderTool $sqlBuilder
     * @return array
     */
    protected function executeQuery($sqlBuilder) {
        $result = array();
        $sql = $sqlBuilder->getQuery();
        if (!empty($sql)) {
            /* @var $sth Statement */
            $sth = $this->pdo->prepare($sql);
            $sqlBuilder->bindParameters($sth);
            $sth->execute();
            $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        }
        return $result;
    }

    protected function getSqlQueryWithParams($sql, $params) {
        foreach ($params as $param) {

            $sql = str_replace(array(
                "{$param->label},",
                "{$param->label} ",
                "{$param->label})"
            ), array(
                "{$param->value},",
                "{$param->value} ",
                "{$param->value})"
            ), $sql);
        }
        return $sql;
    }

}