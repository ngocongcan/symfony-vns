<?php

/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 5/26/16
 * Time: 5:55 PM
 */

namespace AppBundle\Tools\QueryHelpers;

use AppBundle\Tools\Conditions\Condition;
//use Facile\DoctrineMySQLComeBack\Doctrine\DBAL\Statement;
use Doctrine\DBAL\Statement;

class QueryBuilderTool
{

    const LEFTJOIN = "LEFT JOIN";
    const INNERJOIN = "INNER JOIN";
    const ORDERASC = "ASC";
    const ORDERDESC = "DESC";
    const CONNECTAND = " AND ";
    const CONNECTOR = " OR ";
    private $select = "";
    private $bind_params = array();

    /** @var $conditions Condition[] */
    private $conditions = array();
    private $from = "";
    private $joins = array();
    private $groupBy = "";
    private $orderBy = "";
    private $limit = "";
    private $from_alias = "";
    private $count_select = "";

    /**
     * Add select fields
     * @param string $select
     */
    public function addToSelect($select) {
        $this->select .= empty($this->select) ? "SELECT $select" : ", $select";
    }

    public function addToCountSelect($select){
        $this->count_select .= empty($this->count_select) ? "$select" : ", $select";
    }

    /**
     * Add a parameter
     * @param string $label
     * @param $value
     */
    public function addParameters($label, $value) {
        $param = new \stdClass();
        $param->label = $label;
        $param->value = $value;
        $this->bind_params[] = $param;
    }

    /**
     * Add a condition of type integer
     * @param string $field
     * @param string $alias
     * @param $value
     * @param (EQUAL / NOTEQUAL)
     * @param $connect (CONNECTAND / CONNECTOR)
     */
    public function addIntCondition($field, $alias, $value, $type = Condition::EQUAL , $connect = QueryBuilderTool::CONNECTAND) {
        $label = ":value".(1+count($this->conditions) );
        $this->addParameters($label, $value);
        $condition = new Condition($type, $field, $alias, $label);
        $condition->setConnectType($connect);
        $this->conditions[] = $condition;
    }

    public function addCondition(Condition $condition, $isgroup = false) {
        if ($condition->getType() == Condition::NO_TYPE) {
            foreach ($condition->getParameters() as $parameter) {
                $this->addParameters($parameter->label, $parameter->value);
            }
        } else if($condition->getType() != Condition::FUNC && $condition->getType() != Condition::ISNULL && $condition->getType() != Condition::ISNOTNULL && $condition->getType() != Condition::NOPARAM) {
            if($condition->getType() == Condition::TEXT || $condition->getType() == Condition::TEXTINTEXT) {
                $label = ":".$condition->getField();
            } elseif($condition->getType() == Condition::GROUP) {
                foreach($condition->getGroup() as $cpt => $group_condition) {
                    if($group_condition->getType() == Condition::GROUP) {
                        $this->addCondition($group_condition, true);
                    } else {
                        $label = $condition->getLabel()."gvalue$cpt";
                        $group_condition->setLabel($label);
                        if($group_condition->getType() != Condition::TEXT && $group_condition->getType() != Condition::TEXTINTEXT && $group_condition->getType() != Condition::ISNULL) {
                            $this->addParameters($label, $group_condition->getValue());
                        }
                    }
                }
            } elseif($condition->getType() == Condition::IN || $condition->getType() == Condition::NOTIN || $condition->getType() == Condition::TEXTIN) {
                $cndLabel = $condition->getLabel();
                if(is_array($cndLabel) ) {
                    $i = 0;
                    foreach($condition->getValue() as $cpt => $value) {
                        $label = $cndLabel[$i];
                        $this->addParameters($label, $value);
                        $i++;
                    }
                } else {
                    $condition_label = array();
                    foreach($condition->getValue() as $cpt => $value) {
                        $label =  $condition->getLabel()."invalue".$cpt;
                        $condition_label[] = $label;
                        $this->addParameters($label, $value);
                    }
                    $condition->setLabel($condition_label);
                }
            } else {
                $label = $condition->getLabel();
                if (empty($label)) {
                    $label = ":value" . (1 + count($this->conditions));
                }
            }
            if($condition->getType() != Condition::GROUP && $condition->getType() != Condition::IN && $condition->getType() != Condition::NOTIN && $condition->getType() != Condition::TEXTIN) {
                $condition->setLabel($label);
                $this->addParameters($label, $condition->getValue());
            }
        }
        if(!$isgroup) {
            $this->conditions[] = $condition;
        }
    }


    /**
     * Set from part of the query
     * @param string $tablename (or from string)
     * @param string $alias
     */
    public function setFrom($tablename, $alias) {
        $this->from_alias = ($alias != "") ? $alias : $tablename;
        $this->from = " FROM $tablename $alias ";
    }

    /**
     * Add a join to the query
     * @param $join_type (LEFTJOIN or INNERJOIN)
     * @param string $tablename
     * @param string $alias
     * @param string $condition
     */
    public function addJoins($join_type, $tablename, $alias, $condition) {
        $this->joins[] = empty($condition) ? " $join_type $tablename $alias " : " $join_type $tablename $alias ON $condition ";
    }

    /**
     * Add a groupby field
     * @param string $field
     * @param string $alias
     */
    public function addGroupBy($field, $alias) {
        $groupby = empty($alias) ? $field : "$alias.$field";
        $this->groupBy .= empty($this->groupBy) ? " GROUP BY $groupby" : ", $groupby";
    }

    /**
     * Set a groupby field
     * @param string $field
     * @param string $alias
     */
    public function setGroupBy($field, $alias) {
        $groupby = empty($alias) ? $field : "$alias.$field";
        $this->groupBy = " GROUP BY $groupby ";
    }

    /**
     * Add a order by field
     * @param string $field
     * @param $order
     */
    public function addOrderBy($field, $order = QueryBuilderTool::ORDERASC) {
        if (!empty($field) && !empty($order)) {
            $this->orderBy .= empty($this->orderBy) ? " ORDER BY $field $order" : ", $field $order";
        }
    }

    /**
     * Set the limit of the query
     * @param integer $limit
     */
    public function setLimit($limit){
        $this->limit = " LIMIT $limit";
    }

    private function addWhereConditionsToQuery($query){
        $last_condition = null;
        if(!empty($this->conditions) ){
            $cpt = 0;
            while($cpt < count($this->conditions)) {
                if($this->conditions[$cpt]->getFilterType() == Condition::FILTER_WHERE) {
                    if(isset($last_condition) ) {
                        $query .= $last_condition->getConnectType();
                    } else {
                        $query .= " WHERE ";
                    }

                    $last_condition = $this->conditions[$cpt];
                    $query .= $this->conditions[$cpt]->getCondition();
                }
                $cpt ++;
            }
        }
        return $query;
    }

    private function addHavingConditionsToQuery($query){
        // Add having conditions
        if(!empty($this->conditions) ){
            $cpt = 0;
            $last_condition = null;
            while($cpt < count($this->conditions)) {
                if($this->conditions[$cpt]->getFilterType() == Condition::FILTER_HAVING) {
                    if(isset($last_condition) ) {
                        $query .= $last_condition->getConnectType();
                    } else {
                        $query .= " HAVING ";
                    }
                    $last_condition = $this->conditions[$cpt];
                    $query .= $this->conditions[$cpt]->getCondition();
                }
                $cpt ++;
            }
        }
        return $query;
    }
    /**
     * Get the query
     * @return string
     */
    public function getQuery() {
        $query = $this->select . $this->from;
        foreach($this->joins as $join) {
            $query .= $join;
        }

        $query = $this->addWhereConditionsToQuery($query);

        $query .= $this->groupBy;

        $query = $this->addHavingConditionsToQuery($query);

        $query .= $this->orderBy;
        $query .= $this->limit;

        return $query;
    }

    public function getCountQuery() {
        $query = "";
        if(!empty($this->select) ) {
            $query = "SELECT COUNT(*) AS recordCount FROM (". $this->select . $this->from;
            foreach($this->joins as $join) {
                $query .= $join;
            }

            // add where conditions
            $query = $this->addWhereConditionsToQuery($query);

            $query .= $this->groupBy;

            // Add having conditions
            $query = $this->addHavingConditionsToQuery($query);

            $query .= ") as count GROUP BY NULL";
        }
        return $query;
    }

    public function getLightweightCountQuery() {
        $query = "";
        if(!empty($this->select) ) {
            $query = "SELECT COUNT(count.id) AS recordCount FROM (SELECT {$this->from_alias}.id";
            if(!empty($this->count_select) ) {
                $query .= ", ".$this->count_select;
            }
            $query .= " {$this->from}";
            foreach($this->joins as $join) {
                $query .= $join;
            }

            // add where conditions
            $query = $this->addWhereConditionsToQuery($query);

            $query .= $this->groupBy;

            // Add having conditions
            $query = $this->addHavingConditionsToQuery($query);

            $query .= ") as count GROUP BY NULL";
        }
        return $query;
    }

    public function getParameters() {
        return $this->bind_params;
    }

    public function setParameters($params) {
        $this->bind_params = $params;
    }

    public function bindParameters(Statement $sth) {
        foreach($this->bind_params as $param){
            $sth->bindParam($param->label, $param->value);
        }
    }


}