<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/11/16
 * Time: 5:07 PM
 */

namespace RestApiBundle\Exception;

/**
 * Class InvalidBatchActionException
 * @package RestApiBundle\Exception
 *
 */
class InvalidBatchActionException extends  RestApiException
{

}