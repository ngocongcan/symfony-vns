<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/11/16
 * Time: 11:52 AM
 */

namespace RestApiBundle\Exception;
use Symfony\Component\Form\FormInterface;

class InvalidFormException extends RestApiException
{
    protected $form;

    /**
     * InvalidFormException constructor.
     * @param string             $message
     * @param FormInterface|null $form
     */
    public function __construct($message, FormInterface $form = null) {
        parent::__construct($message);
        $this->form = $form;
    }

    /**
     * @return FormInterface|null
     */
    public function getForm() {
        return $this->form;
    }


    public function getFormMessage(){

        $string = (string) $this->form->getErrors(true, false);
        return $string;
    }
}