<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/14/16
 * Time: 2:56 PM
 */

namespace RestApiBundle\EventListener;


use RestApiBundle\Exception\InvalidFormException;
use RestApiBundle\Exception\RestApiException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Event\KernelEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use RestApiBundle\Controller\BaseController;

class ApiExceptionSubscriber implements EventSubscriberInterface
{

    /** @var  BaseController */
    private $restBaseController;

    /**
     * ApiExceptionSubscriber constructor.
     * @param BaseController $restBaseController
     */
    public function __construct(BaseController $restBaseController)
    {
        $this->restBaseController = $restBaseController;
    }


    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $e = $event->getException();
        $pathInfo = $event->getRequest()->getPathInfo();
        if(!strpos($pathInfo, 'api/v1')){
            return;
        }
        $errorMessage = $e->getFile() .' '. ' '. $e->getLine(). ' '. $e->getMessage();
        if($e instanceof InvalidFormException){
            $errorMessage = $e->getFormMessage();
        }

        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if($e->getCode() == JsonResponse::HTTP_NOT_FOUND || $e->getCode() == JsonResponse::HTTP_FORBIDDEN){
            $code = $e->getCode();
        }
        $result = $this->restBaseController->handleResult(null, $errorMessage );

        $response = new JsonResponse($result, $code);
        $response->headers->set('Content-Type', 'application/json');

        $event->setResponse($response);


    }

    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::EXCEPTION => 'onKernelException'
        );
    }


}