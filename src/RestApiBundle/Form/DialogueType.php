<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/28/16
 * Time: 12:01 PM
 */

namespace RestApiBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DialogueType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('name', 'text', array(
                'trim' => true,
                'required' => true,
            ))
            ->add('content','textarea', array(
                'trim' => true,
                'required' => false,
            ))
            ->add('contentUrl', 'url', array(
                'trim' => true,
                'required' => false,
            ))->add('mediaUrl', 'url', array(
                'trim' => true,
                'required' => false,
            ))->add('iconUrl', 'url', array(
                'trim' => true,
                'required' => false,
            ))->add('category', 'entity', array(
                'class' => 'AppBundle:Category',
                'property' => 'id',
            ))
            ->add('approved', 'integer', array(
                'required' => false,
            ))
        ;
    }

    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Dialogue',
            'csrf_protection' => false,
        ));
    }


    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return '';
    }



}