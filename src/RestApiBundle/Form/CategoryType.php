<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/28/16
 * Time: 10:56 AM
 */

namespace RestApiBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('name', 'text', array(
                'trim' => true,
                'required' => true,
            ))
            ->add('description','text', array(
                'trim' => true,
                'required' => false,
            ))
            ->add('iconUrl', 'url', array(
                'trim' => true,
                'required' => false,
            ))
            ->add('approved', 'integer', array(
                'required' => false,
            ))
        ;
    }

    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Category',
            'csrf_protection' => false,
        ));
    }


    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return '';
    }


}