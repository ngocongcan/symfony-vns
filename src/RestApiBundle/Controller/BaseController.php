<?php

namespace RestApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class BaseController extends FOSRestController
{
    public function handleResult($result = null, $errorMessage = null){
        $response = array();
        if(!$errorMessage){
            $response['success'] = true;
            $response['data'] = $result;
        } else {
            $response['success'] = false;
            $response['errorMessage'] = $errorMessage;
        }

        return $response;
    }

}
