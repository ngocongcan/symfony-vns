<?php
/**
 * Created by PhpStorm.
 * category: canngo
 * Date: 11/25/16
 * Time: 12:26 PM
 */

namespace RestApiBundle\Controller;


use RestApiBundle\Exception\RestApiException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcherInterface;

use Symfony\Component\Form\FormTypeInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as Rest;
use RestApiBundle\Exception\InvalidFormException;
use AppBundle\Entity\Category;

class CategoryController extends BaseController
{

    /**
     * Get list category
     *
     * /**
     * ### Sample output ###
     *     [
     *       {
     *         "id": 123,
     *         "name": "Faits divers",
     *         "active": 1,
     *         "approved": 2,
     *         "description": null
     *         "iconUrl": "http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/texte-04.gif"
     *         "dateCreated": 1469523951
     *         "dateUpdated": 1471845147
     *         "dialogues": [
     *           {
     *           "id": 176,
     *           "name": "POKEMON GO LETS PLAY! EPISODE 1",
     *           "active": 1,
     *           "type": "youtube",
     *           "approved": 2,
     *           "iconUrl": "http://i3.ytimg.com/vi/4pgX60AAIYo/hqdefault.jpg",
     *           "mediaUrl": "https://www.youtube.com/watch?v=4pgX60AAIYo",
     *           "dateCreated": 1470964672,
     *           "dateUpdated": null,
     *           "categoryId": 29
     *           }
     *          ]
     *       }
     *     ]
     *
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Returns the list of categories according to the passed parameters.",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     403 = "Forbidden",
     *     500 = "Internal server error"
     *   },
     *  headers={
     *     {
     *       "name"="api-key",
     *       "description"="Authorization key",
     *       "required"=true
     *     }
     *   },
     * )
     *
     * @Annotations\QueryParam(name="approvedStatus", requirements="\d+", default="2", description="The status of category, default is approved.")
     * @Annotations\QueryParam(name="accessToken", nullable=false, description="The unique access token of User and Device")
     *
     * @Annotations\View()
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getCategoriesDialoguesAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $approvedStatus = $paramFetcher->get('approvedStatus');
        $accessToken = $paramFetcher->get('accessToken');
        $result = $this->get('category.handler')->allWithDialogues($approvedStatus, $accessToken);
        return $this->handleResult($result);

    }


    /**
     * Get list category
     *
     * /**
     * ### Sample output ###
     *     [
     *       {
     *         "id": 123,
     *         "name": "Faits divers",
     *         "active": 1,
     *         "approved": 2,
     *         "description": null
     *         "iconUrl": "http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/texte-04.gif"
     *         "dateCreated": 1469523951
     *         "dateUpdated": 1471845147
     *       }
     *     ]
     *
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Returns the list of categories according to the passed parameters.",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     403 = "Forbidden",
     *     500 = "Internal server error"
     *   },
     *  headers={
     *     {
     *       "name"="api-key",
     *       "description"="Authorization key",
     *       "required"=true
     *     }
     *   },
     * )
     *
     * @Annotations\QueryParam(name="approvedStatus", requirements="\d+", default="2", description="The status of category, default is approved.")
     * @Annotations\QueryParam(name="lastUpdated", requirements="\d+", nullable=true, description="The last time that categories were updated.")
     * @Annotations\QueryParam(name="lastCreated", requirements="\d+", nullable=true, description="The last time that categories were created.")
     * @Annotations\QueryParam(name="accessToken", nullable=false, description="The unique access token of User and Device")
     * @Annotations\View()
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getCategoriesAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $approvedStatus = $paramFetcher->get('approvedStatus');
        $lastUpdated = $paramFetcher->get('lastUpdated');
        $lastCreated = $paramFetcher->get('lastCreated');
        $accessToken = $paramFetcher->get('accessToken');

        $result = $this->get('category.handler')->all($approvedStatus, $lastUpdated, $lastCreated, $accessToken);
        return $this->handleResult($result);

    }


    /**
     * Get single category.
     *
     *
     * ### Sample output ###
     *
     *       {
     *         "id": 123,
     *         "name": "Faits divers",
     *         "active": 1,
     *         "approved": 2,
     *         "description": null
     *         "iconUrl": "http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/texte-04.gif"
     *         "dateCreated": 1469523951
     *         "dateUpdated": 1471845147
     *       }
     *
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Gets a category for a given id",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the category is not found",
     *     403 = "Forbidden",
     *     500 = "Internal server error",
     *   },
     *  headers={
     *     {
     *       "name"="api-key",
     *       "description"="Authorization key",
     *       "required"=true
     *     }
     *   },
     * )
     *
     * @Annotations\View()
     *
     * @param int     $id      the category id
     *
     * @return array
     *
     * @throws RestApiException when category not exist
     */

    public function getCategoryAction($id)
    {
        $result = $this->getOr404($id, true);
        return $this->handleResult($result);
    }

    /**
     * Create a category from the submitted data.
     * ### Sample output ###
     *
     *       {
     *         "id": 123,
     *         "name": "Faits divers",
     *         "active": 1,
     *         "approved": 2,
     *         "description": null
     *         "iconUrl": "http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/texte-04.gif"
     *         "dateCreated": 1469523951
     *         "dateUpdated": 1471845147
     *       }
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Creates a new category from the submitted data.",
     *   input = "RestApiBundle\Form\CategoryType",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     403 = "Forbidden",
     *     500 = "Internal server error",
     *   },
     *  headers={
     *     {
     *       "name"="api-key",
     *       "description"="Authorization key",
     *       "required"=true
     *     }
     *   },
     * )
     * @Annotations\View()
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     * @throws InvalidFormException
     */
    public function postCategoryAction(Request $request)
    {
        try {
            $parameters = $request->request->all();
            $result = $this->get('category.handler')->post(
                array_filter($parameters), true
            );

            return $this->handleResult($result);

        } catch (InvalidFormException $exception) {
            throw $exception;
        }
    }

    /**
     * Update existing category from the submitted data or create a new Category at a specific location.
     * ### Sample output ###
     *
     *       {
     *         "id": 123,
     *         "name": "Faits divers",
     *         "active": 1,
     *         "approved": 2,
     *         "description": null
     *         "iconUrl": "http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/texte-04.gif"
     *         "dateCreated": 1469523951
     *         "dateUpdated": 1471845147
     *       }
     *
     * @ApiDoc(
     *   resource = true,
     *   input = "RestApiBundle\Form\CategoryType",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the category is not found",
     *     403 = "Forbidden",
     *     500 = "Internal server error",
     *   },
     *  headers={
     *     {
     *       "name"="api-key",
     *       "description"="Authorization key",
     *       "required"=true
     *     }
     *   },
     * )
     *
     * @Annotations\View()
     * @param Request $request the request object
     * @param int     $id      the category id
     *
     * @return FormTypeInterface|View
     *
     * @throws RestApiException when category not exist
     */
    public function putCategoryAction(Request $request, $id)
    {
        try {
            $parameters = $request->request->all();
            if (!($category = $this->get('category.handler')->get($id))) {
                $result = $this->get('category.handler')->post(
                    array_filter($parameters), true
                );
            } else {
                $result = $this->get('category.handler')->put(
                    $category,
                    $request->request->all(), true
                );
            }
            return $this->handleResult($result);

        } catch (InvalidFormException $exception) {

            throw $exception;
        }
    }

    /**
     * Fetch a category or throw an 404 Exception.
     *
     * @param mixed $id
     *
     * @return category
     *
     * @throws RestApiException
     */
    protected function getOr404($id, $isFiltered = false)
    {
        if (!($category = $this->get('category.handler')->get($id, $isFiltered))) {
            throw new RestApiException(sprintf('The resource \'%s\' was not found.',$id), Response::HTTP_NOT_FOUND);
        }

        return $category;
    }

}