<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/23/16
 * Time: 8:42 PM
 */

namespace RestApiBundle\Controller;

use RestApiBundle\Exception\RestApiException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcherInterface;

use Symfony\Component\Form\FormTypeInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as Rest;
use RestApiBundle\Exception\InvalidFormException;
use AppBundle\Entity\User;

class UserController extends BaseController
{


    /**
     * Get list Videos
     *
     * /**
     *
     * ### Sample output ###
     *     [
     *       {
     *         "id": 123,
     *         "username": "ngocongcan",
     *         "email": null,
     *         "active": true
     *       }
     *     ]
     *
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Returns the list of users according to the passed parameters.",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     403 = "Forbidden",
     *     500 = "Internal server error"
     *   },
     *  headers={
     *     {
     *       "name"="api-key",
     *       "description"="Authorization key",
     *       "required"=true
     *     }
     *   },
     * )
     *
     *
     * @Annotations\View()
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */

    public function getYoutubeVideosAction(Request $request)
    {
        try {
            $result = $this->get('youtube.handler')->load();

            return $this->handleResult($result);

        } catch (InvalidFormException $exception) {
            throw $exception;
        }
    }


    /**
     * Get list User
     *
     * /**
     *
     * ### Sample output ###
     *     [
     *       {
     *         "id": 123,
     *         "username": "ngocongcan",
     *         "email": null,
     *         "active": true
     *       }
     *     ]
     *
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Returns the list of users according to the passed parameters.",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     403 = "Forbidden",
     *     500 = "Internal server error"
     *   },
     *  headers={
     *     {
     *       "name"="api-key",
     *       "description"="Authorization key",
     *       "required"=true
     *     }
     *   },
     * )
     *
     * @Annotations\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing pages.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default="20", description="How many users to return.")
     *
     * @Annotations\View(
     *  templateVar="users"
     * )
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getUsersAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $offset = $paramFetcher->get('offset');
        $offset = null == $offset ? 0 : $offset;
        $limit = $paramFetcher->get('limit');

        $result = $this->get('user.handler')->all($limit, $offset);
        return $this->handleResult($result);

    }


    /**
     * Get single User.
     *
     *
     * ### Sample output ###
     *
     *       {
     *         "id": 123,
     *         "username": "ngocongcan",
     *         "email": null,
     *         "active": true
     *       }
     *
     *
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Gets a User for a given id",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the user is not found",
     *     403 = "Forbidden",
     *     500 = "Internal server error",
     *   },
     *  headers={
     *     {
     *       "name"="api-key",
     *       "description"="Authorization key",
     *       "required"=true
     *     }
     *   },
     * )
     *
     * @Annotations\View(templateVar="user")
     *
     * @param int     $id      the user id
     *
     * @return array
     *
     * @throws RestApiException when user not exist
     */

    public function getUserAction($id)
    {
        $result =  $this->getOr404($id, true);
        return $this->handleResult($result);

    }

    /**
     * Create a User from the submitted data.
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Creates a new user from the submitted data.",
     *   input = "RestApiBundle\Form\UserType",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     403 = "Forbidden",
     *     500 = "Internal server error",
     *   },
     *  headers={
     *     {
     *       "name"="api-key",
     *       "description"="Authorization key",
     *       "required"=true
     *     }
     *   },
     *  parameters = {
     *         { "name" = "projectId", "required" = "required", "dataType" = "integer", "format" = "" },
     *     }
     * )
     * @Annotations\View()
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     * @throws InvalidFormException
     */
    public function postUserAction(Request $request)
    {
        try {
            $parameters = $request->request->all();
            $parameters['ipAddress'] = $request->getClientIp();
            $result = $this->get('user.handler')->post(
                array_filter($parameters), true
            );

            return $this->handleResult($result);

        } catch (InvalidFormException $exception) {
            throw $exception;
        }
    }

    /**
     * Update existing user from the submitted data or create a new user at a specific location.
     *
     * @ApiDoc(
     *   resource = true,
     *   input = "RestApiBundle\Form\UserType",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the user is not found",
     *     403 = "Forbidden",
     *     500 = "Internal server error",
     *   },
     *  headers={
     *     {
     *       "name"="api-key",
     *       "description"="Authorization key",
     *       "required"=true
     *     }
     *   },
     *  parameters = {
     *         { "name" = "projectId", "required" = "required", "dataType" = "integer", "format" = "" },
     *     }
     * )
     *
     * @Annotations\View()
     * @param Request $request the request object
     * @param int     $id      the user id
     *
     * @return FormTypeInterface|View
     *
     * @throws RestApiException when user not exist
     */
    public function putUserAction(Request $request, $id)
    {
        try {
            $parameters = $request->request->all();
            $parameters['ipAddress'] = $request->getClientIp();

            if (!($user = $this->get('user.handler')->get($id))) {
                $result = $this->get('user.handler')->post(
                    array_filter($parameters), true
                );
            } else {
                $result = $this->get('user.handler')->put(
                    $user,
                    $request->request->all(), true
                );
            }
            return $this->handleResult($result);

        } catch (InvalidFormException $exception) {

            throw $exception;
        }
    }

    /**
     * Fetch a User or throw an 404 Exception.
     *
     * @param mixed $id
     *
     * @return User
     *
     * @throws RestApiException
     */
    protected function getOr404($id, $isFiltered = false)
    {
        if (!($user = $this->get('user.handler')->get($id, $isFiltered))) {
            throw new RestApiException(sprintf('The resource \'%s\' was not found.',$id), 404);
        }

        return $user;
    }

}