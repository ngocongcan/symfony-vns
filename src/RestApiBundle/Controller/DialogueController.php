<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/28/16
 * Time: 12:52 PM
 */

namespace RestApiBundle\Controller;

use RestApiBundle\Exception\RestApiException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcherInterface;

use Symfony\Component\Form\FormTypeInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as Rest;
use RestApiBundle\Exception\InvalidFormException;
use AppBundle\Entity\Dialogue;

class DialogueController extends BaseController
{
    /**
     * Get list Dialogue
     *
     * /**
     *
     * ### Sample output ###
     *     [
     *       {
     *         "id": 123,
     *         "name": "Au café – dialogue FLE au café",
     *         "active": 1,
     *         "approved": 2,
     *         "content": "Employé : Monsieur, .\r\nClient : Bonjour, un café s’il vous plaît.\r\n\r\nEmployé : Un café.\r\n\r\nEmployé: Voilà, monsieur… Trois euros. \r\n\r\nClient : Tenez.\r\n\r\nEmployé : Merci.",
     *         "iconUrl": null,
     *         "dateCreated": 1469523951,
     *         "dateUpdated": 1469526817,
     *         "categoryId": 19
     *       }
     *     ]
     *
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Returns the list of dialogues according to the passed parameters.",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     403 = "Forbidden",
     *     500 = "Internal server error"
     *   },
     *  headers={
     *     {
     *       "name"="api-key",
     *       "description"="Authorization key",
     *       "required"=true
     *     }
     *   },
     * )
     *
     * @Annotations\QueryParam(name="approvedStatus", requirements="\d+", default="2", description="The status of Dialogue, default is approved.")
     * @Annotations\QueryParam(name="lastUpdated", requirements="\d+", nullable=true, description="The last time that dialogues were updated.")
     * @Annotations\QueryParam(name="lastCreated", requirements="\d+", nullable=true, description="The last time that dialogues were created.")
     * @Annotations\QueryParam(name="accessToken", nullable=false, description="The unique access token of User and Device")
     * @Annotations\View()
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getDialoguesAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $approvedStatus = $paramFetcher->get('approvedStatus');
        $lastUpdated = $paramFetcher->get('lastUpdated');
        $lastCreated = $paramFetcher->get('lastCreated');
        $accessToken = $paramFetcher->get('accessToken');

        $result = $this->get('dialogue.handler')->all($approvedStatus, $lastUpdated, $lastCreated, $accessToken);
        return $this->handleResult($result);
    }


    /**
     * Get single Dialogue.
     *
     *
     * ### Sample output ###
     *
     *       {
     *         "id": 123,
     *         "name": "Au café – dialogue FLE au café",
     *         "active": 1,
     *         "approved": 2,
     *         "content": "Employé : Monsieur, .\r\nClient : Bonjour, un café s’il vous plaît.\r\n\r\nEmployé : Un café.\r\n\r\nEmployé: Voilà, monsieur… Trois euros. \r\n\r\nClient : Tenez.\r\n\r\nEmployé : Merci.",
     *         "iconUrl": null,
     *         "dateCreated": 1469523951,
     *         "dateUpdated": 1469526817,
     *         "categoryId": 19
     *       }
     *
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Gets a Dialogue for a given id",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the Dialogue is not found",
     *     403 = "Forbidden",
     *     500 = "Internal server error",
     *   },
     *  headers={
     *     {
     *       "name"="api-key",
     *       "description"="Authorization key",
     *       "required"=true
     *     }
     *   },
     * )
     *
     * @Annotations\View()
     *
     * @param int     $id      the Dialogue id
     *
     * @return array
     *
     * @throws RestApiException when Dialogue not exist
     */

    public function getDialogueAction($id)
    {
        $result = $this->getOr404($id, true);
        return $this->handleResult($result);

    }

    /**
     * Create a Dialogue from the submitted data.
     * ### Sample output ###
     *
     *       {
     *         "id": 123,
     *         "name": "Au café – dialogue FLE au café",
     *         "active": 1,
     *         "approved": 2,
     *         "content": "Employé : Monsieur, .\r\nClient : Bonjour, un café s’il vous plaît.\r\n\r\nEmployé : Un café.\r\n\r\nEmployé: Voilà, monsieur… Trois euros. \r\n\r\nClient : Tenez.\r\n\r\nEmployé : Merci.",
     *         "iconUrl": null,
     *         "dateCreated": 1469523951,
     *         "dateUpdated": 1469526817,
     *         "categoryId": 19
     *       }
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Creates a new Dialogue from the submitted data.",
     *   input = "RestApiBundle\Form\DialogueType",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     403 = "Forbidden",
     *     500 = "Internal server error",
     *   },
     *  headers={
     *     {
     *       "name"="api-key",
     *       "description"="Authorization key",
     *       "required"=true
     *     }
     *   },
     * )
     * @Annotations\View()
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     * @throws InvalidFormException
     */
    public function postDialogueAction(Request $request)
    {
        try {
            $parameters = $request->request->all();
            $result = $this->get('dialogue.handler')->post(
                array_filter($parameters), true
            );

            return $this->handleResult($result);

        } catch (InvalidFormException $exception) {
            throw $exception;
        }
    }

    /**
     * Update existing Dialogue from the submitted data or create a new Dialogue at a specific location.
     * ### Sample output ###
     *
     *       {
     *         "id": 123,
     *         "name": "Au café – dialogue FLE au café",
     *         "active": 1,
     *         "approved": 2,
     *         "content": "Employé : Monsieur, .\r\nClient : Bonjour, un café s’il vous plaît.\r\n\r\nEmployé : Un café.\r\n\r\nEmployé: Voilà, monsieur… Trois euros. \r\n\r\nClient : Tenez.\r\n\r\nEmployé : Merci.",
     *         "iconUrl": null,
     *         "dateCreated": 1469523951,
     *         "dateUpdated": 1469526817,
     *         "categoryId": 19
     *       }
     *
     * @ApiDoc(
     *   resource = true,
     *   input = "RestApiBundle\Form\DialogueType",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the Dialogue is not found",
     *     403 = "Forbidden",
     *     500 = "Internal server error",
     *   },
     *  headers={
     *     {
     *       "name"="api-key",
     *       "description"="Authorization key",
     *       "required"=true
     *     }
     *   },
     * )
     *
     * @Annotations\View()
     * @param Request $request the request object
     * @param int     $id      the Dialogue id
     *
     * @return FormTypeInterface|View
     *
     * @throws RestApiException when Dialogue not exist
     */
    public function putDialogueAction(Request $request, $id)
    {
        try {
            $parameters = $request->request->all();
            if (!($dialogue = $this->get('dialogue.handler')->get($id))) {
                $result = $this->get('dialogue.handler')->post(
                    array_filter($parameters), true
                );
            } else {
                $result = $this->get('dialogue.handler')->put(
                    $dialogue,
                    $request->request->all(), true
                );
            }
            return $this->handleResult($result);

        } catch (InvalidFormException $exception) {

            throw $exception;
        }
    }

    /**
     * Fetch a Dialogue or throw an 404 Exception.
     *
     * @param mixed $id
     *
     * @return Dialogue
     *
     * @throws RestApiException
     */
    protected function getOr404($id, $isFiltered = false)
    {
        if (!($dialogue = $this->get('dialogue.handler')->get($id, $isFiltered))) {
            throw new RestApiException(sprintf('The resource \'%s\' was not found.',$id), Response::HTTP_NOT_FOUND);
        }

        return $dialogue;
    }

}