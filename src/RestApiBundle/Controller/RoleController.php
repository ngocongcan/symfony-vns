<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/13/16
 * Time: 9:40 PM
 */

namespace RestApiBundle\Controller;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;


class RoleController extends BaseController
{

    /**
     * REST action which returns role by id.
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Gets a Type for a given id",
     *   output = "AppBundle\Entity\Roles",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the page is not found"
     *   }
     * )
     *
     * @param $id
     * @return mixed
     */
    public function getRoleAction($id) {
        /** @var TypeRepository $typeRepository */
        $typeRepository = $this->getDoctrine()->getRepository('AppBundle:Roles');
        $type = NULL;
        try {
            $type = $typeRepository->find($id);
        } catch (\Exception $exception) {
            $type = NULL;
        }

        if (!$type) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.', $id));
        }
        return $type;
    }


}