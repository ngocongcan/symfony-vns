<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/28/16
 * Time: 10:24 AM
 */

namespace RestApiBundle\Controller;

use RestApiBundle\Exception\RestApiException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcherInterface;

use Symfony\Component\Form\FormTypeInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as Rest;
use RestApiBundle\Exception\InvalidFormException;


class RegisterController extends BaseController
{
    /**
     * Create a UserDevice from the submitted data.
     * ### Sample output ###
     *
     *      {
     *       "id": 2,
     *       "buildVersion": null,
     *       "releaseVersion": null,
     *       "lastActive": 1466257067,
     *       "deviceIdentifiter": null,
     *       "userId": 3,
     *       "deviceId": 2
     *      }
     *
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Register a new User, Device and UserDevice from the submitted data.",
     *   input = "RestApiBundle\Form\UserDeviceType",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     403 = "Forbidden",
     *     500 = "Internal server error",
     *   },
     *  headers={
     *     {
     *       "name"="api-key",
     *       "description"="Authorization key",
     *       "required"=true
     *     }
     *   },
     *  parameters = {
     *         { "name" = "projectName", "required" = "required", "dataType" = "string", "format" = "" },
     *         { "name" = "deviceId", "required" = "required", "dataType" = "string", "format" = "" },
     *         { "name" = "type", "required" = "", "dataType" = "string", "format" = "" },
     *         { "name" = "osVersion", "required" = "", "dataType" = "string", "format" = "" },
     *         { "name" = "localize", "required" = "", "dataType" = "string", "format" = "" },
     *         { "name" = "username", "required" = "", "dataType" = "string", "format" = "" },
     *         { "name" = "plainPassword", "required" = "", "dataType" = "string", "format" = "" },
     *         { "name" = "email", "required" = "", "dataType" = "string", "format" = "" },
     *         { "name" = "firstName", "required" = "", "dataType" = "string", "format" = "" },
     *         { "name" = "lastName", "required" = "", "dataType" = "string", "format" = "" },
     *     }
     * )
     *
     * @Annotations\View(
     * )
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     * @throws InvalidFormException
     */
    public function postRegisterUserdeviceAction(Request $request)
    {
        try {
            $parameters = $request->request->all();
            $parameters['ipAddress'] = $request->getClientIp();
            $result = $this->get('user.device.handler')->post(
                array_filter($parameters), true
            );

            return $this->handleResult($result);

        } catch (InvalidFormException $exception) {
            throw $exception;
        }
    }

}