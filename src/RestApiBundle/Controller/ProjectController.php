<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/11/16
 * Time: 10:06 AM
 */

namespace RestApiBundle\Controller;
use RestApiBundle\Exception\RestApiException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcherInterface;

use Symfony\Component\Form\FormTypeInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as Rest;
use RestApiBundle\Exception\InvalidFormException;
use AppBundle\Entity\Project;

class ProjectController extends BaseController
{

    /**
     * Get list Project
     *
     * /**
     *
     * ### Sample output ###
     *     [
     *       {
     *         "id": 123,
     *         "name": "Summer 2016",
     *         "active": true,
     *         "description": "SUMMER_2016"
     *       }
     *     ]
     *
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Returns the list of projects according to the passed parameters.",
     *   output = "AppBundle\Entity\Project",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     403 = "Forbidden",
     *     500 = "Internal server error"
     *   },
     *  headers={
     *     {
     *       "name"="api-key",
     *       "description"="Authorization key",
     *       "required"=true
     *     }
     *   },
     * )
     *
     * @Annotations\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing pages.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default="20", description="How many projects to return.")
     *
     * @Annotations\View(
     *  templateVar="projects"
     * )
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getProjectsAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $offset = $paramFetcher->get('offset');
        $offset = null == $offset ? 0 : $offset;
        $limit = $paramFetcher->get('limit');

        $result = $this->get('project.handler')->all($limit, $offset);
        return $this->handleResult($result);

    }


    /**
     * Get single Project.
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Gets a Project for a given id",
     *   output = "AppBundle\Entity\Project",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the project is not found",
     *     403 = "Forbidden",
     *     500 = "Internal server error",
     *   },
     *  headers={
     *     {
     *       "name"="api-key",
     *       "description"="Authorization key",
     *       "required"=true
     *     }
     *   },
     * )
     *
     * @Annotations\View(templateVar="project")
     *
     * @param int     $id      the project id
     *
     * @return array
     *
     * @throws RestApiException when project not exist
     */

    public function getProjectAction($id)
    {
        $result = $this->getOr404($id, true);
        return $this->handleResult($result);

    }

    /**
     * Create a Project from the submitted data.
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Creates a new project from the submitted data.",
     *   input = "RestApiBundle\Form\ProjectType",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     403 = "Forbidden",
     *     500 = "Internal server error",
     *   },
     *  headers={
     *     {
     *       "name"="api-key",
     *       "description"="Authorization key",
     *       "required"=true
     *     }
     *   },
     * )
     *
     * @param Request $request the request object
     * @Annotations\View()
     * @return FormTypeInterface|View
     * @throws InvalidFormException
     */
    public function postProjectAction(Request $request)
    {
        try {
            $result = $this->get('project.handler')->post(
                $request->request->all(), true
            );

            return $this->handleResult($result);

        } catch (InvalidFormException $exception) {
            throw $exception;
        }
    }

    /**
     * Update existing project from the submitted data or create a new project at a specific location.
     *
     * @ApiDoc(
     *   resource = true,
     *   input = "RestApiBundle\Form\ProjectType",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the project is not found",
     *     403 = "Forbidden",
     *     500 = "Internal server error",
     *   },
     *  headers={
     *     {
     *       "name"="api-key",
     *       "description"="Authorization key",
     *       "required"=true
     *     }
     *   },
     * )
     *
     * @Annotations\View(
     *  template = "RestApiBundle:Project:editProject.html.twig",
     *  templateVar = "form"
     * )
     *
     * @param Request $request the request object
     * @param int     $id      the project id
     *
     * @return FormTypeInterface|View
     *
     * @throws RestApiException when project not exist
     */
    public function putProjectAction(Request $request, $id)
    {
        try {
            if (!($project = $this->get('project.handler')->get($id))) {
                $result = $this->get('project.handler')->post(
                    $request->request->all(),true
                );
            } else {
                $result = $this->get('project.handler')->put(
                    $project,
                    $request->request->all(), true
                );
            }
            return $this->handleResult($result);

        } catch (InvalidFormException $exception) {

            throw $exception;
        }
    }

    /**
     * Fetch a Project or throw an 404 Exception.
     *
     * @param mixed $id
     *
     * @return Project
     *
     * @throws RestApiException
     */
    protected function getOr404($id, $isFiltered = false)
    {
        if (!($project = $this->get('project.handler')->get($id, $isFiltered))) {
            throw new RestApiException(sprintf('The resource \'%s\' was not found.',$id), 404);
        }

        return $project;
    }

}