<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/15/16
 * Time: 11:34 AM
 */

namespace RestApiBundle\Controller;

use RestApiBundle\Exception\RestApiException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcherInterface;

use Symfony\Component\Form\FormTypeInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as Rest;
use RestApiBundle\Exception\InvalidFormException;
use AppBundle\Entity\UserDevice;

class UserDeviceController extends BaseController
{
    /**
     * Get list UserDevice
     *
     * /**
     *
     * ### Sample output ###
     *     [
     *      {
     *       "id": 2,
     *       "buildVersion": null,
     *       "releaseVersion": null,
     *       "lastActive": 1466257067,
     *       "deviceIdentifiter": null,
     *       "userId": 3,
     *       "deviceId": 2
     *      }
     *     ]
     *
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Returns the list of Userdevices according to the passed parameters.",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     403 = "Forbidden",
     *     500 = "Internal server error"
     *   },
     *  headers={
     *     {
     *       "name"="api-key",
     *       "description"="Authorization key",
     *       "required"=true
     *     }
     *   },
     * )
     *
     * @Annotations\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing pages.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default="20", description="How many Userdevices to return.")
     *
     * @Annotations\View(
     *  templateVar="userdevices"
     * )
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getUserdevicesAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $offset = $paramFetcher->get('offset');
        $offset = null == $offset ? 0 : $offset;
        $limit = $paramFetcher->get('limit');

        $result = $this->get('user.device.handler')->all($limit, $offset);
        return $this->handleResult($result);

    }


    /**
     * Get single UserDevice.
     *
     * ### Sample output ###
     *
     *      {
     *       "id": 2,
     *       "buildVersion": null,
     *       "releaseVersion": null,
     *       "lastActive": 1466257067,
     *       "deviceIdentifiter": null,
     *       "userId": 3,
     *       "deviceId": 2
     *      }
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Gets a Userdevice for a given id",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the Userdevice is not found",
     *     403 = "Forbidden",
     *     500 = "Internal server error",
     *   },
     *  headers={
     *     {
     *       "name"="api-key",
     *       "description"="Authorization key",
     *       "required"=true
     *     }
     *   },
     * )
     *
     * @Annotations\View(templateVar="userdevice")
     *
     * @param int     $id      the UserDevice id
     *
     * @return array
     *
     * @throws RestApiException when Userdevice not exist
     */

    public function getUserdeviceAction($id)
    {
        $result = $this->getOr404($id, true);

        return $this->handleResult($result);
    }

    /**
     * Create a UserDevice from the submitted data.
     * ### Sample output ###
     *
     *      {
     *       "id": 2,
     *       "buildVersion": null,
     *       "releaseVersion": null,
     *       "lastActive": 1466257067,
     *       "deviceIdentifiter": null,
     *       "userId": 3,
     *       "deviceId": 2
     *      }
     *
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Creates a new Userdevice from the submitted data.",
     *   input = "RestApiBundle\Form\UserDeviceType",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     403 = "Forbidden",
     *     500 = "Internal server error",
     *   },
     *  headers={
     *     {
     *       "name"="api-key",
     *       "description"="Authorization key",
     *       "required"=true
     *     }
     *   },
     *  parameters = {
     *         { "name" = "projectId", "required" = "required", "dataType" = "integer", "format" = "" },
     *         { "name" = "deviceId", "required" = "required", "dataType" = "integer", "format" = "" },
     *         { "name" = "type", "required" = "", "dataType" = "integer", "format" = "" },
     *         { "name" = "osVersion", "required" = "", "dataType" = "integer", "format" = "" },
     *         { "name" = "localize", "required" = "", "dataType" = "integer", "format" = "" },
     *         { "name" = "username", "required" = "", "dataType" = "string", "format" = "" },
     *         { "name" = "plainPassword", "required" = "", "dataType" = "string", "format" = "" },
     *         { "name" = "email", "required" = "", "dataType" = "string", "format" = "" },
     *         { "name" = "firstName", "required" = "", "dataType" = "string", "format" = "" },
     *         { "name" = "lastName", "required" = "", "dataType" = "string", "format" = "" },
     *     }
     * )
     *
     * @Annotations\View(
     *  template = "RestApiBundle:UserDevice:newUserDevice.html.twig",
     *  templateVar = "form"
     * )
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     * @throws InvalidFormException
     */
    public function postUserdeviceAction(Request $request)
    {
        try {
            $parameters = $request->request->all();
            $parameters['ipAddress'] = $request->getClientIp();
            $result = $this->get('user.device.handler')->post(
                array_filter($parameters), true
            );

            return $this->handleResult($result);

        } catch (InvalidFormException $exception) {
            throw $exception;
        }
    }

    /**
     * Update existing UserDevice from the submitted data or create a new UserDevice at a specific location.
     * ### Sample output ###
     *
     *      {
     *       "id": 2,
     *       "buildVersion": null,
     *       "releaseVersion": null,
     *       "lastActive": 1466257067,
     *       "deviceIdentifiter": null,
     *       "userId": 3,
     *       "deviceId": 2
     *      }
     *
     *
     * @ApiDoc(
     *   resource = true,
     *   input = "RestApiBundle\Form\UserDeviceType",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the UserDevice is not found",
     *     403 = "Forbidden",
     *     500 = "Internal server error",
     *   },
     *  headers={
     *     {
     *       "name"="api-key",
     *       "description"="Authorization key",
     *       "required"=true
     *     }
     *   },
     *
     * )
     *
     * @Annotations\View(
     *  template = "RestApiBundle:Project:editUserDevice.html.twig",
     *  templateVar = "form"
     * )
     *
     * @param Request $request the request object
     * @param int     $id      the UserDevice id
     *
     * @return FormTypeInterface|View
     *
     * @throws RestApiException when UserDevice not exist
     */
    public function putUserdeviceAction(Request $request, $id)
    {
        try {
            if (!($userDevice = $this->get('user.device.handler')->get($id))) {
                $result = $this->get('user.device.handler')->post(
                    $request->request->all(), true
                );
            } else {
                $result = $this->get('user.device.handler')->put(
                    $userDevice,
                    array_filter($request->request->all()), true
                );
            }
            return $this->handleResult($result);

        } catch (InvalidFormException $exception) {

            throw $exception;
        }
    }

    /**
     * Fetch a UserDevice or throw an 404 Exception.
     *
     * @param mixed $id
     *
     * @return UserDevice
     *
     * @throws RestApiException
     */
    protected function getOr404($id, $isFiltered = false)
    {
        if (!($project = $this->get('user.device.handler')->get($id, $isFiltered))) {
            throw new RestApiException(sprintf('The resource \'%s\' was not found.',$id), 404);
        }

        return $project;
    }

}