<?php

namespace RestApiBundle\Security;

use Exception;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use AppBundle\Entity\User;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Http\Authentication\SimplePreAuthenticatorInterface;
use RestApiBundle\Controller\BaseController;

/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 5/24/16
 * Time: 2:54 PM
 */
class ApiKeyAuthenticator implements SimplePreAuthenticatorInterface, AuthenticationFailureHandlerInterface {

    /** @var BaseController   */
    private $restBaseController;

    /**
     * ApiKeyAuthenticator constructor.
     * @param BaseController $restBaseController
     */
    public function __construct(BaseController $restBaseController)
    {
        $this->restBaseController = $restBaseController;
    }


    /**
	 * @param Request $request
	 * @param string  $providerKey
	 * @return PreAuthenticatedToken
	 */
	public function createToken(Request $request, $providerKey) {
		// look for an api key  parameter
		$apiKey = $request->get('api-key');

		if (!$apiKey) {
			$apiKey = $request->headers->get('api-key');
		}

		if (!$apiKey) {
			throw new AuthenticationException('No API Key found', 403);
		}

		return new PreAuthenticatedToken(
			'anon.',
			$apiKey,
			$providerKey
		);
	}

	/**
	 * @param TokenInterface $token
	 * @param string         $providerKey
	 * @return bool
	 */
	public function supportsToken(TokenInterface $token, $providerKey) {
		return $token instanceof PreAuthenticatedToken && $token->getProviderKey() === $providerKey;
	}

	/**
	 * @param TokenInterface        $token
	 * @param UserProviderInterface $userProvider
	 * @param string                $providerKey
	 * @return PreAuthenticatedToken
	 */
	public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey) {
		if (!$userProvider instanceof ApiKeyUserProvider) {
			throw new InvalidArgumentException(sprintf(
				'The user provider must be an instance of ApiKeyUserProvider (%s was given).',
				get_class($userProvider)
			), 403);
		}

		$apiKey = $token->getCredentials();

		/* @var $user User */

		$user = $userProvider->getUserByApiKey($apiKey);

		if (!$user) {
			throw new AuthenticationException('API Key does not exist', 403);
		}

		return new PreAuthenticatedToken(
			$user,
			$apiKey,
			$providerKey,
			$user->getRoles()
		);
	}

	/**
	 * @param Request                 $request
	 * @param AuthenticationException $exception
	 * @return JsonResponse
	 * @throws Exception
	 */
	public function onAuthenticationFailure(Request $request, AuthenticationException $exception) {
		$response = new JsonResponse();
		$response->setCharset('utf-8');
		$response->headers->set('Content-Type', 'application/json; charset=utf-8');
        $result = $this->restBaseController->handleResult(null, $exception->getMessage());
		$response->setData($result);
		$response->setStatusCode($exception->getCode());

		return $response;
	}
}
