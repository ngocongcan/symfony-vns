<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 5/24/16
 * Time: 3:00 PM
 */

namespace RestApiBundle\Security;
use AppBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

/**
 * Class ApiKeyUserProvider
 *
 * @package RestApiBundle\Security
 */
class ApiKeyUserProvider implements UserProviderInterface {

	/** @var ContainerInterface */
	protected $container;

	/**
	 * ApiKeyUserProvider constructor.
	 *
	 * @param ContainerInterface $serviceContainer
	 */
	public function __construct(ContainerInterface $serviceContainer) {
		$this->container = $serviceContainer;
	}

	/**
	 * @param string $apiKey
	 *
	 * @return string
	 *
	 */
	public function getUserByApiKey($apiKey) {
		return $this->container->get('user_repository')->findOneByApiKey($apiKey);
	}

	/**
	 * @param string $username
	 *
	 * @return User
	 *
	 */
	public function loadUserByUsername($username) {
		return new User();
	}

	/**
	 * @param UserInterface $user
	 *
	 * @return UserInterface|void
	 */
	public function refreshUser(UserInterface $user) {
		// this is used for storing authentication in the session
		// but in this example, the token is sent in each request,
		// so authentication can be stateless. Throwing this exception
		// is proper to make things stateless
		throw new UnsupportedUserException();
	}

	/**
	 * @param string $class
	 *
	 * @return bool
	 *
	 */
	public function supportsClass($class) {
		return 'AppBundle\Entity\User' === $class;
	}
}
