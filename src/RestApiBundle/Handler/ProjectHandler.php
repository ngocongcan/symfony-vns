<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/11/16
 * Time: 11:57 AM
 */

namespace RestApiBundle\Handler;


use AppBundle\Entity\Project;
use RestApiBundle\Form\ProjectType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;
use RestApiBundle\Exception\InvalidFormException;
use AppBundle\Repository\ProjectRepository;
use RestApiBundle\Exception\RestApiException;


class ProjectHandler implements ProjectHandlerInterface
{
    private $doctrine;
    private $entityClass;
    private $formFactory;
    /** @var  ProjectRepository */
    private $projectRepository;

    public function __construct(RegistryInterface $doctrine, $entityClass, FormFactoryInterface $formFactory)
    {
        $this->doctrine = $doctrine;
        $this->entityClass = $entityClass;
        $this->formFactory = $formFactory;
        $this->projectRepository = $this->doctrine->getRepository('AppBundle:Project');
    }

       /**
     * Get a Project given the name
     *
     * @api
     *
     * @param mixed $name
     *
     * @return Project
     */
    public function getByName($name)
    {
        return $this->projectRepository->findOneBy(array('name' => $name));
    }


    /**
     * Get a Project given the identifier
     *
     * @api
     *
     * @param mixed $id
     *
     * @return Project
     */
    public function get($id, $isFiltered = false)
    {
        if(!$isFiltered) {
            return $this->projectRepository->find($id);
        }
        return $this->projectRepository->findById($id);
    }


    /**
     * Get a list of Project.
     *
     * @param int $limit the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all($limit = 20, $offset = 0)
    {
        return $this->projectRepository->findAllOrderedByName($limit, $offset);
    }

    /**
     * Post Projects, creates a new Projects.
     *
     * @api
     *
     * @param array $parameters
     *
     * @return Project
     */
    public function post(array $parameters, $isFiltered = false)
    {
        /** @var Project $project */
        $project = $this->createProject();
        $project->setDateCreated(time());
        $project->setActive(true);
        $savedProject = $this->processForm($project, $parameters, 'POST');
        return $this->get($savedProject->getId(), $isFiltered);
    }

    /**
     * Edit a Project.
     *
     * @api
     *
     * @param Project $projects
     * @param array $parameters
     *
     * @return Project
     */
    public function put(Project $project, array $parameters, $isFiltered = false)
    {
        $project = $this->processForm($project, $parameters, 'PUT');
        return $this->get($project->getId(), $isFiltered);
    }

    /**
     * Partially update a Project.
     *
     * @api
     *
     * @param Project $projects
     * @param Project $parameters
     *
     * @return Project
     */
    public function patch(Project $project, array $parameters)
    {
        $project =  $this->processForm($project, $parameters, 'PATCH');
        return $this->get($project->getId());

    }

    /**
     * Processes the form.
     *
     * @param Project $project
     * @param array         $parameters
     * @param String        $method
     *
     * @return Project
     *
     * @throws \RestApiBundle\Exception\InvalidFormException
     */
    private function processForm(Project $project, array $parameters, $method = "PUT")
    {
        $form = $this->formFactory->create(new ProjectType(), $project, array('method' => $method));
        $form->submit($parameters, 'PATCH' === $method);
        if ($form->isValid()) {

            $project = $form->getData();
            $em = $this->doctrine->getManager();
            $em->persist($project);
            $em->flush($project);

            return $project;
        }

        throw new InvalidFormException('Invalid submitted data', $form);
    }

    private function createProject()
    {
        return new $this->entityClass();
    }

}