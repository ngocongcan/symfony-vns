<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/15/16
 * Time: 11:35 AM
 */

namespace RestApiBundle\Handler;
use AppBundle\Entity\User;
use AppBundle\Entity\Device;
use AppBundle\Entity\UserDevice;

interface UserDeviceHandlerInterface
{
    /**
     * Get a UserDevice given the identifier
     *
     * @api
     *
     * @param mixed $id
     *
     * @return UserDevice
     */
    public function get($id,  $isObject = false);

    /**
     * Get a list of UserDevice.
     *
     * @param int $limit  the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all($limit = 5, $offset = 0);

    /**
     * Post UserDevice, creates a new UserDevice.
     *
     * @api
     *
     * @param array $parameters
     *
     * @return UserDevice
     */
    public function post(array $parameters);

    /**
     * Edit a UserDevice.
     *
     * @api
     *
     * @param UserDevice   $userDevice
     * @param array           $parameters
     *
     * @return UserDevice
     */
    public function put(UserDevice $userDevice, array $parameters);

    /**
     * Partially update a UserDevice.
     *
     * @api
     *
     * @param UserDevice   $userDevice
     * @param UserDevice           $parameters
     *
     * @return UserDevice
     */
    public function patch(UserDevice $userDevice, array $parameters);

}