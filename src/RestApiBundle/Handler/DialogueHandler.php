<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/28/16
 * Time: 12:54 PM
 */

namespace RestApiBundle\Handler;

use AppBundle\Entity\Dialogue;
use RestApiBundle\Form\DialogueType;
use RestApiBundle\Exception\InvalidFormException;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Exception;
use RestApiBundle\Exception\RestApiException;
use AppBundle\Repository\DialogueRepository;

class DialogueHandler
{

    private $entityClass;
    private $entityHandler;
    /** @var DialogueRepository  */
    private $dialogueRepository;

    public function __construct(EntityHandler $entityHandler, $entityClass)
    {
        $this->entityHandler = $entityHandler;
        $this->entityClass = $entityClass;
        $this->dialogueRepository = $this->entityHandler->getDoctrine()->getRepository($entityClass);
    }

    /**
     * Get a Dialogue given the identifier
     *
     * @api
     *
     * @param mixed $id
     *
     * @return Dialogue
     */
    public function get($id, $isFiltered = false)
    {
        if(!$isFiltered) {
            return $this->dialogueRepository->find($id);
        }
        return $this->dialogueRepository->get($id);
    }

    /**
     * Get a list of Dialogue.
     *
     * @param int $lastUpdated
     * @param int $lastUpdated
     *
     * @return array
     */
    public function all($approvedStatus = 2,$lastUpdated, $lastCreated, $accessToken)
    {
        if(is_null($accessToken)){
            throw new RestApiException('accessToken is missing', 404);
        }
        $project = $this->entityHandler->getProjectByAccessToken($accessToken);
        if(is_null($project)){
            throw new RestApiException('Project linked to this accessToken was not found', 404);
        }
        return $this->dialogueRepository->getAllByProject($project->getId(), null, $approvedStatus, $lastUpdated, $lastCreated);
    }

    /**
     * Post Dialogue, creates a new $user.
     *
     * @api
     *
     * @param array $parameters
     *
     * @return Dialogue
     */
    public function post(array $parameters, $isFiltered = false)
    {
        try{

            if(array_key_exists('name', $parameters)){
                $aDialogue = $this->findDialogueByName($parameters['name']);
                if($aDialogue){
                    if($isFiltered){
                        return $this->get($aDialogue->getId(), true);
                    }
                    return $aDialogue;
                }
            }
            /** @var Dialogue $aDialogue */
            $aDialogue = $this->entityHandler->createEntity($this->entityClass);
            $aDialogue->setDateCreated(time());

            $savedDialogue = $this->processForm($aDialogue, $parameters, 'POST');
            if($isFiltered){
                return $this->get($savedDialogue->getId(), true);
            }
            return $savedDialogue;

        } catch (Exception $e){
            throw $e;
        }

    }

    /**
     * Edit a Dialogue.
     *
     * @api
     *
     * @param Dialogue $Dialogue
     * @param array $parameters
     *
     * @return Dialogue
     */
    public function put(Dialogue $aDialogue, array $parameters, $isFiltered = false)
    {
        $aDialogue->setDateUpdated(time());
        $updatedDialogue = $this->processForm($aDialogue, $parameters, 'PUT');
        return $this->get($updatedDialogue->getId(), $isFiltered);
    }



    /**
     * Processes the form.
     *
     * @param Dialogue $Dialogue
     * @param array         $parameters
     * @param String        $method
     *
     * @return Dialogue
     *
     * @throws \RestApiBundle\Exception\InvalidFormException
     */
    private function processForm(Dialogue $Dialogue, array $parameters, $method = "PUT")
    {
        $form = $this->entityHandler->getFormFactory()->create(new DialogueType(), $Dialogue, array('method' => $method));
        $form->submit($parameters, 'PATCH' === $method);
        if ($form->isValid()) {

            $savedDialogue = $form->getData();
            $em = $this->entityHandler->getDoctrine()->getManager();
            $em->persist($savedDialogue);
            $em->flush($savedDialogue);

            return $savedDialogue;
        }

        throw new InvalidFormException('Invalid submitted data', $form);
    }

    /**
     * @param $name
     * @return null|Dialogue
     */

    private function findDialogueByName($name){

        return $this->dialogueRepository->findOneBy(array(
            'name' => $name,
        ));
    }

}