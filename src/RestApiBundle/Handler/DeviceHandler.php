<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/15/16
 * Time: 11:56 AM
 */

namespace RestApiBundle\Handler;


use AppBundle\Entity\Device;
use RestApiBundle\Exception\InvalidFormException;
use RestApiBundle\Exception\RestApiException;
use RestApiBundle\Form\DeviceType;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Form\FormFactoryInterface;

class DeviceHandler implements DeviceHandlerInterface
{

    private $doctrine;
    private $entityClass;
    private $formFactory;

    public function __construct(RegistryInterface $doctrine, $entityClass, FormFactoryInterface $formFactory)
    {
        $this->doctrine = $doctrine;
        $this->entityClass = $entityClass;
        $this->formFactory = $formFactory;
    }

    /**
     * Get a Device given the identifier
     *
     * @api
     *
     * @param mixed $id
     *
     * @return Device
     */
    public function get($id)
    {
        // TODO: Implement get() method.
    }

    /**
     * Get a list of Device.
     *
     * @param int $limit the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all($limit = 5, $offset = 0)
    {
        // TODO: Implement all() method.
    }

    /**
     * Post Device, creates a new $device.
     *
     * @api
     *
     * @param array $parameters
     *
     * @return Device
     */
    public function post(array $parameters)
    {
        /** @var Device $device */
        $device = null;
        if(array_key_exists('deviceId', $parameters)){
            $deviceId = $parameters['deviceId'];
            $device = $this->findDeviceByDeviceID($deviceId);
        }

        if($device === null){
            $device = $this->createDevice();
            $device->setDateCreated(time());
        }
        $device->setActive(true);
        return $this->processForm($device, $parameters, 'POST');
    }

    /**
     * Edit a Device.
     *
     * @api
     *
     * @param Device $device
     * @param array $parameters
     *
     * @return Device
     */
    public function put(Device $user, array $parameters)
    {
        // TODO: Implement put() method.
    }

    /**
     * Partially update a Device.
     *
     * @api
     *
     * @param Device $device
     * @param Device $parameters
     *
     * @return Device
     */
    public function patch(Device $device, array $parameters)
    {
        // TODO: Implement patch() method.
    }


    /**
     * Processes the form.
     *
     * @param Device $device
     * @param array         $parameters
     * @param String        $method
     *
     * @return Device
     *
     * @throws \RestApiBundle\Exception\InvalidFormException
     */
    private function processForm(Device $device, array $parameters, $method = "PUT")
    {
        $form = $this->formFactory->create(new DeviceType(), $device, array('method' => $method));
        $form->submit($parameters, 'PATCH' === $method);
        if ($form->isValid()) {

            $device = $form->getData();
            $em = $this->doctrine->getManager();
            $em->persist($device);
            $em->flush($device);

            return $device;
        }

        throw new InvalidFormException('Invalid submitted data', $form);
    }



    private function createDevice()
    {
        return new $this->entityClass();
    }

    /**
     * @param $deviceId
     * @return Device|null
     */

    private function findDeviceByDeviceID($deviceId){
        $entityRep = $this->doctrine->getRepository('AppBundle:Device');
        $entity = $entityRep->findOneBy(array(
            'deviceId' => $deviceId
        ));

        if($entity) return $entity;
        return null;

    }
}