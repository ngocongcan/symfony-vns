<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/11/16
 * Time: 2:07 PM
 */

namespace RestApiBundle\Handler;

use AppBundle\Entity\Project;

interface ProjectHandlerInterface
{
    /**
     * Get a Project given the identifier
     *
     * @api
     *
     * @param mixed $id
     *
     * @return Project
     */
    public function get($id,  $isObject = false);

    /**
     * Get a list of Project.
     *
     * @param int $limit  the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all($limit = 5, $offset = 0);

    /**
     * Post Project, creates a new Projects.
     *
     * @api
     *
     * @param array $parameters
     *
     * @return Project
     */
    public function post(array $parameters);

    /**
     * Edit a Project.
     *
     * @api
     *
     * @param Project   $projects
     * @param array           $parameters
     *
     * @return Project
     */
    public function put(Project $projects, array $parameters);

    /**
     * Partially update a Project.
     *
     * @api
     *
     * @param Project   $projects
     * @param Project           $parameters
     *
     * @return Project
     */
    public function patch(Project $projects, array $parameters);

}