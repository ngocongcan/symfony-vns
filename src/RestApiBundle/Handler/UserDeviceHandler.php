<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/15/16
 * Time: 11:39 AM
 */

namespace RestApiBundle\Handler;


use AppBundle\Entity\Device;
use AppBundle\Entity\Project;
use AppBundle\Entity\User;
use AppBundle\Entity\UserDevice;
use AppBundle\Services\Common;
use RestApiBundle\Exception\RestApiException;
use RestApiBundle\Form\UserDeviceType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;
use RestApiBundle\Exception\InvalidFormException;
use AppBundle\Repository\UserDeviceRepository;

class UserDeviceHandler implements UserDeviceHandlerInterface
{
    private $entityClass;
    private $common;
    private $entityHandler;
    private $userHandler;
    private $projectHandler;
    private $deviceHandler;
    /** @var  UserDeviceRepository */
    private $userDeviceRepository;

    public function __construct(EntityHandler $entityHandler, $entityClass, UserHandler $userHandler, DeviceHandler $deviceHandler, ProjectHandler $projectHandler, Common $common)
    {
        $this->entityHandler = $entityHandler;
        $this->entityClass = $entityClass;
        $this->common = $common;
        $this->userHandler = $userHandler;
        $this->deviceHandler = $deviceHandler;
        $this->projectHandler = $projectHandler;
        $this->userDeviceRepository = $this->entityHandler->getDoctrine()->getRepository($this->entityClass);
    }

    /**
     * Get a UserDevice given the identifier
     *
     * @api
     *
     * @param mixed $id
     *
     * @return UserDevice
     */
    public function get($id, $isFiltered = false)
    {

        if(!$isFiltered) {
            return $this->userDeviceRepository->find($id);
        }
        return $this->userDeviceRepository->get($id);
    }

    /**
     * Get a list of UserDevice.
     *
     * @param int $limit the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all($limit = 20, $offset = 0)
    {

        return $this->userDeviceRepository->findAllOrderById($limit, $offset);
    }

    /**
     * Post UserDevice, creates a new UserDevice.
     *
     * @api
     *
     * @param array $parameters
     *
     * @return UserDevice
     */
    public function post(array $parameters , $isFiltered = false)
    {

        if(array_key_exists('projectName', $parameters) == false){
            throw new RestApiException("projectName is missing", 404);
        }

        $projectParams = array();
        $projectParams["projectName"] = $parameters['projectName'];
        $projectName = $parameters['projectName'];

        $project = $this->projectHandler->getByName($projectName);
        if(is_null($project)){
            throw new RestApiException("Project with ". $projectName . " was not found", 404);
        }

        $userParams = array();

        if(array_key_exists('ipAddress', $parameters)){
            $userParams['ipAddress'] = $parameters['ipAddress'];
        }
        if(array_key_exists('username', $parameters)){
            $userParams['username'] = $parameters['username'];
        }
        if(array_key_exists('plainPassword', $parameters)){
            $userParams['plainPassword'] = $parameters['plainPassword'];
        }
        if(array_key_exists('email', $parameters)){
            $userParams['email'] = $parameters['email'];
        }
        if(array_key_exists('firstName', $parameters)){
            $userParams['firstName'] = $parameters['firstName'];
        }
        if(array_key_exists('lastName', $parameters)){
            $userParams['lastName'] = $parameters['lastName'];
        }
        $user = $this->userHandler->post($userParams);
        $deviceParams = array();

        if(array_key_exists('deviceId', $parameters)){
            $deviceParams['deviceId'] = $parameters['deviceId'];
        }
        if(array_key_exists('type', $parameters)){
            $deviceParams['type'] = $parameters['type'];
        }
        if(array_key_exists('osVersion', $parameters)){
            $deviceParams['osVersion'] = $parameters['osVersion'];
        }
        if(array_key_exists('localize', $parameters)){
            $deviceParams['localize'] = $parameters['localize'];
        }

        $device = $this->deviceHandler->post($deviceParams);

        $parameters = array_diff_key($parameters, $userParams, $deviceParams,$projectParams);

        /** @var UserDevice $userDevice */
        $userDevice = $this->findUserDeviceByUserAndDeviceAndProject($user, $device, $project);
        if(!$userDevice){
            $userDevice = $this->createUserDevice();
            $userDevice->setDateCreated(time());
            $userDevice->setUser($user);
            $userDevice->setDevice($device);
            $userDevice->setProject($project);
            $accessToken = $this->common->generateAccessToken();
            $userDevice->setAccessToken($accessToken);
        }
        $userDevice->setLastActive(time());
        $newUserDevice = $this->processForm($userDevice, $parameters, 'POST');

        return $this->get($newUserDevice->getId(), $isFiltered);
    }

    /**
     * Edit a UserDevice.
     *
     * @api
     *
     * @param UserDevice $userDevice
     * @param array $parameters
     *
     * @return UserDevice
     */
    public function put(UserDevice $userDevice, array $parameters, $isFiltered = false)
    {
        $userDevice = $this->processForm($userDevice, $parameters, 'PUT');
        return $this->get($userDevice->getId(), $isFiltered);
    }

    /**
     * Partially update a UserDevice.
     *
     * @api
     *
     * @param UserDevice $userDevice
     * @param UserDevice $parameters
     *
     * @return UserDevice
     */
    public function patch(UserDevice $userDevice, array $parameters)
    {
        $userDevice = $this->processForm($userDevice, $parameters, 'PATCH');
        return $this->get($userDevice->getId());
    }

    /**
     * Processes the form.
     *
     * @param UserDevice $userDevice
     * @param array         $parameters
     * @param String        $method
     *
     * @return UserDevice
     *
     * @throws \RestApiBundle\Exception\InvalidFormException
     */
    private function processForm(UserDevice $userDevice, array $parameters, $method = "PUT")
    {
        $form = $this->entityHandler->getFormFactory()->create(new UserDeviceType(), $userDevice, array('method' => $method));
        $form->submit($parameters, 'PATCH' === $method);
        if ($form->isValid()) {

            $userDevice = $form->getData();
            $em = $this->entityHandler->getDoctrine()->getManager();
            $em->persist($userDevice);
            $em->flush($userDevice);

            return $userDevice;
        }

        throw new InvalidFormException('Invalid submitted data', $form);
    }

    private function createUserDevice()
    {
        return  $this->entityHandler->createEntity($this->entityClass);
    }

    /**
     * @param User $user
     * @param Device $device
     * @param Project $project
     * @return null|object
     */
    private function findUserDeviceByUserAndDeviceAndProject(User $user, Device $device, Project $project){

        $entity = $this->userDeviceRepository->findOneBy(array(
            'user' => $user,
            'device' => $device,
            'project' => $project,
        ));

       return $entity;
    }
}