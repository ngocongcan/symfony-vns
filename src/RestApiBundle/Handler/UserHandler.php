<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/11/16
 * Time: 10:34 AM
 */

namespace RestApiBundle\Handler;


use AppBundle\Entity\Project;
use AppBundle\Entity\User;
use AppBundle\Services\Common;
use RestApiBundle\Exception\InvalidFormException;
use RestApiBundle\Exception\RestApiException;
use RestApiBundle\Form\UserType;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Form\FormFactoryInterface;
use AppBundle\Entity\UserProject;
use Doctrine\ORM\EntityRepository;
use Exception;
use AppBundle\Repository\UserRepository;

class UserHandler implements UserHandlerInterface
{
    private $doctrine;
    private $entityClass;
    private $formFactory;
    private $projectHandler;
    private $common;
    /** @var UserRepository  */
    private $userRepository;

    public function __construct(RegistryInterface $doctrine, $entityClass, FormFactoryInterface $formFactory, ProjectHandler $projectHandler, Common $common)
    {
        $this->doctrine = $doctrine;
        $this->entityClass = $entityClass;
        $this->formFactory = $formFactory;
        $this->projectHandler = $projectHandler;
        $this->common = $common;
        $this->userRepository = $this->doctrine->getRepository('AppBundle:User');
    }

    /**
     * Get a User given the identifier
     *
     * @api
     *
     * @param mixed $id
     *
     * @return User
     */
    public function get($id, $isFiltered = false)
    {
        if(!$isFiltered) {
            return $this->userRepository->find($id);
        }
        return $this->userRepository->get($id);
    }

    /**
     * Get a list of User.
     *
     * @param int $limit the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all($limit = 20, $offset = 0)
    {
        return $this->userRepository->findAllOrderedByUserName($limit, $offset);
    }

    /**
     * Post User, creates a new $user.
     *
     * @api
     *
     * @param array $parameters
     *
     * @return User
     */
    public function post(array $parameters, $isFiltered = false)
    {
        try{
            $user = null;
            if(array_key_exists('username', $parameters)){
                $username = $parameters['username'];
                $user = $this->findUserByUsername($username);
            }

            if($user === null) {
                $apiKey = $this->common->generateApiKey();
                /** @var User $user */
                $user = $this->createUser();
                $user->setDateCreated(time());
                $user->setActive(true);
                $user->setUsername($apiKey);
                $password = $user->generatePlainPassword();
                $user->setPlainPassword($password);
                $user->setApiKey($apiKey);
            }

            $user = $this->processForm($user, $parameters, 'POST');
//            $this->createUserProject($user, $project);

            if($isFiltered){
                return $this->get($user->getId(), true);
            }
            return $user;

        } catch (Exception $e){
            throw $e;
        }

    }

    /**
     * Edit a User.
     *
     * @api
     *
     * @param User $user
     * @param array $parameters
     *
     * @return User
     */
    public function put(User $user, array $parameters, $isFiltered = false)
    {
        $user = $this->processForm($user, $parameters, 'PUT');
        return $this->get($user->getId(), $isFiltered);
    }

    /**
     * Partially update a User.
     *
     * @api
     *
     * @param User $user
     * @param User $parameters
     *
     * @return User
     */
    public function patch(User $user, array $parameters)
    {
        // TODO: Implement patch() method.
    }


    /**
     * Processes the form.
     *
     * @param User $user
     * @param array         $parameters
     * @param String        $method
     *
     * @return User
     *
     * @throws \RestApiBundle\Exception\InvalidFormException
     */
    private function processForm(User $user, array $parameters, $method = "PUT")
    {
        $form = $this->formFactory->create(new UserType(), $user, array('method' => $method));
        $form->submit($parameters, 'PATCH' === $method);
        if ($form->isValid()) {

            $user = $form->getData();
            $em = $this->doctrine->getManager();
            $em->persist($user);
            $em->flush($user);

            return $user;
        }

        throw new InvalidFormException('Invalid submitted data', $form);
    }

    private function createUser()
    {
        return new $this->entityClass();
    }

    /**
     * @param $username
     * @return User|null
     */
    private function findUserByUsername($username){
        $userRep = $this->doctrine->getRepository('AppBundle:User');
        $user = $userRep->findOneBy(array(
            'username' => $username
        ));

        if($user) return $user;
        return null;

    }

    private function createUserProject(User $user , Project $project){

        try {
            $userProject = $this->doctrine->getRepository('AppBundle:UserProject')->findOneBy(array(
                'user' => $user,
                'project' => $project,
            ));

            if(!$userProject) {
                $userProject = new UserProject();
                $userProject->setProject($project);
                $userProject->setUser($user);
                $userProject->setDateCreated(time());
                $em = $this->doctrine->getManager();
                $em->persist($userProject);
                $em->flush();
            }
        } catch (Exception $e){

        }

    }

}