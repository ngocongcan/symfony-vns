<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 12/12/16
 * Time: 10:57 AM
 */

namespace RestApiBundle\Handler;

use Google_Client;
use Google_Service_YouTube;
use Google_Service_Exception;

/**
*   https://github.com/youtube/api-samples/tree/master/php
***/
class YoutubeHandler
{

    public function load(){
        $client = new Google_Client();
        $client->setApplicationName("EC Practice");
        $client->setDeveloperKey("AIzaSyDPS2oiB9YQbFXQxt87G5eTW4wy8c6zexs");
        $results = array();
        $youtube = new Google_Service_YouTube($client);
        try {
            // Call the search.list method to retrieve results matching the specified
            // query term.
            $searchResponse = $youtube->search->listSearch('id,snippet', array(
                'q' => "[French for beginners]",
                'maxResults' => 50,
                'type' => 'video',
                'channelId' => "UCocV3q1nM96aB3BdZe37YnQ",
                'order' => "date",
            ));
            $videos = '';
            $channels = '';
            $playlists = '';


            return $searchResponse;
            // Add each result to the appropriate list, and then display the lists of
            // matching videos, channels, and playlists.
            foreach ($searchResponse['items'] as $searchResult) {
                switch ($searchResult['id']['kind']) {
                    case 'youtube#video':
                        $videos .= sprintf('<li>%s (%s)</li>',
                            $searchResult['snippet']['title'], $searchResult['id']['videoId']);
                        break;
                    case 'youtube#channel':
                        $channels .= sprintf('<li>%s (%s)</li>',
                            $searchResult['snippet']['title'], $searchResult['id']['channelId']);
                        break;
                    case 'youtube#playlist':
                        $playlists .= sprintf('<li>%s (%s)</li>',
                            $searchResult['snippet']['title'], $searchResult['id']['playlistId']);
                        break;
                }
            }

            $results['videos'] = $videos;
            $results['channels'] = $channels;
            $results['playlists'] = $playlists;


        } catch (Google_Service_Exception $e) {
            return   htmlspecialchars($e->getMessage());
        } catch (Google_Exception $e) {
              return   htmlspecialchars($e->getMessage());
        }


        return $results;
    }


}