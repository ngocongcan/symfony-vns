<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/15/16
 * Time: 11:55 AM
 */

namespace RestApiBundle\Handler;

use AppBundle\Entity\Device;

interface DeviceHandlerInterface
{
    /**
     * Get a Device given the identifier
     *
     * @api
     *
     * @param mixed $id
     *
     * @return Device
     */
    public function get($id);

    /**
     * Get a list of Device.
     *
     * @param int $limit  the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all($limit = 5, $offset = 0);

    /**
     * Post Device, creates a new $device.
     *
     * @api
     *
     * @param array $parameters
     *
     * @return Device
     */
    public function post(array $parameters);

    /**
     * Edit a Device.
     *
     * @api
     *
     * @param Device   $device
     * @param array           $parameters
     *
     * @return Device
     */
    public function put(Device $user, array $parameters);

    /**
     * Partially update a Device.
     *
     * @api
     *
     * @param Device   $device
     * @param Device           $parameters
     *
     * @return Device
     */
    public function patch(Device $device, array $parameters);

}