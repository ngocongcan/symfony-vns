<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/11/16
 * Time: 10:34 AM
 */

namespace RestApiBundle\Handler;

use AppBundle\Entity\User;

interface UserHandlerInterface
{

    /**
     * Get a User given the identifier
     *
     * @api
     *
     * @param mixed $id
     *
     * @return User
     */
    public function get($id, $isObject = false);

    /**
     * Get a list of User.
     *
     * @param int $limit  the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all($limit = 5, $offset = 0);

    /**
     * Post User, creates a new $user.
     *
     * @api
     *
     * @param array $parameters
     *
     * @return User
     */
    public function post(array $parameters);

    /**
     * Edit a User.
     *
     * @api
     *
     * @param User   $user
     * @param array           $parameters
     *
     * @return User
     */
    public function put(User $user, array $parameters);

    /**
     * Partially update a User.
     *
     * @api
     *
     * @param User   $user
     * @param User           $parameters
     *
     * @return User
     */
    public function patch(User $user, array $parameters);
}