<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/27/16
 * Time: 12:35 PM
 */

namespace RestApiBundle\Handler;

use AppBundle\Entity\Category;
use RestApiBundle\Exception\RestApiException;
use RestApiBundle\Form\CategoryType;
use RestApiBundle\Exception\InvalidFormException;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Exception;
use AppBundle\Repository\CategoryRepository;

class CategoryHandler
{
    private $entityClass;
    private $entityHandler;
    /** @var CategoryRepository  */
    private $categoryRepository;
    /** @var  DialogueHandler */
    private $dialogueHandler;

    public function __construct(EntityHandler $entityHandler, $entityClass, $dialogueHandler)
    {
        $this->entityHandler = $entityHandler;
        $this->entityClass = $entityClass;
        $this->dialogueHandler = $dialogueHandler;
        $this->categoryRepository = $this->entityHandler->getDoctrine()->getRepository($entityClass);
    }

    /**
     * Get a Category given the identifier
     *
     * @api
     *
     * @param mixed $id
     *
     * @return Category
     */
    public function get($id, $isFiltered = false)
    {
        if(!$isFiltered) {
            return $this->categoryRepository->find($id);
        }
        return $this->categoryRepository->get($id);
    }

    /**
     * Get a list of Category.
     *
     * @param int $lastUpdated
     * @param int $lastUpdated
     *
     * @return array
     */
    public function all($approvedStatus = 2,$lastUpdated, $lastCreated, $accessToken)
    {
        if(is_null($accessToken)){
            throw new RestApiException('accessToken is missing', 404);
        }
        $project = $this->entityHandler->getProjectByAccessToken($accessToken);
        if(is_null($project)){
            throw new RestApiException('Project linked to this accessToken was not found', 404);
        }
        $categories = $this->categoryRepository->getAllByProject($project->getId(), null, $approvedStatus, $lastUpdated, $lastCreated);
        return $categories;

    }

    /**
     * Get a list of Category.
     *
     * @param int $lastUpdated
     * @param int $lastUpdated
     *
     * @return array
     */
    public function allWithDialogues($approvedStatus = 2, $accessToken)
    {
        if(is_null($accessToken)){
            throw new RestApiException('accessToken is missing', 404);
        }
        $project = $this->entityHandler->getProjectByAccessToken($accessToken);
        if(is_null($project)){
            throw new RestApiException('Project linked to this accessToken was not found', 404);
        }
        $categories = $this->categoryRepository->getAllByProject($project->getId(), null, $approvedStatus, null, null);
        $dialogues = $this->dialogueHandler->all($approvedStatus, null, null, $accessToken);

        $categoryArray = array();
        foreach($categories as $cat){
            $dialogueArray = array();
            foreach($dialogues as $dia) {
                if($dia['categoryId'] === $cat['id']){
                    $dialogueArray[] = $dia;
                }
            }
            if(count($dialogueArray)){
                $cat['dialogues'] = $dialogueArray;
                $categoryArray[] = $cat;
            }
        }

        return $categoryArray;

    }

    /**
     * Post Category, creates a new $user.
     *
     * @api
     *
     * @param array $parameters
     *
     * @return Category
     */
    public function post(array $parameters, $isFiltered = false)
    {
        try{

            if(array_key_exists('name', $parameters)){
                $category = $this->findCategoryByName($parameters['name']);
                if($category){
                    if($isFiltered){
                        return $this->get($category->getId(), true);
                    }
                    return $category;
                }
            }
            /** @var Category $category */
            $category = $this->entityHandler->createEntity($this->entityClass);
            $projects = $this->entityHandler->getLinkedProjects();
            if(count($projects) != 1) {
                throw new RestApiException('This user linked more than one Project', 403);
            }
            $category->setProject($projects[0]);
            $category->setDateCreated(time());

            $savedCategory = $this->processForm($category, $parameters, 'POST');
            if($isFiltered){
                return $this->get($savedCategory->getId(), true);
            }
            return $savedCategory;

        } catch (Exception $e){
            throw $e;
        }

    }

    /**
     * Edit a Category.
     *
     * @api
     *
     * @param Category $category
     * @param array $parameters
     *
     * @return Category
     */
    public function put(Category $category, array $parameters, $isFiltered = false)
    {
        $category->setDateUpdated(time());
        $updatedCategory = $this->processForm($category, $parameters, 'PUT');
        return $this->get($updatedCategory->getId(), $isFiltered);
    }



    /**
     * Processes the form.
     *
     * @param Category $category
     * @param array         $parameters
     * @param String        $method
     *
     * @return Category
     *
     * @throws \RestApiBundle\Exception\InvalidFormException
     */
    private function processForm(Category $category, array $parameters, $method = "PUT")
    {
        $form = $this->entityHandler->getFormFactory()->create(new CategoryType(), $category, array('method' => $method));
        $form->submit($parameters, 'PATCH' === $method);
        if ($form->isValid()) {

            $savedCategory = $form->getData();
            $em = $this->entityHandler->getDoctrine()->getManager();
            $em->persist($savedCategory);
            $em->flush($savedCategory);

            return $savedCategory;
        }

        throw new InvalidFormException('Invalid submitted data', $form);
    }

    /**
     * @param $name
     * @return null|Category
     */

    private function findCategoryByName($name){

        return $this->categoryRepository->findOneBy(array(
            'name' => $name,
            'project' => $this->entityHandler->getProject(),
        ));
    }


}

