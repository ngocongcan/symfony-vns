<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 11/27/16
 * Time: 1:11 PM
 */

namespace RestApiBundle\Handler;

use RestApiBundle\Exception\RestApiException;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use AppBundle\Entity\User;

class EntityHandler
{

    private $doctrine;
    private $formFactory;
    private $tokenStorage;
    /** @var User $user */
    private $user;

    public function __construct(RegistryInterface $doctrine, FormFactoryInterface $formFactory,TokenStorage $tokenStorage)
    {
        $this->doctrine = $doctrine;
        $this->formFactory = $formFactory;
        $this->tokenStorage = $tokenStorage;
        $this->user = $tokenStorage->getToken() ? $tokenStorage->getToken()->getUser() : null;

    }

    /**
     * @return RegistryInterface
     */
    public function getDoctrine()
    {
        return $this->doctrine;
    }

    /**
     * @return FormFactoryInterface
     */
    public function getFormFactory()
    {
        return $this->formFactory;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }


    /**
     * @return array
     */
    public function getLinkedProjects(){
        $usersProjects = $this->doctrine->getRepository('AppBundle:UserProject')->findOneBy(
            array('user'=> $this->user)
        );

        $projects = array_map(function($o) { return $o->getProject(); }, $usersProjects);
        return $projects;
    }

    /**
     * @return Project| null
     */
    public function getProjectByAccessToken($accessToken){

        if(is_null($accessToken) || strlen(trim($accessToken)) == 0){
            throw new RestApiException("accessToken is invalided", 404);
        }
        $userDevice = $this->doctrine->getRepository('AppBundle:UserDevice')->findOneBy(
            array('accessToken'=> $accessToken)
        );

        return $userDevice ? $userDevice->getProject() : null ;
    }

    /**
     * @param $entityClass
     * @return mixed
     */
    public function createEntity($entityClass)
    {
        return new $entityClass();
    }

}